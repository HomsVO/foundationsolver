import 'rsuite/dist/styles/rsuite-dark.min.css';
import React, { Component } from 'react';
import PillarFoundationApp from './Components/PillarFoundation/PillarApp/index.jsx'
import StripeFoundationApp from './Components/StripeFoundation/StripeApp/index.jsx'
import StiltFoundationApp from './Components/StiltFoundation/StiltApp/index.jsx'
import 'bootstrap/dist/css/bootstrap.min.css';



import './App.css';

class App extends Component {
    state = {
        foundationType:'pillar',
        admin:false,
    };
    getData = () =>{
        let admin = localStorage.getItem('aaa') || false;
        this.setState({admin:admin})
    };

    foundationHandler = (e) =>{
        this.setState({[e.target.name]:e.target.value});
    };
  render() {
    return (

        <>
            <div className={'btn-group'}>
                <input type="radio" id={'founType-pillar'} className={'hide'} checked={this.state.foundationType === 'pillar'}
                       name={'foundationType'} value={'pillar'} onChange={this.foundationHandler}/>
                <label htmlFor="founType-pillar"  className={'btn btn-light mb-0'}>Столбчатый</label>
                <input type="radio" id={'founType-stripe'} className={'hide'}  checked={this.state.foundationType === 'stripe'}
                       name={'foundationType'} value={'stripe'} onChange={this.foundationHandler}/>
                <label htmlFor="founType-stripe" className={'btn btn-light mb-0'}>Ленточный</label>

                { this.state.admin &&
                        <>
                            <input type="radio" id={'founType-stilt'} className={'hide'}  checked={this.state.foundationType === 'stilt'}
                                   name={'foundationType'} value={'stilt'} onChange={this.foundationHandler}/>
                            <label htmlFor="founType-stilt" className={'btn btn-light mb-0'}>Свайный</label>
                        </>
                }

            </div>
            { this.state.foundationType === 'pillar' && <PillarFoundationApp/> }
            { this.state.foundationType === 'stripe' && <StripeFoundationApp/> }
            { this.state.foundationType === 'stilt' && <StiltFoundationApp/> }


            {/*<PillarFoundationApp/>*/}
            {/*<StripeFoundationApp/>*/}

        </>

    );
  }
  componentDidMount() {
      this.getData();
  }
}

export default App;
