import React from 'React';
import ReactDOM from "react-dom";
import { parseNumericInput, isEmptyObject, isEmptyString, isEmpty } from '../Modules/Global';
import NumericInput from "./NumericInput";

class CustomNumericInput extends React.Component
{

    constructor(props) {
        super(props);

        this.props = props;

        this.state = {
            value: this.props.value,
            isValid: this.props.hasOwnProperty('isValid') ? this.props.isValid : true,
            disabled: this.props.hasOwnProperty('disabled') ? !!this.props.disabled : false,
        };

        this.onValidate = this.props.onValidate || function () {
            return {
                isValid: true,
                textError: ''
            }
        };

        this.options = $.extend(true, {
            step: 1,
            min: Number.MIN_SAFE_INTEGER || -9007199254740991,
            max: Number.MAX_SAFE_INTEGER || 9007199254740991,
            // parse: parseNumericInput,
            className: 'form-control right-nav-input',
            enabledHandlerOnInput: this.props.hasOwnProperty('enabledHandlerOnInput') ? !!this.props.enabledHandlerOnInput : false,
            allowFloat: this.props.hasOwnProperty('allowFloat') ? !!this.props.allowFloat : true,
            allowNegative: this.props.hasOwnProperty('allowNegative') ? !!this.props.allowNegative : false,
            selectValueOnFocus: this.props.hasOwnProperty('selectValueOnFocus') ? !!this.props.selectValueOnFocus : false,
            receiveProps: this.props.hasOwnProperty('receiveProps') ? !!this.props.receiveProps : false,
        }, props);
    }

    componentDidMount() {
        $(ReactDOM.findDOMNode(this).parentNode).find('input[type="text"]').numericInput({
            allowFloat: this.options.allowFloat,
            allowNegative: this.options.allowNegative
        });
    }

    componentWillReceiveProps(nextProps) {
        let state = {};

        if (this.options.receiveProps) {
            state.value = nextProps.value;
            state.disabled = nextProps.hasOwnProperty('disabled') ? nextProps.disabled : false;
        }

        if (nextProps.isValid !== this.state.isValid) {
            let validator = this.onValidate({
                name: this.props.name,
                value: nextProps.value
            });

            state.isValid =  validator.isValid;
            state.textError =  validator.textError;
        }

        if (!(Object.keys(state).length === 0 && state.constructor === Object)) this.setState(state);
    }

    onInput (e) {
        let value = parseFloat(e.target.value);

        if (e.target.value === '-')
            return;
        else if (!isFinite(value))
            value = null;

        if (!isFinite(value)) value = null;

        let validator = this.onValidate({
            name: this.props.name,
            value: value
        });

        let state = {
            value: e.target.value,
        };

        if (!isEmpty(value)) {
            state.isValid =  validator.isValid;
            state.textError = validator.textError;
        }

        this.setState(state);

        if (this.options.enabledHandlerOnInput) this.props.onChange(value, validator);

        if (this.props.hasOwnProperty('onInput')) this.props.onInput(value, validator);
    }

    handleChange (value) {
        if (!isFinite(value)) value = null;

        let validator = this.onValidate({
            name: this.props.name,
            value: value
        });

        this.setState({
            value: value,
            isValid: validator.isValid,
            textError: validator.textError
        });

        if (this.props.hasOwnProperty('onChange')) this.props.onChange(value, validator);
    }

    render() {
        const self = this;

        // Наследуем базовые параметры
        let hasErrorClass = !this.state.isValid ? ' has-error' : '';
        return (
            <div className={'form-group no-margin' + hasErrorClass}>
                <label htmlFor="">{this.props.label}</label>
                <div className="sb-field-unit-container">
                    <NumericInput
                        disabled={this.state.disabled}
                        className={this.options.className}
                        placeholder={this.options.placeholder}
                        value={this.state.value}
                        inputMode="text"
                        step={this.options.step}
                        min={this.options.min}
                        max={this.options.max}
                        // parse={parseNumericInput}
                        onStep={this.handleChange.bind(this)}
                        onBlur={this.handleChange.bind(this)}
                        onFocus={e => {
                            if (self.options.selectValueOnFocus) e.target.setSelectionRange(0, e.target.value.length);
                        }}
                        onInput={this.onInput.bind(this)}
                    />
                    <span className="sb-unit sb-unit-length">{this.props.measure}</span>
                </div>
                {(!this.state.isValid) &&
                <div className="error-wrapper" style={{
                    display: (!this.state.isValid) ? 'block' : 'none'
                }}>{this.state.textError}</div>
                }
            </div>
        );
    }
}

export default CustomNumericInput;