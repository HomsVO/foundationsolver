import React, { Component } from 'react';
import { ToggleButton,ToggleButtonGroup } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-select/dist/css/bootstrap-select.min.css';
import FoundationSolve from '../PillarFoundationSolve'
import FoundationCheck from '../PillarFoundationCheck'

import './index.css';

class PillarFoundationProps extends Component {

  app = this.props.app;
  toggleButtonsGroupHandler = (value,e) =>{
        let name = e.target.name;

        if(name === 'тип_расчета'){
            if(value === 'check'){
                this.app.setState({'привязка_к_абсолютным_отметкам':false});
            }
        }

        if(name === 'тип_металлической_колонны'){
            if(value === 'двухветвевая' && this.app.state.n%2 !== 0) this.app.setState({n:2})
            if(value === 'одноветвевая' && this.app.state.n > 4) this.app.setState({n:2})
        }

        this.app.setState({
            [name]:value
        });



      if(name === 'тип_колонны' && value !== 'металлическая'){
          this.app.setState({anchorBoltHTMLReport:false})
      }
      if(name === 'тип_колонны' && value === 'металлическая'){
          this.app.setState({anchorBoltHTMLReport:true})
      }

    }

  paramsHandler = e =>{
      let name = e.target.name;
      let value = e.target.value;
      this.app.setState({[name]:value})
  }
  boltHandler = (number) =>{
        let svg;
        if(this.app.state['тип_металлической_колонны'] === 'двухветвевая'){
            svg = document.getElementById('svg-object');
        }else{
            if(this.app.state['база_колонны'] === 'низкая'){
                svg = document.getElementById('lowTraversaSvg');
            }else{
                svg = document.getElementById('highTraversaSvgB');
            }

        }
        const svgDoc = svg.contentDocument;

        const allBolts = svgDoc.getElementsByClassName('n');
        const bolts = svgDoc.getElementsByClassName('n-' + number);

        for(let i = 0;i < allBolts.length;i++){
            let bolt = allBolts[i];

            bolt.style.visibility = 'hidden';

        }

        for(let i = 0;i < bolts.length;i++){
            let bolt = bolts[i];

            bolt.style.visibility = 'visible';

        }


    }
  svgOneBranchHandler = (e) =>{
        let id = e.currentTarget.id;
        let img;
        let isHide = e.type === 'mouseout';
        let isShow = e.type === 'mouseover';
        let isLSide = id === 'la' || id === 'lp';
        let isBSide = id === 'ba' || id === 'bp' || e.currentTarget.name === 'n';
        let isBolts = e.currentTarget.name === 'n';

        this.boltHandler(this.app.state.n);

         if(this.app.state['база_колонны'] === 'высокая'){

            if(isLSide) img = document.getElementById('highTraversaSvgL');
            if(isBSide) img = document.getElementById('highTraversaSvgB');
            if(!isLSide && !isBSide) return false;

         }

         if(this.app.state['база_колонны'] === 'низкая'){
              img = document.getElementById('lowTraversaSvg');
         }

         if(isShow) img.style.opacity = 1;
         if(isHide) img.style.opacity = 0;


        const svgDoc = img.contentDocument;

        let items = svgDoc.getElementsByClassName(e.currentTarget.id);

        if(id === 'c_' || isBolts || id === 'ba'){
            let name = '';
            if(isBolts) name = 'n';
            if(id === 'ba') name = 'ba';
            if(e.currentTarget.id === 'c_') name = 'c_';

            items = svgDoc.getElementsByClassName(name + '-' + this.app.state.n);

        }

        for(let i = 0;i < items.length;i++){
            let item = items[i];
            if(item.classList.contains('no-color')) continue;

            if(item.hasAttribute('stroke')){

                item.setAttribute('stroke',isHide?isBolts?'black':'none':'red');
                item.setAttribute('stroke-width',isBolts?'0.5px':'1.5px');

            }else{
                if(item.getAttribute('fill') === '#f2f2f2') continue;
                item.setAttribute('fill',isHide?isBolts?'black':'none':'red');

            }

        }


    }
  svgTwoBranchesHandler = (e) => {

        let isHide = e.type === 'mouseout';
        let isShow = e.type === 'mouseover';
        let isBolts = e.currentTarget.name === 'n' || e.currentTarget.id === 'n';
        this.boltHandler(this.app.state.n);

        const img = document.getElementById('svg-object');

        if(isHide) img.style.opacity = 0;
        if(isShow) img.style.opacity = 1;

        const svgDoc = img.contentDocument;

        const lines = svgDoc.getElementsByClassName(e.currentTarget.id);

        if(isBolts || e.currentTarget.id === 'c_two' || e.currentTarget.id === 'c_'){
            let name = e.target.id;
            if(isBolts) name = e.currentTarget.name;
            const items = svgDoc.getElementsByClassName(name + '-' + this.app.state.n);


            for(let i = 0;i < items.length;i++){
                let item = items[i];

                if(!item.classList.contains('n-black')){
                    item.setAttribute('stroke',isHide?isBolts?'black':'none':'red');
                }
                if(e.currentTarget.id ==='c_two'){


                    const title = svgDoc.getElementsByClassName(e.currentTarget.id)[0];

                    title.setAttribute('fill',isHide?'none':'red');
                    item.setAttribute('stroke-width','3px');
                }
            }

            return false;
        }

        for(let i = 0; i < lines.length;i++){

            if(lines[i].hasAttribute('stroke')){

                lines[i].setAttribute('stroke',isHide?'none':'red');
                lines[i].setAttribute('stroke-width','3px');

            }else{

                lines[i].setAttribute('fill',isHide?'none':'red');

            }

        }



    }
  render() {
    return (
        <div style={{display: (this.app.state.tabs === '3')?'block':'none'}} className='container'>
            <div className={'row'}>
                <div className="col-md-6 mt-3">
                    <ToggleButtonGroup type="radio" className={'w-50'} name="тип_расчета" onChange={this.toggleButtonsGroupHandler} value={this.app.state['тип_расчета']}>
                        <ToggleButton size="sm" variant="custom" value={"solve"}>Вычислить</ToggleButton>
                        <ToggleButton size="sm" variant="custom" value={"check"}>Проверить</ToggleButton>
                    </ToggleButtonGroup>
                </div>

              {(this.app.state['тип_расчета'] === 'solve') &&
                  <FoundationSolve
                      app={this.app}
                      wrapper={this}
                  />
              }
              {(this.app.state['тип_расчета'] === 'check') &&
                  <FoundationCheck
                      app={this.app}
                      wrapper={this}
                  />
              }

            </div>
        </div>
    );
  }
}

export default PillarFoundationProps;
