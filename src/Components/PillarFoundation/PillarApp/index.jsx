import 'rsuite/dist/styles/rsuite-dark.min.css';
import React, { Component } from 'react';
import PillarLoads from '../PillarLoads'
import deleteIcon from '../../../img/trash.svg';
import Surveys from '../PillarSurveys'
import SurveysTable from '../../SurveysTable'
import PillarFoundationProps from '../PillarFoundationProps/index.jsx'
import PillarReport from '../PillarReport/index.jsx'
import Errors from '../../Errors/index.jsx'
import { Notification } from 'rsuite';
import { isEmpty } from "../../Global/Global";
import 'bootstrap/dist/css/bootstrap.min.css';
import rightArrow from "../../../img/right-arrow.svg";
import leftArrow from "../../../img/left-arrow.svg";



import './index.css';

// const URL = 'http://localhost:8000';
const URL = 'http://foundation-ru.1gb.ru';


class PillarApp extends Component {

    state = {
        "loadsData": [
            {
                "id": 1,
                "title": "Собственный вес",
                "N": "825",
                "Mx": "0",
                "My": "0",
                "Qx": "0",
                "Qy": "0",
                "type": "постоянная"
            },
            {
                "id": 2,
                "title": "Полезные",
                "N": "125",
                "Mx": "185",
                "My": "40",
                "Qx": "10",
                "Qy": "5",
                "type": "длительная"
            },
            {
                "id": 3,
                "title": "Снег",
                "N": "10",
                "Mx": "14",
                "My": "8",
                "Qx": "0",
                "Qy": "0",
                "type": "кратковременная"
            },
            {
                "id": 4,
                "title": "Ветер",
                "N": "2",
                "Mx": "9",
                "My": "4",
                "Qx": "15",
                "Qy": "10",
                "type": "кратковременная"
            }
        ],
        "surveysData": [
            {
                "глубина_залегания": 4.2,
                "подтип_грунта": "",
                "тип_грунта": "суглинок",
                "E_1": "",
                "Ee_1": "",
                "Esr_1": "",
                "Il_1": "0.67",
                "Ip_1": "12.4",
                "Sr_1": "0.63",
                "e_1": "0.85",
                "R_1": "",
                "filter_1": "",
                "particles_1": "",
                "Угол_внутреннего_трения_1": "",
                "влажность_Wl_1": "",
                "влажность_Wp_1": "",
                "плотность_p_1": "",
                "плотность_pd_1": "",
                "плотность_ps_1": "2.69",
                "природная_влажность_W_1": "",
                "сцепление_C_1": "",
                "E_2": "14.1",
                "Ee_2": "",
                "Esr_2": "4.7",
                "Il_2": "",
                "Ip_2": "",
                "Sr_2": "",
                "e_2": "",
                "R_2": "",
                "filter_2": "",
                "particles_2": "",
                "Угол_внутреннего_трения_2": "17",
                "влажность_Wl_2": "",
                "влажность_Wp_2": "",
                "плотность_p_2": "1.73",
                "плотность_pd_2": "",
                "плотность_ps_2": "",
                "природная_влажность_W_2": "",
                "сцепление_C_2": "13",
                "E_3": "",
                "Ee_3": "",
                "Esr_3": "",
                "Il_3": "",
                "Ip_3": "",
                "Sr_3": "",
                "e_3": "",
                "R_3": "",
                "filter_3": "",
                "particles_3": "",
                "Угол_внутреннего_трения_3": "",
                "влажность_Wl_3": "",
                "влажность_Wp_3": "",
                "плотность_p_3": "",
                "плотность_pd_3": "",
                "плотность_ps_3": "",
                "природная_влажность_W_3": "",
                "сцепление_C_3": ""
            },
            {
                "глубина_залегания": 2.4,
                "подтип_грунта": "",
                "тип_грунта": "суглинок",
                "E_1": "",
                "Ee_1": "",
                "Esr_1": "",
                "Il_1": "0.60",
                "Ip_1": "14.1",
                "Sr_1": "0.87",
                "e_1": "0.93",
                "R_1": "",
                "filter_1": "",
                "particles_1": "",
                "Угол_внутреннего_трения_1": "",
                "влажность_Wl_1": "",
                "влажность_Wp_1": "",
                "плотность_p_1": "",
                "плотность_pd_1": "",
                "плотность_ps_1": "2.69",
                "природная_влажность_W_1": "",
                "сцепление_C_1": "",
                "E_2": "",
                "Ee_2": "",
                "Esr_2": "5.3",
                "Il_2": "",
                "Ip_2": "",
                "Sr_2": "",
                "e_2": "",
                "R_2": "",
                "filter_2": "",
                "particles_2": "",
                "Угол_внутреннего_трения_2": "17",
                "влажность_Wl_2": "",
                "влажность_Wp_2": "",
                "плотность_p_2": "1.80",
                "плотность_pd_2": "",
                "плотность_ps_2": "",
                "природная_влажность_W_2": "",
                "сцепление_C_2": "15",
                "E_3": "",
                "Ee_3": "",
                "Esr_3": "",
                "Il_3": "",
                "Ip_3": "",
                "Sr_3": "",
                "e_3": "",
                "R_3": "",
                "filter_3": "",
                "particles_3": "",
                "Угол_внутреннего_трения_3": "",
                "влажность_Wl_3": "",
                "влажность_Wp_3": "",
                "плотность_p_3": "",
                "плотность_pd_3": "",
                "плотность_ps_3": "",
                "природная_влажность_W_3": "",
                "сцепление_C_3": ""
            },
            {
                "глубина_залегания": 3.8,
                "подтип_грунта": "",
                "тип_грунта": "суглинок",
                "E_1": "",
                "Ee_1": "",
                "Esr_1": "",
                "Il_1": "0.15",
                "Ip_1": "15.1",
                "Sr_1": "0.88",
                "e_1": "0.75",
                "R_1": "",
                "filter_1": "",
                "particles_1": "",
                "Угол_внутреннего_трения_1": "",
                "влажность_Wl_1": "",
                "влажность_Wp_1": "",
                "плотность_p_1": "",
                "плотность_pd_1": "",
                "плотность_ps_1": "2.69",
                "природная_влажность_W_1": "",
                "сцепление_C_1": "",
                "E_2": "",
                "Ee_2": "",
                "Esr_2": "11",
                "Il_2": "",
                "Ip_2": "",
                "Sr_2": "",
                "e_2": "",
                "R_2": "",
                "filter_2": "",
                "particles_2": "",
                "Угол_внутреннего_трения_2": "21",
                "влажность_Wl_2": "",
                "влажность_Wp_2": "",
                "плотность_p_2": "1.91",
                "плотность_pd_2": "",
                "плотность_ps_2": "",
                "природная_влажность_W_2": "",
                "сцепление_C_2": "23",
                "E_3": "",
                "Ee_3": "",
                "Esr_3": "",
                "Il_3": "",
                "Ip_3": "",
                "Sr_3": "",
                "e_3": "",
                "R_3": "",
                "filter_3": "",
                "particles_3": "",
                "Угол_внутреннего_трения_3": "",
                "влажность_Wl_3": "",
                "влажность_Wp_3": "",
                "плотность_p_3": "",
                "плотность_pd_3": "",
                "плотность_ps_3": "",
                "природная_влажность_W_3": "",
                "сцепление_C_3": ""
            },
            {
                "глубина_залегания": 4.2,
                "подтип_грунта": "",
                "тип_грунта": "суглинок",
                "E_1": "",
                "Ee_1": "",
                "Esr_1": "",
                "Il_1": "0.07",
                "Ip_1": "17.9",
                "Sr_1": "0.88",
                "e_1": "0.79",
                "R_1": "",
                "filter_1": "",
                "particles_1": "",
                "Угол_внутреннего_трения_1": "",
                "влажность_Wl_1": "",
                "влажность_Wp_1": "",
                "плотность_p_1": "",
                "плотность_pd_1": "",
                "плотность_ps_1": "2.70",
                "природная_влажность_W_1": "",
                "сцепление_C_1": "",
                "E_2": "",
                "Ee_2": "",
                "Esr_2": "13.3",
                "Il_2": "",
                "Ip_2": "",
                "Sr_2": "",
                "e_2": "",
                "R_2": "",
                "filter_2": "",
                "particles_2": "",
                "Угол_внутреннего_трения_2": "16",
                "влажность_Wl_2": "",
                "влажность_Wp_2": "",
                "плотность_p_2": "1.89",
                "плотность_pd_2": "",
                "плотность_ps_2": "",
                "природная_влажность_W_2": "",
                "сцепление_C_2": "33",
                "E_3": "",
                "Ee_3": "",
                "Esr_3": "",
                "Il_3": "",
                "Ip_3": "",
                "Sr_3": "",
                "e_3": "",
                "R_3": "",
                "filter_3": "",
                "particles_3": "",
                "Угол_внутреннего_трения_3": "",
                "влажность_Wl_3": "",
                "влажность_Wp_3": "",
                "плотность_p_3": "",
                "плотность_pd_3": "",
                "плотность_ps_3": "",
                "природная_влажность_W_3": "",
                "сцепление_C_3": ""
            }
        ],
        "tableSize": "normal",
        "tabs": "1",
        "нагрузки": "загружения",
        "тип_подколонника": "круглый",
        "вручную_значение": "",
        "привязка_к_абсолютным_отметкам": false,
        "глубина_заложения": "1.3",
        "высота_фундамента": "1.5",
        "отметка_поверхности": "",
        "вручную_сторона": "l",
        "отметка_верха": "",
        "отметка_подошвы": "",
        "класс_бетона": "B15",
        "город": 274,
        "форма_подошвы": "прямоугольная",
        "отапливаемость_здания": true,
        "рассчет_LB": "соотношение",
        "соотношение_значение": "0.75",
        "тип_здания": "тип_5",
        "тип_колонны": "металлическая",
        "глубина_слоя": 0.3,
        "lc": 0.6,
        "bc": 0.4,
        "конструктивная_схема": "жесткая",
        "устройство_пола": "на_лагах",
        "kh_temp": 18,
        "фундамент_под": "наружную_стену",
        "наличие_подвала": "false",
        "YII_": 1.82,
        "геотехническая_категория": "1",
        "Su": 15,
        "крен": false,
        "класс_арматуры": "A500",
        "initDataHTMLReport": true,
        "solveDHTMLReport": true,
        "solveAHTMLReport": true,
        "solveRHTMLReport": true,
        "solveEHtmlReport": true,
        "solvePHtmlReport": true,
        "EPRCheckHTMLReport": true,
        "solveSHTMLReport": true,
        "solveStepsHTMLReport": true,
        "solvePressingHTMLReport": true,
        "solvePlateReinforcementHTMLReport": true,
        "solveCheckMult": true,
        "plateCrackingHTMLReport": true,
        "solveUnderColumnReinforcement": true,
        "shortSolvedParams": true,
        "krenHTMLReport": false,
        "anchorBoltHTMLReport": true,
        'titlePageHTMLReport': true,
        'contentTitles': true,
        "планируемость_территории": false,
        "dw": 4.5,
        "Yc_h": 5,
        "Yc_l": 36,
        "k": 0.4,
        "la": 0.5,
        "La": 0.5,
        "LBc": 0.5,
        "c": 0.15,
        "alpha": 3.15,
        "база_колонны": "высокая",
        "errors": {},
        "ba": 0.45,
        "c_two": 0.5,
        "c_": 0.15,
        "hf": 1.5,
        "d": 1.4,
        "l": 2.8,
        "тип_расчета": "solve",
        "size": true,
        "anchor": true,
        "reinforcement": true,
        "тип_металлической_колонны": "одноветвевая",
        "нагрузка_на_колонну": "статическая",
        "марка_стали": "Ст3пс2",
        "конструкция_болта": "с отгибом",
        "n": "3",
        "h": 0.2,
        "b": 2.8,
        "yg": "1.3",
        "dc": 0.7,
        "lp": 0.8,
        "bp": 0.65,
        "db": "12",
        "H0": 25,
        "hl": {
            "1": 0.3,
            "2": 0.3
        },
        "xl": {
            "1": 0.6,
            "2": 1
        },
        "hb": {
            "1": 0.3
        },
        "xb": {
            "1": 0.6
        },
        "nb": 1,
        "nl": 2,
        "ad": 5,
        "rein_step": 200,
        "диаметр_арматуры": 0.014,
        "D": 0.5,
        "hn": 0.1,
        "iu": "",
        "наличие_подготовки": true,
        "ds_undercolumn":0.014,
        "kernels":4,
    };

    optionsSu = [];
    optionsCities = [];
    bolts = [];

    getAnchorBolts = () => {

        fetch(URL + '/anchors')
            .then((response) => { return response.json(); })
            .then((myJson) => {
                myJson.forEach(c=>{
                    this.bolts.push({
                        d:c.d,
                        t:c.type,
                    })
                })
            })
    }

  saveData = ()=>{ localStorage.setItem('data',JSON.stringify(this.state));};
  deleteData = ()=>{ localStorage.removeItem('data')};
  getData = () =>{ this.setState({...JSON.parse(localStorage.getItem('data'))})};

  getSu(){
    fetch(URL + '/su')
        .then((response) => { return response.json(); })
        .then((myJson) => {
            myJson.forEach(c=>{
                this.optionsSu.push({
                    value:c.id,
                    Su:c.Su,
                    label:c.title,
                    isDisabled:c.isDisabled,
                    kren:c.iu,
                    type:c.type,
                })
            })
        })

  }
  getCities(){
        fetch(URL + '/cities')
            .then((response)=> { return response.json(); })
            .then((myJson) => {
                myJson.forEach(c => {
                    this.optionsCities.push({
                        value: c.id,
                        label: c.city
                    })
                })
            })
    }

  solveFoundation = () =>{
    (async () => {
      const rawResponse = await fetch(URL + '/solver', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(this.state)
      });
      const content = await rawResponse.json();
      console.log(content);
    })();
  };

  showErrorFields = (errors) =>{

      Object.keys(errors).forEach( key =>{
          let el = document.querySelector('input#'+key);
          let evt = new Event('blur');
          if(el) el.dispatchEvent(evt);

      });

  };

  validateCheckSteps = (errors) => {

      let sides = ['l','b'];
      sides.forEach(side => {
          for (let i = 1; i <= 5;i++) {
              if(i > this.state['n' + side]){

                  delete  errors['x' + side +'-'+i];
                  delete  errors['h' + side +'-'+i];

              }else{

                  let xSide = this.state['x' + side][i];
                  let hSide = this.state['h' + side][i];

                  let xlValid = true;
                  let hlValid = true;

                  if((xSide < 0.1 || xSide > 7 || isEmpty(xSide) || isNaN(xSide)) && this.state['тип_расчета'] === 'check'){
                      xlValid = false;
                  }

                  if((hSide < 0.2 || hSide > 0.45 || isEmpty(hSide) || isNaN(hSide)) && this.state['тип_расчета'] === 'check'){
                      hlValid = false;
                  }

                  if(!xlValid) errors['x' + side +'-'+i] = {};
                  else delete  errors['x' + side +'-'+i];

                  if(!hlValid) errors['h' + side +'-'+i] = {};
                  else delete  errors['h' + side +'-'+i];


              }
          }
      });

      if(this.state.nl + this.state.nb === 0 && this.state['тип_расчета'] === 'check' && this.state.reinforcement){
          errors['nl'] = {};
          errors['nb'] = {};
      }else {
          delete errors['nb'];
          delete errors['nl'];
      }


      return errors;
  };

  validateDataAfterSubmit(){

      let errors = this.state.errors;

      // Валидация полей без таблицы

          Object.keys(this.state).forEach( key =>{
                const value = this.state[key];
                const name = key;

                let isValid = true;

                switch (name) {
                    case 'dw':
                        let deep = 0;
                        this.state.surveysData.forEach(item => deep+= item.глубина_залегания);
                        isValid = true;
                        if(deep < value && value !== 0) isValid = false;
                        if (value < 0) isValid = false;
                        break;
                    case 'Su':
                            if (value < 0) isValid = false;
                        break;
                    case 'соотношение_значение':
                            if(this.state['рассчет_LB'] === 'соотношение' && this.state['форма_подошвы'] === 'прямоугольная'){
                                if(value < 0 || value > 1) isValid = false;
                            }
                        break;
                    case 'глубина_слоя':

                            if(!this.state['планируемость_территории']) {
                                if (value > this.state['глубина_заложения'] && !isEmpty(this.state['глубина_заложения'])) {
                                    isValid = false;
                                }
                                if (isEmpty(value)) isValid = false;
                            }
                        break;
                    case 'глубина_заложения':

                            if(value < 0.6 || value > 10) isValid = false;
                            if(isEmpty(value)) isValid = true;
                            if(this.state['планируемость_территории'] === 'false'){
                                if(value < this.state['глубина_слоя'] && !isEmpty(value)){
                                    isValid = false;
                                }
                            }
                        break;
                    case 'высота_фундамента':
                        if(this.state['тип_расчета'] === 'check' && this.state['anchor'] && (!this.state['reinforcement'] && !this.state['size'])) {
                            if (isEmpty(value) || parseFloat(value) === 0) isValid = false;
                        }else {
                            if (value < 0.6 || value > 15) isValid = false;
                            if (isEmpty(value)) isValid = true;
                        }
                        break;
                    case 'вручную_значение':
                        if(this.state['рассчет_LB'] === 'вручную' && this.state['форма_подошвы'] === 'прямоугольная'){
                            if (value < 0.3) isValid = false;
                            if(value > 25) isValid = false;
                            if(isEmpty(value)) isValid = false;
                        }
                        break;
                    case 'hn':
                        if(this.state['наличие_подготовки']){
                            if(isEmpty(value) || value < 0.1 || value > 1) isValid = false;
                        }
                        break;
                    case 'h':
                        if(this.state['тип_колонны'] === 'металлическая') {
                            if (this.state['тип_металлической_колонны'] === 'двухветвевая') {
                                if (value < 0.2 || value > 3 || isEmpty(value)) isValid = false;
                            }
                        }
                        break;
                    case 'k':
                        if(this.state['тип_колонны'] === 'металлическая') {
                            if (this.state['тип_металлической_колонны'] === 'двухветвевая') {
                                if (value < 0.15 || value > 1.1 || isEmpty(value)) isValid = false;
                            }
                        }
                        break;
                    case 'LBc':
                        if(this.state['форма_подошвы'] === 'круглая') {
                            if (this.state['тип_подколонника'] === 'круглый') {
                                if (value < 0.3 || value > 1.2 || isEmpty(value)) isValid = false;
                            }
                        }
                        break;
                    case 'YII_':
                        if (value > 2.5 || value < 0) isValid = false;
                        if (isEmpty(value))  isValid = false;
                        break;
                    case 'kh_temp':
                        if(value < 0 || value > 35) isValid = false;
                        break;
                    case 'bp':
                        if(this.state['тип_колонны'] === "металлическая" && this.state['тип_металлической_колонны'] === "одноветвевая"){
                            if(value < 0.2 || value > 1) isValid = false;
                            if(isEmpty(value))  isValid = false;
                        }
                        break;
                    case 'lp':
                        if(this.state['тип_колонны'] === 'металлическая'){
                            if(this.state['тип_металлической_колонны'] === 'одноветвевая'){
                                if(this.state['база_колонны'] === 'низкая'){
                                    if(value <= this.state.c*2) isValid = false;
                                }
                                if(value < 0.2 || value > 1) isValid = false;
                                if(isEmpty(value))  isValid = false;
                            }
                        }
                        break;
                    case 'La':
                        if(value < 0.2 || value > 1 || isEmpty(value)) isValid = false;
                        break;
                    case 'c':
                        if(value < 0.1 || value > 0.6 || isEmpty(value)) isValid = false;
                        break;
                    case 'lc':
                    case 'bc':
                        if(this.state['тип_колонны'] !== 'металлическая'){
                            if(this.state['тип_колонны'] === 'монолитная' || (this.state['тип_подколонника'] !== 'круглый' && this.state['форма_подошвы'] === 'круглая')){
                                if(value < 0.3 || value > 1.2){
                                    isValid = false;
                                }
                            }
                        }
                        if(this.state['тип_колонны'] === 'сборная'){
                            if(value > 1.6){
                                isValid = false;
                            }
                        }
                        break;
                    case 'Yc_l':
                        if(this.state['конструктивная_схема'] === 'жесткая'){
                            if(value < 0 || value > 120) isValid = false;
                            if(isEmpty(value)) isValid = false;
                        }
                        break;
                    case 'Yc_h':
                        if(this.state['конструктивная_схема'] === 'жесткая'){
                            if(value < 0 || value > 60) isValid = false;
                            if(isEmpty(value)) isValid = false;
                        }
                        break;
                    case 'la':
                    case 'ba':
                            if(this.state['тип_колонны'] === 'металлическая'){
                                if(this.state['тип_металлической_колонны'] === 'одноветвевая'){
                                    if (value < 0.05 || isEmpty(value) || value > 0.8) isValid = false;
                                }
                            }
                        break;
                    case 'dc':
                        if(this.state['тип_колонны'] === 'сборная'){
                            if(value > 1.2 || this.state.lc > value) isValid = false;
                            if(isEmpty(value)) isValid = false;

                            if(!isEmpty(this.state['высота_фундамента'])){
                                if(value > this.state['высота_фундамента'] - 0.2) isValid = false;
                            }
                        }
                        break;
                    default:
                        break;
                }

                if(this.state['тип_расчета'] === 'check'){

                    switch (name) {
                        case 'l':
                            if(this.state['форма_подошвы'] === 'прямоугольная') {
                                if (value < 0 || value > 12 || isEmpty(value)) {
                                    isValid = false;
                                }
                            }
                            break;
                        case 'kernels':
                            if(this.state['reinforcement']) {
                                if (value < 4 || value > 26 || isEmpty(value)) {
                                    isValid = false;
                                }
                            }
                            break;
                        case 'b':
                            if(this.state['форма_подошвы'] === 'прямоугольная') {
                                if (value < 0 || value > 12 || isEmpty(value)) {
                                    isValid = false;
                                }
                            }
                            break;
                        case 'D':
                            if(this.state['форма_подошвы'] === 'круглый') {
                                if (value < 0 || value > 12 || isEmpty(value)) {
                                    isValid = false;
                                }
                            }
                            break;
                        case 'ad':
                            if(value < 4 || value > 7 && this.state['reinforcement']){
                                isValid = false;
                            }
                            break;
                        case 'глубина_заложения':
                            if(value < 0)isValid = false;
                            if(value < 0.5 || value > 4.5) isValid = false;
                            if(!this.state['планируемость_территории']){
                                if(value < this.state['глубина_слоя'] && !isEmpty(value)){
                                    isValid = false;
                                }
                            }
                            if (isEmpty(value)) isValid = true;
                            break;
                    }
                }

                if(!isValid) errors[name] = {};
                else delete  errors[name];
            });

          errors = this.validateCheckSteps(errors);



      // Валидация таблицы
      if(this.state.surveysData.length > 0){
          let deep = 0;
          this.state.surveysData.forEach(item => deep += item['глубина_залегания']);
          let dw = (this.state.dw <= deep && this.state.dw !== 0);

          this.state.surveysData.forEach( (item,index) => {

              Object.keys(item).forEach( key =>{

                  const gType = item['тип_грунта'];
                  const value = item[key];
                  const name = key + "_" + index;

                  let isClay = false;
                  const isNotRock = gType !== 'скальные';

                  switch (gType) {
                      case 'суглинок':
                      case 'глина':
                      case 'супесь':
                          isClay = true;
                          break;
                      case 'крупнообломочный грунт':
                          if(item['подтип_грунта'] === 'глинистый_заполнитель') isClay = true;
                          break;
                  }

                  let isValid = true;
                  switch (key) {
                      case 'Ip_1':
                      {
                          if(value < 0 || value > 100 || isEmpty(value) && isClay ) isValid = false;
                      }
                          break;
                      case 'Il_1':
                      {
                          if(value < -1 || value > 2 || isEmpty(value) && isClay ) isValid = false;
                      }
                          break;
                      case 'плотность_ps_1':
                      {
                          if(dw){
                              if(value < 1.5 || value > 3 || isEmpty(value) && isNotRock) isValid = false;
                          }
                      }
                          break;
                      case 'плотность_pd_1':
                      {
                          if(this.state.tableSize !== 'normal'){
                              if(value < 1.3 || value > 2.5 && isEmpty(value)) isValid = false;
                          }
                      }
                          break;
                      case 'e_1':
                      {
                          if(isNotRock && dw) {
                              if (value < 0.2 || value > 1 || isEmpty(value)) isValid = false;
                          }
                      }
                          break;
                      case 'Ee_2':
                      {
                          if(this.state['геотехническая_категория'] === '3' && isNotRock){
                              if(value < 1 || value > 250 || isEmpty(value)) isValid = false;
                          }
                      }
                          break;
                      case 'filter':
                      {
                          if(value < 0 || value > 100 || isEmpty(value)) isValid = false;
                      }
                          break;
                      case 'Sr_1':
                      {
                          if(isNotRock && value < 0 || value > 1 || isEmpty(value)) isValid = false;
                      }
                          break;
                      case 'Esr_2':
                      {
                          if(isNotRock){
                              if(value < 1 || value > 50 || isEmpty(value) && isNotRock) isValid = false;
                          }
                      }
                          break;
                      case 'плотность_p_2':
                      {
                          if(isNotRock){
                              if(value < 1.5 || value > 2.5 || isEmpty(value) && isNotRock) isValid = false;
                          }
                      }
                          break;
                      case 'E_2':
                      {
                          if(isNotRock){
                              if(value < 1 || value > 100 || isEmpty(value) && isNotRock) isValid = false;
                          }

                      }
                          break;
                      case 'сцепление_C_2':
                      {
                          if((value < 0.1 || value > 50 || isEmpty(value)) && isNotRock) isValid = false;
                      }
                          break;
                      case 'Угол_внутреннего_трения_2':
                      {
                          if( isNotRock ){
                              if (value < 0 || value > 45 || isEmpty(value))  isValid = false;
                          }
                      }
                          break;
                      case 'R_2':
                          if((value < 5 || value > 20 || isEmpty(value)) && !isNotRock) isValid = false;
                          break;
                      case 'particles':
                          if(this.state.tableSize !== 'normal'){
                              if(value < 0 || value > 100 || isEmpty(value) && isNotRock) isValid = false;
                          }

                          break;
                      case 'природная_влажность_W_1':
                      {
                          if(this.state.tableSize !== 'normal'){
                              if(value < 0.01 || value > 100 || isEmpty(value) && isNotRock) isValid = false;
                          }
                      }
                          break;
                      case 'влажность_Wl_1':
                      {
                          if(isNotRock && this.state.tableSize !== 'normal'){
                              if(value < 0 || value > 50 || isEmpty(value)) isValid = false;
                          }
                      }
                          break;
                      case 'влажность_Wp_1':
                      {
                          if(isNotRock && this.state.tableSize !== 'normal'){
                              if(value < 0 || value > 50 || isEmpty(value)) isValid = false;
                          }
                      }
                          break;
                      case 'filter_1':
                      {
                          if(this.state.tableSize !== 'normal') {
                              if (value < 0 || value > 100 || isEmpty(value)) isValid = false;
                          }
                      }
                          break;
                      case 'глубина_залегания':
                          if (value <= 0 || isEmpty(value)) isValid = false;
                          break;
                  }
                  if(!isValid) errors[name] = {};
                  else delete  errors[name];
              });
          });
      }
      this.showErrorFields(errors);


      this.setState({
          errors:errors
      });

  }

  tabsHandler = (e) => {
        this.setState({
            tabs:e.target.value
        });
  };

  formSubmit = (e) => {
    e.preventDefault();

    this.validateDataAfterSubmit();
    let loadsIsValid = false;

    this.state.loadsData.forEach(item => {
        if (item.type === 'постоянная' || item.type === 'длительная') loadsIsValid = true;
    });

    if(!isEmpty(this.state.errors)) {

        Notification.error({
            title: 'Ошибка',
            duration: 40000,
            description: <Errors app={this}/>,
        });
        return false;
    }
    if(this.state.surveysData.length === 0){
        Notification.error({
            title: 'Ошибка',
            duration: 40000,
            description: <div className={'notification-container'}> Не задано ни одного грунтового слоя</div>,
        });
        return false;
    }

    if(!loadsIsValid){
        Notification.error({
            title: 'Ошибка',
            duration: 40000,
            description: <div className={'notification-container'}> Должна быть задана хотя бы одна длительная или постоянная нагрузка</div>,
        });
        return false;
    }
    e.target.submit();
  };



  render() {

    return (
        <div className="pillar container-fluid p-0 root-container">
            <div className="container-fluid p-0">
                <div className="control-panel ">
                    <div className="d-flex">
                        <div className="btn-group btn-group--ml5" onChange={this.tabsHandler} role="group" aria-label="Basic example">
                            <input type="radio" name='tabs' id='control1' value='1' checked={this.state.tabs === '1'}  />
                            <label htmlFor="control1" className="btn btn-control">Сбор нагрузок на фундамент</label>
                            <input type="radio" name='tabs' id='control3' value='3' checked={this.state.tabs === '3'}/>
                            <label htmlFor="control3" className="btn btn-control">Параметры фундамента</label>
                            <input type="radio" name='tabs' id='control2' value='2' checked={this.state.tabs === '2'}/>
                            <label htmlFor="control2" className="btn btn-control">Инженерно-геологические изыскания</label>
                            <input type="radio" name='tabs' id='control4' value='4' checked={this.state.tabs === '4'}/>
                            <label htmlFor="control4" className="btn btn-control">Параметры отчета</label>
                        </div>
                        <div className="btn-group btn-group--ml5" role="group" aria-label="Basic example">
                            <a href='#' className='btn btn-primary my-5px' onClick={this.saveData}>Сохранить данные!</a>
                            <a href='#' className='btn btn-primary my-5px' onClick={this.deleteData}>
                                <img src={deleteIcon} style={{width:'15px',height:'100%'}} alt=""/>
                            </a>
                        </div>
                        <div role="group" className='btn-group btn-group--ml5 my-5px' aria-label="Basic example">
                            <form method="POST" onSubmit={this.formSubmit} target="_blank" action={URL + '/' + this.state['тип_расчета']}>
                                <button type='submit'  className='mx-1 btn btn-success' > Получить отчет </button>
                                <input type='text' className='hidden' name='data' value={JSON.stringify(this.state)}/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
          <div className='container-fluid'>
               <PillarLoads app={this}/>
               <div style={{display: (this.state.tabs === '2')?'block':'none'}} className={'mt-2'}>
                    <Surveys data={this.state} app={this} />
                    <SurveysTable data={this.state} app={this}/>
                   <div className="d-flex justify-content-between mt-3">
                       <div className="position-relative z-index-top mr-3">
                           <div className={'mb-5 pointer'} >
                               <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabs:"3"})} >
                                   <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                   <span className={'mr-2 fz-14'}><b>К Параметрам</b></span>
                               </div>
                           </div>
                       </div>
                       <div className="position-relative z-index-top ml-3">
                           <div className={'mb-5 pointer'} >
                               <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabs:"4"})}>
                                   <span className={'mr-2 fz-14'}><b>К Отчету</b></span>
                                   <img width='25px' height='25px' src={rightArrow} alt=""/>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
              <PillarFoundationProps app={this} />
              <PillarReport app={this}/>
          </div>
    </div>
    );
  }
  componentDidMount() {

      this.getData();
      this.getSu(URL);
      this.getCities(URL);
      this.getAnchorBolts();
  }
}

export default PillarApp;
