import React, { Component } from 'react';
import { ToggleButton,ToggleButtonGroup} from 'react-bootstrap'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Popover, Whisper,Checkbox} from 'rsuite/lib/index';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-select/dist/css/bootstrap-select.min.css';
import 'react-tabs/style/react-tabs.css';
import anchor1 from "../../../img/anchor1.png";
import anchor2 from "../../../img/anchor2.png";
import anchor5 from "../../../img/anchor3.png";
import anchor4 from "../../../img/anchor4.png";
import anchor3 from "../../../img/anchor5.png";

import highTraversaSvgL from '../../../img/highTraversaSvgL.svg';
import highTraversaSvgB from '../../../img/highTraversaSvgB.svg';
import lowTraversa from '../../../img/lowTraversa.svg';
import twoBranches from '../../../img/twoBranches.svg';


import './index.css';
import CustomNumericInput from "../../CustomNumericInput/CustomNumericInput";
import ConstructProperties from "../../ConstructProperties/index.jsx";
import {isEmpty , precision} from "../../Global/Global";
import rightArrow from "../../../img/right-arrow.svg";
import leftArrow from "../../../img/left-arrow.svg";

 
class PillarFoundationSolve extends Component {

  state ={
    su:null,
    selectedOption:{
      Su: 10,
      isDisabled: 0,
      kren: "",
      label: "то же. с устройством железобетонных поясов или монолитных перекрытий. а также здания монолитной конструкции",
      value: 3
    },
    tabIndex:0,
  }

  wrapper = this.props.wrapper;
  app = this.props.app;
  options = this.app.optionsSu;
  svgTwoBranchesHandler = this.wrapper.svgTwoBranchesHandler
  svgOneBranchHandler = this.wrapper.svgOneBranchHandler;
  paramsHandler = this.wrapper.paramsHandler;
  toggleButtonsGroupHandler = this.wrapper.toggleButtonsGroupHandler;


  validateField = (props) => {

    const value = props.value;
    const name = props.name;
    let isValid = true,
        textError = 'Заполните поле';

    switch (name) {
      case 'вручную_значение':
        if(this.app.state['рассчет_LB'] === 'вручную'){
          if (value < 0.3) {
            isValid = false;
            textError = 'Сторона фундамента слишком мала';
          }
          if(value > 25){
            isValid = false;
            textError = 'Сторона фундамента слишком велика';
          }
          if(isEmpty(value)) {
            isValid = false;
            textError = 'Заполните поле';
          }
        }
        break;
      case 'hn':
        if(this.app.state['наличие_подготовки']){
          if(isEmpty(value)) {
            isValid = false;
            textError = 'Заполните поле';
          }

          if(value < 0.1 || value > 1){
            isValid = false;
            textError = 'hn должно быть не более 1 м и не менее 0.1 м';
          }
        }
        break;
      case 'Su':
        if (value < 0) {
          isValid = false;
          textError = 'Осадка не может быть отрицательной';
        }
        if(isEmpty(value)){
          isValid = false;
          textError = "Заполните поле";
        }
        break;
      case 'la':
      case 'ba':
        if(this.app.state['тип_колонны'] === 'металлическая'){
          if(this.app.state['тип_металлической_колонны'] === 'одноветвевая'){
            if (value < 0.05 || isEmpty(value) || value > 0.8) {
              isValid = false;
              textError = 'Расстояние от оси колонны до оси болта не менее 0.05 м и не более 0.8 м';
            }
          }
        }
        break;
      case 'соотношение_значение':
        if(value < 0.6 || value > 0.85){
          isValid = false;
          textError = 'Рекомендуется принимать соотношение сторон прямоугольного фундамента в пределах 0.6-0.85 в соответствии с СП 50-101-2004';
        }
        if(value < 0){
          isValid = false;
          textError = 'Соотношение сторон не может быть отрицательным';
        }
        if(value > 1){
          isValid = false;
          textError = 'Соотношение сторон не может быть больше 1';
        }
        break;
      case 'kh_temp':
        if(value < 0){
          isValid = false;
          textError = 'Температура не может быть отрицательной, введите корректные данные';
        }
        if(value > 45){
          isValid = false;
          textError = 'Температура должна быть не более 45';
        }
        if(isEmpty(value)) isValid = false;
        break;
      
      case 'bp':
        if(value < 0.2 || value > 1){
          isValid = false;
          textError = 'Введены некорректные размеры анкерной плиты, уточните размеры';
        }
        if(this.app.state['тип_колонны'] === 'металлическая'){
          if(this.app.state['тип_металлической_колонны'] === 'одноветвевая'){
            if(this.app.state['база_колонны'] === 'низкая'){
              if(value <= this.app.state.c_*2){
                isValid = false;
                textError = 'Анкерный болт находится за границей плиты,увеличьте анкерную плиту'
              }
            }
          }
        }
        if(isEmpty(value)) isValid = false;
        break;
      case 'lp':
        if(value < 0.2 || value > 1){
          isValid = false;
          textError = 'Введены некорректные размеры анкерной плиты, уточните размеры';
        }
        if(this.app.state['тип_колонны'] === 'металлическая'){
          if(this.app.state['тип_металлической_колонны'] === 'одноветвевая'){
            if(this.app.state['база_колонны'] === 'низкая'){
              if(value <= this.app.state.c*2){
                isValid = false;
                textError = 'Анкерный болт находится за границей плиты,увеличьте анкерную плиту'
              }
            }
          }
        }
        if(isEmpty(value)) isValid = false;
        break;
      case 'lc':
        if(this.app.state['тип_колонны'] !== 'металлическая'){
          if((this.app.state['тип_подколонника'] !== 'круглый' && this.app.state['форма_подошвы'] === 'круглая') || this.app.state['тип_колонны'] === 'монолитная' ){
            if(value < 0.3 || value > 1.2){
              isValid = false;
              textError = 'Длина подколонника должна быть в пределах от 0.3 м до 1.2 м';
            }
          }
        }
        if(this.app.state['тип_колонны'] === 'сборная'){
          if(value > 1.6){
            isValid = false;
            textError = 'Сечение колонны должно быть в пределах от 0.1 м до 1.6 м';
          }
        }

        break;
      case 'bc':
        if(this.app.state['тип_колонны'] !== 'металлическая'){
          if((this.app.state['тип_подколонника'] !== 'круглый' && this.app.state['форма_подошвы'] === 'круглая') || this.app.state['тип_колонны'] === 'монолитная' ){
            if(value < 0.3 || value > 1.2){
              isValid = false;
              textError = 'Длина подколонника должна быть в пределах от 0.3 м до 1.2 м';
            }
          }
        }
        if(this.app.state['тип_колонны'] === 'сборная'){
          if(value > 1.6){
            isValid = false;
            textError = 'Сечение колонны должно быть в пределах от 0.1 м до 1.6 м';
          }
        }
        break;
      case 'Yc_l':
          if(value > 120){
            isValid = false;
            textError = 'L не более 120 м';
          }
          if(value < 1){
            isValid = false;
            textError = 'L не менее 1 м';
          }
          if(isEmpty(value)){
            isValid = false;
            textError = 'Заполните поле';
          }
          break;
      case 'Yc_h':
        if(value > 60){
          isValid = false;
          textError = 'H не более 60 м';
        }
        if(value < 1){
          isValid = false;
          textError = 'H не менее 1 м';
        }
        if(isEmpty(value)){
          isValid = false;
          textError = 'Заполните поле';
        }
        break;
      case 'h':
        if(value < 0.2 || value > 3 || isEmpty(value)){
          isValid = false;
          textError = 'Расстояние в осях между ветвями колонны должно быть не менее 0.2 м и не более 3 м';
        }
        break;
      case 'k':
        if(value < 0.15 || value > 1.1 || isEmpty(value)){
          isValid = false;
          textError = 'Расстояние от оси болта до противоположной грани болта должна быть в пределах от 0.15 м до 1.1 м';
        }
        break;
      case 'La':
        if(value < 0.2 || value > 1 || isEmpty(value)){
          isValid = false;
          textError = 'Расстояние от оси болта до противоположной грани пластины должно быть в пределах от 0.2 м до 1 м';
        }
        break;
      case 'c':
        if(value < 0.1 || value > 0.6 || isEmpty(value)){
          isValid = false;
          textError = 'Расстояние от оси болта до оси колонны должно быть в пределах от 0.1 м до 0.6 м';
        }
        if(this.app.state['тип_колонны'] === 'металлическая'){
          if(this.app.state['тип_металлической_колонны'] === 'одноветвевая'){
            if(this.app.state['база_колонны'] === 'низкая'){
              if(this.app.state.lp <= value*2){
                isValid = false;
                textError = 'Анкерный болт находится за границей плиты,увеличьте анкерную плиту'
              }
            }
          }
        }
        break;
      case 'c_':
        if(value < 0.02 || value > 0.6 || isEmpty(value)){
          isValid = false;
          textError = 'Расстояние от оси болта до оси колонны должно быть в пределах от 0.02 м до 0.6 м';
        }
        if(this.app.state['тип_колонны'] === 'металлическая'){
          if(this.app.state['тип_металлической_колонны'] === 'одноветвевая'){
            if(this.app.state['база_колонны'] === 'низкая'){
              if(this.app.state.bp <= value*2){
                isValid = false;
                textError = 'Анкерный болт находится за границей плиты,увеличьте анкерную плиту'
              }
            }
          }
        }
        break;
      case 'LBc':
        if(this.app.state['форма_подошвы'] === 'круглая') {
          if (this.app.state['тип_подколонника'] === 'круглый') {
            if (value < 0.3 || value > 1.2 || isEmpty(value)) {
              isValid = false;
              textError = 'Диаметр подколонника  должна быть в пределах от 0.3 м до 1.2 м';
            }
          }
        }
        break;
      case 'dc':
        if(this.app.state['тип_колонны'] === 'сборная'){
          if(value > 2){
            isValid = false;
            textError = 'Глубина заделки должна быть меньше из условия соотношения сечения колонны к глубине ее заделки';
          }
          if(Math.max(this.app.state.lc,this.app.state.bc) > value){
            isValid = false;
            textError = 'Условие заделки колонны в стакан не обеспеченно убедитесь в правильности введенных данных';
          }
          if(!isEmpty(this.app.state['высота_фундамента'])){
            if(value > this.app.state['высота_фундамента'] - 0.2){
              isValid = false;
              textError = 'Высота фундамента меньше глубины заделки колонны в стакан';
            }
          }
        }
        break;
    }

    if(value < 0){
      isValid = false;
      textError = 'Значение не может быть отрицательным';
    }
    return {
      isValid: isValid,
      textError: (isValid) ? '' : textError
    }
  }

  customNumericInputHandler = (value, name , validator) =>{

      let errors = this.app.state.errors;
      if(!validator.isValid) {
        if(name === 'соотношение_значение') {
          if (value < 0 || value > 1) {
            errors[name] = {
              textError: validator.textError,
              tab: '3'
            };
          }
        }else {
          errors[name] = {
            textError: validator.textError,
            tab: '3'
          };
        }
      }else{
          delete  errors[name];
      }

      this.app.setState({
        [name]: value,
        errors:errors
      });

      if(name === 'n'){
        this.boltHandler(value);
      }


  };


  popover = (html) =>{
    return <Popover visible style={{
      color:"#fff"
    }}>{html}</Popover>
  }

  customCheckboxHandler = (name,checked) =>{
     this.app.setState({
        [name]:checked,
    })

}
 render() {


    return (
        <div className='container-fluid'>
            <div className="row mt-3">
              <div className="col-md-12">
                <Tabs forceRenderTabPanel selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.setState({ tabIndex })}>
                  <TabList>
                    <Tab>Харатеристики фундамента</Tab>
                    <Tab>Конструктивные особенности здания</Tab>
                    <Tab>Тип и параметры подколонника</Tab>
                  </TabList>
                  <TabPanel>
                    <div className={'row'}>
                      <div className="col-md-6 pb-5">
                        <p className="mb-1 mt-3">Форма подошвы</p>
                        <ToggleButtonGroup type="radio" name="форма_подошвы" value={this.app.state['форма_подошвы']} onChange={this.toggleButtonsGroupHandler}>
                          <ToggleButton size="sm" variant="custom" value={'прямоугольная'}>Прямоугольная</ToggleButton>
                          <ToggleButton size="sm" variant="custom" value={'круглая'}>Круглая</ToggleButton>
                          <ToggleButton size="sm" variant="custom" value={'квадратная'}>Квадратная</ToggleButton>
                        </ToggleButtonGroup>
                        {(this.app.state['форма_подошвы'] === 'прямоугольная') &&
                        <div className={'mt-3'}>
                          <ToggleButtonGroup onChange={this.toggleButtonsGroupHandler} type="radio" name="рассчет_LB" value={this.app.state['рассчет_LB']}>
                            <ToggleButton size="sm" variant="custom" value='вручную'>l или b вручную</ToggleButton>
                            <ToggleButton size="sm" variant="custom" value='соотношение'>b/l через соотношение</ToggleButton>
                          </ToggleButtonGroup>
                          {this.app.state['рассчет_LB'] === 'вручную' &&
                          <div className="d-flex w-75 mt-3">
                            <ToggleButtonGroup type="radio" onChange={this.toggleButtonsGroupHandler} name="вручную_сторона" value={this.app.state['вручную_сторона']} className={'h-30px'}>
                              <ToggleButton size="sm" variant="custom" value={'l'}>l</ToggleButton>
                              <ToggleButton size="sm" variant="custom" value={'b'}>b</ToggleButton>
                            </ToggleButtonGroup>
                            <CustomNumericInput
                                name={'вручную_значение'}
                                className="form-control input-text m-0"
                                value={this.app.state['вручную_значение']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                label={""}
                                min={0}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('вручную_значение')}
                                onChange={this.customNumericInputHandler}
                            />
                          </div>
                          }
                          {this.app.state['рассчет_LB'] === 'соотношение' &&
                          <div className="mt-3">
                            <CustomNumericInput
                                name={'соотношение_значение'}
                                className="form-control input-text"
                                value={this.app.state['соотношение_значение']}
                                allowNegative={false}
                                measure={''}
                                labelLeft={true}
                                label={"Соотношение b/l ="}
                                step={0.05}
                                min={0.05}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={true}
                                onChange={this.customNumericInputHandler}
                            />
                          </div>
                          }
                        </div>
                        }

                        <p className={'mt-3'}><b><ins>Конструктивные особенности фундамента</ins></b></p>
                        <div className="d-flex  justify-content-between">
                          <p className="lh-30 mb-0 mr-2">Фундамент под:</p>
                          <ToggleButtonGroup type="radio" onChange={this.toggleButtonsGroupHandler} name="фундамент_под" value={this.app.state['фундамент_под']}>
                            <ToggleButton size="sm" variant="custom" value={'наружную_стену'}>Наружную стену</ToggleButton>
                            <ToggleButton size="sm" variant="custom" value={'внутреннюю_стену'}>Внутреннюю стену</ToggleButton>
                          </ToggleButtonGroup>
                        </div>
                        <div className="mt-3">
                          <Checkbox name={'наличие_подготовки'} checked={this.app.state['наличие_подготовки']} onChange={(v,checked) => this.customCheckboxHandler('наличие_подготовки',checked)} >Наличие подготовки под фундамент</Checkbox>
                          {(this.app.state['наличие_подготовки']) &&
                          <CustomNumericInput
                              name={'hn'}
                              className="form-control input-text"
                              value={this.app.state['hn']}
                              allowNegative={false}
                              measure={'[м]'}
                              precision={2}
                              min={0}
                              labelLeft={true}
                              label={"Толщина подготовки h<sub>n</sub>:"}
                              step={0.1}
                              onValidate={this.validateField}
                              enabledHandlerOnInput={true}
                              isValid={!this.app.state.errors.hasOwnProperty('hn')}
                              onChange={this.customNumericInputHandler}
                          />
                          }
                        </div>
                        <p className={'mt-3'}><b><ins>Характеристики арматуры и бетона</ins></b></p>
                        <div className="d-flex  justify-content-between ">
                          <p className="lh-30 mb-0">Класс арматуры:</p>
                          <select name='класс_арматуры' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['класс_арматуры']}>
                            <option value='A240'>А240</option>
                            <option value='A400'>А400</option>
                            <option value='A500'>А500</option>
                            <option value='A600'>А600</option>
                            <option value='A800'>А800</option>
                            <option value='A1000'>А1000</option>
                          </select>
                        </div>
                        <div className="d-flex  justify-content-between mt-2">
                          <p className="lh-30 mb-0">Класс бетона:</p>
                          <select name='класс_бетона' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['класс_бетона']}>
                            <option value='B15'>B15</option>
                            <option value='B20'>B20</option>
                            <option value='B25'>B25</option>
                            <option value='B30'>B30</option>
                            <option value='B35'>B35</option>
                            <option value='B40'>B40</option>
                            <option value='B45'>B45</option>
                            <option value='B50'>B50</option>
                            <option value='B55'>B55</option>
                            <option value='B60'>B60</option>
                            <option value='B70'>B70</option>
                            <option value='B80'>B80</option>
                            <option value='B90'>B90</option>
                            <option value='B100'>B100</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="d-flex justify-content-between">
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:"1"})} >
                            <img width='25px' height='25px' className={'mr-2'} src={leftArrow} alt=""/>
                            <span className={'mr-2 fz-14'}><b>К Нагрузкам</b></span>
                          </div>
                        </div>
                      </div>
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex + 1})}>
                            <span className={'mr-2 fz-14'}><b>К особенностям здания</b></span>
                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </TabPanel>
                  <TabPanel>
                    <ConstructProperties context={this}/>
                    <div className="d-flex justify-content-between">
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex - 1})} >
                            <img width='25px' height='25px'  className={'mr-2'} src={leftArrow} alt=""/>
                            <span className={'mr-2 fz-14'}><b>К характеристикам фундамента</b></span>
                          </div>
                        </div>
                      </div>
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex + 1})}>
                            <span className={'mr-2 fz-14'}><b>К параметрам подколонника</b></span>
                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </TabPanel>
                  <TabPanel>
                    <div className={'row'}>
                      <div className="col-md-7 pb-5">
                        <p className="mb-0 mt-3"><b><ins>Сопряжение с фундаментом:</ins></b></p>
                        <ToggleButtonGroup className='mt-2 w-100' type="radio" name="тип_колонны" onChange={this.toggleButtonsGroupHandler} value={this.app.state['тип_колонны']}>
                          <ToggleButton size="sm" variant="custom" value={'монолитная'}>Монолитную Ж/Б</ToggleButton>
                          <ToggleButton size="sm" variant="custom" value={'металлическая'}>Металлическую</ToggleButton>
                          <ToggleButton size="sm" variant="custom" value={'сборная'}>Сборную Ж/Б</ToggleButton>
                        </ToggleButtonGroup>
                        {(this.app.state['тип_колонны'] === 'монолитная') &&
                        <div className="mt-3">
                          {(this.app.state['форма_подошвы'] !== 'круглая') &&
                          <div>
                            <div>
                              <CustomNumericInput
                                  name={'lc'}
                                  className="form-control input-text"
                                  value={this.app.state['lc']}
                                  allowNegative={false}
                                  measure={'[м]'}
                                  precision={2}
                                  labelLeft
                                  label={"Длина подколонника l<sub>c</sub>:"}
                                  min={0}
                                  step={0.1}
                                  onValidate={this.validateField}
                                  enabledHandlerOnInput={true}
                                  isValid={!this.app.state.errors.hasOwnProperty('lc')}
                                  onChange={this.customNumericInputHandler}
                              />
                            </div>
                            <div className="mt-2">
                              <CustomNumericInput
                                  name={'bc'}
                                  className="form-control input-text"
                                  value={this.app.state['bc']}
                                  allowNegative={true}
                                  measure={'[м]'}
                                  precision={2}
                                  labelLeft
                                  label={"Ширина подколонника b<sub>c</sub>:"}
                                  min={0}
                                  step={0.1}
                                  onValidate={this.validateField}
                                  enabledHandlerOnInput={true}
                                  isValid={!this.app.state.errors.hasOwnProperty('bc')}
                                  onChange={this.customNumericInputHandler}
                              />
                            </div>
                          </div>
                          }
                          {(this.app.state['форма_подошвы'] === 'круглая') &&
                          <div>
                            {(this.app.state['тип_подколонника'] === 'круглый') &&
                            <CustomNumericInput
                                name={'LBc'}
                                className="form-control input-text"
                                value={this.app.state['LBc']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                labelLeft
                                label={"Диаметр круглого подколонника"}
                                min={0}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('LBc')}
                                onChange={this.customNumericInputHandler}
                            />
                            }
                            {(this.app.state['тип_подколонника'] === 'прямоугольный') &&
                            <div>
                              <div>
                                <CustomNumericInput
                                    name={'lc'}
                                    className="form-control input-text"
                                    value={this.app.state.lc}
                                    allowNegative={false}
                                    measure={'[м]'}
                                    precision={2}
                                    labelLeft
                                    label={"Длина подколоника:"}
                                    min={0}
                                    step={0.1}
                                    onValidate={this.validateField}
                                    enabledHandlerOnInput={true}
                                    isValid={!this.app.state.errors.hasOwnProperty('lc')}
                                    onChange={this.customNumericInputHandler}
                                />
                              </div>
                              <div className="mt-2">
                                <CustomNumericInput
                                    name={'bc'}
                                    className="form-control input-text"
                                    value={this.app.state.bc}
                                    allowNegative={false}
                                    measure={'[м]'}
                                    precision={2}
                                    labelLeft
                                    label={"Ширина подколоника:"}
                                    step={0.1}
                                    onValidate={this.validateField}
                                    enabledHandlerOnInput={true}
                                    isValid={!this.app.state.errors.hasOwnProperty('bc')}
                                    onChange={this.customNumericInputHandler}
                                />
                              </div>
                            </div>
                            }
                            <p className={'mb-0'}>Тип подколонника</p>
                            <ToggleButtonGroup className='mt-2 w-100' type="radio" onChange={this.toggleButtonsGroupHandler} name="тип_подколонника" value={this.app.state['тип_подколонника']}>
                              <ToggleButton size="sm" variant="custom" value='круглый'>Круглый</ToggleButton>
                              <ToggleButton size="sm" variant="custom" value='прямоугольный'>Прямоугольный</ToggleButton>
                            </ToggleButtonGroup>
                          </div>
                          }
                        </div>
                        }
                        {(this.app.state['тип_колонны'] === 'сборная') &&
                        <div className="mt-3">
                          <div>
                            <CustomNumericInput
                                name={'lc'}
                                className="form-control input-text"
                                value={this.app.state['lc']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                labelLeft
                                label={"Длина колонны l : "}
                                min={0}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('lc')}
                                onChange={this.customNumericInputHandler}
                            />
                          </div>
                          <div className="mt-2">
                            <CustomNumericInput
                                name={'bc'}
                                className="form-control input-text"
                                value={this.app.state['bc']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                labelLeft
                                label={"Ширина колонны b : "}
                                min={0}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('bc')}
                                onChange={this.customNumericInputHandler}
                            />
                          </div>
                          <div className="mt-2">
                            <CustomNumericInput
                                name={'dc'}
                                className="form-control input-text"
                                value={this.app.state['dc']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                labelLeft
                                label={"Глубина заделки колонны в стакан dc : "}
                                step={0.1}
                                min={0}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('dc')}
                                onChange={this.customNumericInputHandler}
                            />
                          </div>
                        </div>
                        }
                        {(this.app.state['тип_колонны'] === 'металлическая') &&
                        <div className="mt-3">
                          <div className="d-flex justify-content-between">
                            <p className="lh-30 mb-0 mr-5">Конструкция колонны:</p>
                            <ToggleButtonGroup type="radio" name="тип_металлической_колонны" onChange={this.toggleButtonsGroupHandler} value={this.app.state['тип_металлической_колонны']}>
                              <ToggleButton size="sm" variant="custom" value='двухветвевая'>Двухветвевая</ToggleButton>
                              <ToggleButton size="sm" variant="custom" value='одноветвевая'>Одноветвевая</ToggleButton>
                            </ToggleButtonGroup>
                          </div>
                          {(this.app.state['тип_металлической_колонны'] === 'одноветвевая') &&
                          <div className="d-flex justify-content-between mt-3">
                            <p className="lh-30 mb-0 mr-5">База колонны:</p>
                            <ToggleButtonGroup type="radio" name="база_колонны" onChange={this.toggleButtonsGroupHandler} value={this.app.state['база_колонны']}>
                              <ToggleButton size="sm" variant="custom" value='высокая'>С высокой траверсой</ToggleButton>
                              <ToggleButton size="sm" variant="custom" value='низкая'>С низкой траверсой</ToggleButton>
                            </ToggleButtonGroup>
                          </div>
                          }
                          <div className="d-flex justify-content-between mt-3">
                            <p className="lh-30 mb-0 mr-5">Нагрузка на колонну:</p>
                            <ToggleButtonGroup type="radio" name="нагрузка_на_колонну" onChange={this.toggleButtonsGroupHandler} value={this.app.state['нагрузка_на_колонну']}>
                              <ToggleButton size="sm" variant="custom" value='статическая'>Статическая</ToggleButton>
                              <ToggleButton size="sm" variant="custom" value='динамическая'>Динамическая</ToggleButton>
                            </ToggleButtonGroup>
                          </div>

                          {(this.app.state['нагрузка_на_колонну'] === 'динамическая') &&
                          <div className='d-flex justify-content-between mt-3'>
                            <p className="lh-30 mb-0 mr-5">Число циклов нагружений:</p>
                            <select name='alpha' className='fz-14 form-control w-25' onChange={this.paramsHandler} value={this.app.state.alpha}>
                              <option value='3.15'>50 тыс.</option>
                              <option value='2.25'>200 тыс.</option>
                              <option value='1.57'>800 тыс.</option>
                              <option value='1.25'>2 млн.</option>
                              <option value='1'>5 млн. и более</option>
                            </select>
                          </div>
                          }

                          <div className='mt-3'>
                            <p className="lh-30 mb-0 mr-5">Конструкция болта: <b>{this.app.state['конструкция_болта']} </b></p>
                            <div className="d-flex mt-2 justify-content-between" onChange={this.paramsHandler} >
                              <input type="radio" name='конструкция_болта' id='bolt11' value='конический' checked={this.app.state['конструкция_болта'] === 'конический'} />
                              <label htmlFor="bolt11" >
                                <img src={anchor1} alt="" className="anchor-img"/>
                              </label>
                              <input type="radio" name='конструкция_болта' id='bolt22' value='прямой' checked={this.app.state['конструкция_болта'] === 'прямой'}/>
                              <label htmlFor="bolt22" >
                                <img src={anchor2} alt="" className="anchor-img"/>
                              </label>
                              <input type="radio" name='конструкция_болта' id='bolt33' value='с отгибом' checked={this.app.state['конструкция_болта'] === 'с отгибом'}/>
                              <label htmlFor="bolt33">
                                <img src={anchor3} alt="с анкерной плитой съемный" className="anchor-img"/>
                              </label>
                              <input type="radio" name='конструкция_болта' id='bolt44' value='с анкерной плитой съемный' checked={this.app.state['конструкция_болта'] === 'с анкерной плитой съемный'}/>
                              <label htmlFor="bolt44">
                                <img src={anchor4} alt="" className="anchor-img"/>
                              </label>
                              <input type="radio" name='конструкция_болта' id='bolt55' value='с анкерной плитой глухой' checked={this.app.state['конструкция_болта'] === 'с анкерной плитой глухой'}/>
                              <label htmlFor="bolt55" >
                                <img src={anchor5} alt="" className="anchor-img"/>
                              </label>
                            </div>
                          </div>
                          <div className="mt-3 d-flex justify-content-between">
                            <p className="lh-30 mb-0 mr-5">Марка стали болта:</p>
                            <select name='марка_стали' className='fz-14 form-control w-25' onChange={this.paramsHandler} value={this.app.state['марка_стали']} >
                              <option value='Ст3пс2'>Ст3пс2</option>
                              <option value='Ст3пс4'>Ст3пс4</option>
                              <option value='Ст3сп4'>Ст3сп4</option>
                              <option value='Ст3сп2'>Ст3сп2</option>
                              <option value='09Г2С-4'>09Г2С-4</option>
                              <option value='09Г2С-6'>09Г2С-6</option>
                            </select>
                          </div>
                          {(this.app.state['тип_металлической_колонны'] === 'двухветвевая') &&
                          <div>
                            <div className="mt-3 d-flex justify-content-between">
                              <p className="lh-30 mb-0 mr-5">Количество болтов на одной ветви:</p>
                              <select
                                  name='n'
                                  className='fz-14 form-control w-25'
                                  onChange={this.paramsHandler}
                                  value={this.app.state.n}
                                  onMouseOver={this.svgTwoBranchesHandler}
                                  onMouseOut={this.svgTwoBranchesHandler}
                              >
                                <option value={2}>2</option>
                                <option value={4}>4</option>
                                <option value={6}>6</option>
                                <option value={8}>8</option>
                              </select>
                            </div>
                            <div className="mt-3">
                              <CustomNumericInput
                                  name={'k'}
                                  className="form-control input-text"
                                  value={this.app.state.k}
                                  allowNegative={false}
                                  measure={'[м]'}
                                  precision={2}
                                  labelLeft
                                  label={"Растояние в осях между болтами k:"}
                                  min={0}
                                  step={0.1}
                                  onValidate={this.validateField}
                                  enabledHandlerOnInput={true}
                                  onMouseOver={this.svgTwoBranchesHandler}
                                  onMouseOut={this.svgTwoBranchesHandler}
                                  isValid={!this.app.state.errors.hasOwnProperty('k')}
                                  onChange={this.customNumericInputHandler}
                              />
                            </div>
                            <div className="mt-3">
                              <CustomNumericInput
                                  name={'h'}
                                  className="form-control input-text"
                                  value={this.app.state['h']}
                                  allowNegative={false}
                                  measure={'[м]'}
                                  precision={2}
                                  labelLeft
                                  label={"Расстояние в осях между ветвей h:"}
                                  min={0}
                                  step={0.1}
                                  onMouseOver={this.svgTwoBranchesHandler}
                                  onMouseOut={this.svgTwoBranchesHandler}
                                  onValidate={this.validateField}
                                  enabledHandlerOnInput={true}
                                  isValid={!this.app.state.errors.hasOwnProperty('h')}
                                  onChange={this.customNumericInputHandler}
                              />
                            </div>
                            {(this.app.state.n > 2) &&
                            <div className="mt-2">
                              <CustomNumericInput
                                  name={'c_two'}
                                  className="form-control input-text"
                                  value={this.app.state.c_two}
                                  allowNegative={false}
                                  measure={'[м]'}
                                  precision={2}
                                  labelLeft
                                  label={"Расстояние по осям между крайними болтами ветвей c:"}
                                  min={0}
                                  step={0.1}
                                  onValidate={this.validateField}
                                  enabledHandlerOnInput={true}
                                  onMouseOver={this.svgTwoBranchesHandler}
                                  onMouseOut={this.svgTwoBranchesHandler}
                                  isValid={!this.app.state.errors.hasOwnProperty('c_two')}
                                  onChange={this.customNumericInputHandler}
                              />
                            </div>
                            }
                          </div>
                          }
                          {(this.app.state['тип_металлической_колонны'] === 'одноветвевая') &&
                          <div>
                            <div className="mt-3 d-flex justify-content-between">
                              <p className="lh-30 mb-0 mr-5">Количество растянутых болтов:</p>
                              <select
                                  name='n'
                                  className='fz-14 form-control w-25'
                                  onChange={this.paramsHandler}
                                  value={this.app.state.n}
                                  onMouseOver={this.svgOneBranchHandler}
                                  onMouseOut={this.svgOneBranchHandler}
                              >
                                <option value={1}>1</option>
                                <option value={2}>2</option>
                                <option value={3}>3</option>
                                <option value={4}>4</option>
                              </select>
                            </div>
                            <div className="mt-3">
                              <CustomNumericInput
                                  name={'bp'}
                                  className="form-control input-text m-0"
                                  value={this.app.state['bp']}
                                  allowNegative={false}
                                  measure={'[м]'}
                                  precision={2}
                                  labelLeft
                                  label={"Ширина анкерной плитки b : "}
                                  min={0}
                                  step={0.1}
                                  onMouseOver={this.svgOneBranchHandler}
                                  onMouseOut={this.svgOneBranchHandler}
                                  onValidate={this.validateField}
                                  enabledHandlerOnInput={true}
                                  isValid={!this.app.state.errors.hasOwnProperty('bp')}
                                  onChange={this.customNumericInputHandler}
                              />
                            </div>
                            <div className="mt-3">
                              <CustomNumericInput
                                  name={'lp'}
                                  className="form-control input-text"
                                  value={this.app.state['lp']}
                                  allowNegative={false}
                                  measure={'[м]'}
                                  labelLeft
                                  label={"Длинна анкерной плитки l : "}
                                  min={0}
                                  step={0.1}
                                  precision={2}
                                  onValidate={this.validateField}
                                  enabledHandlerOnInput={true}
                                  onMouseOver={this.svgOneBranchHandler}
                                  onMouseOut={this.svgOneBranchHandler}
                                  isValid={!this.app.state.errors.hasOwnProperty('lp')}
                                  onChange={this.customNumericInputHandler}
                              />
                            </div>


                            {(this.app.state['база_колонны'] === 'высокая') &&

                            <div>

                              <div className="mt-3">
                                <CustomNumericInput
                                    name={'la'}
                                    className="form-control input-text"
                                    value={this.app.state.la}
                                    allowNegative={false}
                                    measure={'[м]'}
                                    labelLeft
                                    label={"Расстояние от оси колонны до оси болта la:"}
                                    min={0}
                                    precision={3}
                                    step={0.1}
                                    onMouseOver={this.svgOneBranchHandler}
                                    onMouseOut={this.svgOneBranchHandler}
                                    onValidate={this.validateField}
                                    enabledHandlerOnInput={true}
                                    isValid={!this.app.state.errors.hasOwnProperty('la')}
                                    onChange={this.customNumericInputHandler}
                                />
                              </div>
                              {this.app.state.n > 1 &&
                                <div className="mt-3">
                                  <CustomNumericInput
                                      name={'ba'}
                                      className="form-control input-text"
                                      value={this.app.state.ba}
                                      allowNegative={false}
                                      measure={'[м]'}
                                      labelLeft
                                      label={"Расстояние от оси колонны до оси болта ba:"}
                                      min={0}
                                      precision={3}
                                      step={0.1}
                                      onMouseOver={this.svgOneBranchHandler}
                                      onMouseOut={this.svgOneBranchHandler}
                                      onValidate={this.validateField}
                                      enabledHandlerOnInput={true}
                                      isValid={!this.app.state.errors.hasOwnProperty('ba')}
                                      onChange={this.customNumericInputHandler}
                                  />
                                </div>
                              }

                            </div>
                            }


                            {(this.app.state['база_колонны'] === 'низкая') &&
                            <div>
                              <div className="mt-3">
                                <CustomNumericInput
                                    name={'c'}
                                    className="form-control input-text"
                                    value={this.app.state.c}
                                    allowNegative={false}
                                    measure={'[м]'}
                                    precision={3}
                                    labelLeft
                                    label={"Расстояние от оси болта до оси колонны с:"}
                                    min={0}
                                    step={0.1}
                                    onMouseOver={this.svgOneBranchHandler}
                                    onMouseOut={this.svgOneBranchHandler}
                                    onValidate={this.validateField}
                                    enabledHandlerOnInput={true}
                                    isValid={!this.app.state.errors.hasOwnProperty('c')}
                                    onChange={this.customNumericInputHandler}
                                />
                              </div>
                              {(this.app.state.n > 1) &&
                              <div className="mt-3">
                                <CustomNumericInput
                                    name={'c_'}
                                    className="form-control input-text"
                                    value={this.app.state.c_}
                                    allowNegative={false}
                                    measure={'[м]'}
                                    precision={3}
                                    labelLeft
                                    label={"Расстояние от оси болта до оси колонны с':"}
                                    onMouseOver={this.svgOneBranchHandler}
                                    onMouseOut={this.svgOneBranchHandler}
                                    min={0}
                                    step={0.1}
                                    onValidate={this.validateField}
                                    enabledHandlerOnInput={true}
                                    isValid={!this.app.state.errors.hasOwnProperty('c_')}
                                    onChange={this.customNumericInputHandler}
                                />
                              </div>
                              }

                            </div>
                            }
                          </div>
                          }
                        </div>
                        }
                      </div>
                      <div className="col-md-5 position-relative">
                        {(this.app.state['тип_колонны'] === 'металлическая') &&
                        <div className="imgS mt-5" >
                          <object id="svg-object"  data={twoBranches} type="image/svg+xml" />
                          <object id="lowTraversaSvg" data={lowTraversa} type="image/svg+xml" />
                          <object id="highTraversaSvgL" data={highTraversaSvgL} type="image/svg+xml" />
                          <object id="highTraversaSvgB" data={highTraversaSvgB} type="image/svg+xml" />
                        </div>
                        }
                      </div>
                    </div>
                    <div className="d-flex justify-content-between">
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex - 1})} >
                            <img width='25px' height='25px' className={'mr-2'} src={leftArrow} alt=""/>
                            <span className={'mr-2 fz-14'}><b>К особенностям здания</b></span>
                          </div>
                        </div>
                      </div>
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:"2"})}>
                            <span className={'mr-2 fz-14'}><b>К Геологии</b></span>
                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                          </div>
                        </div>
                      </div>
                    </div>

                  </TabPanel>
                </Tabs>
              </div>
            </div>


        </div>
    );
  }
}

export default PillarFoundationSolve;
