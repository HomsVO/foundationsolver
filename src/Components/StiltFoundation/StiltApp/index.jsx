import 'rsuite/dist/styles/rsuite-dark.min.css';
import React, { Component } from 'react';
import StiltLoads from '../StiltLoads'
import deleteIcon from '../../../img/trash.svg';
import StiltSurveys from '../StiltSurveys'
import SurveysTable from '../../SurveysTable'
import StiltFoundationProps from '../StiltFoundationProps/index.jsx'
import StiltReport from '../StiltReport/index.jsx'
import Errors from '../../Errors/index.jsx'
import { Notification } from 'rsuite';
import { isEmpty } from "../../Global/Global";
import 'bootstrap/dist/css/bootstrap.min.css';
import rightArrow from "../../../img/right-arrow.svg";
import leftArrow from "../../../img/left-arrow.svg";



import './index.css';

// const URL = 'http://127.0.0.1:8000';
const URL = 'http://foundation-ru.1gb.ru';


class StiltApp extends Component {

    state = {
        "loadsData": [
            {
                "id": 1,
                "title": "Собственный вес",
                "N": "5000",
                "Mx": "0",
                "My": "0",
                "Qx": "0",
                "Qy": "0",
                "type": "постоянная"
            },
            {
                "id": 2,
                "title": "Полезные",
                "N": "125",
                "Mx": "185",
                "My": "40",
                "Qx": "10",
                "Qy": "5",
                "type": "длительная"
            },
            {
                "id": 3,
                "title": "Снег",
                "N": "10",
                "Mx": "14",
                "My": "8",
                "Qx": "0",
                "Qy": "0",
                "type": "кратковременная"
            },
            {
                "id": 4,
                "title": "Ветер",
                "N": "2",
                "Mx": "9",
                "My": "4",
                "Qx": "15",
                "Qy": "10",
                "type": "кратковременная"
            }
        ],
        "surveysData": [
            {
                "глубина_залегания": 4.2,
                "подтип_грунта": "",
                "тип_грунта": "суглинок",
                "E_1": "",
                "Ee_1": "",
                "Esr_1": "",
                "Il_1": "0.67",
                "Ip_1": "12.4",
                "Sr_1": "0.63",
                "e_1": "0.85",
                "R_1": "",
                "filter_1": "",
                "particles_1": "",
                "Угол_внутреннего_трения_1": "",
                "влажность_Wl_1": "",
                "влажность_Wp_1": "",
                "плотность_p_1": "",
                "плотность_pd_1": "",
                "плотность_ps_1": "2.69",
                "природная_влажность_W_1": "",
                "сцепление_C_1": "",
                "E_2": "14.1",
                "Ee_2": "",
                "Esr_2": "4.7",
                "Il_2": "",
                "Ip_2": "",
                "Sr_2": "",
                "e_2": "",
                "R_2": "",
                "filter_2": "",
                "particles_2": "",
                "Угол_внутреннего_трения_2": "17",
                "влажность_Wl_2": "",
                "влажность_Wp_2": "",
                "плотность_p_2": "1.73",
                "плотность_pd_2": "",
                "плотность_ps_2": "",
                "природная_влажность_W_2": "",
                "сцепление_C_2": "13",
                "E_3": "",
                "Ee_3": "",
                "Esr_3": "",
                "Il_3": "",
                "Ip_3": "",
                "Sr_3": "",
                "e_3": "",
                "R_3": "",
                "filter_3": "",
                "particles_3": "",
                "Угол_внутреннего_трения_3": "",
                "влажность_Wl_3": "",
                "влажность_Wp_3": "",
                "плотность_p_3": "",
                "плотность_pd_3": "",
                "плотность_ps_3": "",
                "природная_влажность_W_3": "",
                "сцепление_C_3": ""
            },
            {
                "глубина_залегания": 2.4,
                "подтип_грунта": "",
                "тип_грунта": "суглинок",
                "E_1": "",
                "Ee_1": "",
                "Esr_1": "",
                "Il_1": "0.60",
                "Ip_1": "14.1",
                "Sr_1": "0.87",
                "e_1": "0.93",
                "R_1": "",
                "filter_1": "",
                "particles_1": "",
                "Угол_внутреннего_трения_1": "",
                "влажность_Wl_1": "",
                "влажность_Wp_1": "",
                "плотность_p_1": "",
                "плотность_pd_1": "",
                "плотность_ps_1": "2.69",
                "природная_влажность_W_1": "",
                "сцепление_C_1": "",
                "E_2": "",
                "Ee_2": "",
                "Esr_2": "5.3",
                "Il_2": "",
                "Ip_2": "",
                "Sr_2": "",
                "e_2": "",
                "R_2": "",
                "filter_2": "",
                "particles_2": "",
                "Угол_внутреннего_трения_2": "17",
                "влажность_Wl_2": "",
                "влажность_Wp_2": "",
                "плотность_p_2": "1.80",
                "плотность_pd_2": "",
                "плотность_ps_2": "",
                "природная_влажность_W_2": "",
                "сцепление_C_2": "15",
                "E_3": "",
                "Ee_3": "",
                "Esr_3": "",
                "Il_3": "",
                "Ip_3": "",
                "Sr_3": "",
                "e_3": "",
                "R_3": "",
                "filter_3": "",
                "particles_3": "",
                "Угол_внутреннего_трения_3": "",
                "влажность_Wl_3": "",
                "влажность_Wp_3": "",
                "плотность_p_3": "",
                "плотность_pd_3": "",
                "плотность_ps_3": "",
                "природная_влажность_W_3": "",
                "сцепление_C_3": ""
            },
            {
                "глубина_залегания": 3.8,
                "подтип_грунта": "",
                "тип_грунта": "суглинок",
                "E_1": "",
                "Ee_1": "",
                "Esr_1": "",
                "Il_1": "0.15",
                "Ip_1": "15.1",
                "Sr_1": "0.88",
                "e_1": "0.75",
                "R_1": "",
                "filter_1": "",
                "particles_1": "",
                "Угол_внутреннего_трения_1": "",
                "влажность_Wl_1": "",
                "влажность_Wp_1": "",
                "плотность_p_1": "",
                "плотность_pd_1": "",
                "плотность_ps_1": "2.69",
                "природная_влажность_W_1": "",
                "сцепление_C_1": "",
                "E_2": "",
                "Ee_2": "",
                "Esr_2": "11",
                "Il_2": "",
                "Ip_2": "",
                "Sr_2": "",
                "e_2": "",
                "R_2": "",
                "filter_2": "",
                "particles_2": "",
                "Угол_внутреннего_трения_2": "21",
                "влажность_Wl_2": "",
                "влажность_Wp_2": "",
                "плотность_p_2": "1.91",
                "плотность_pd_2": "",
                "плотность_ps_2": "",
                "природная_влажность_W_2": "",
                "сцепление_C_2": "23",
                "E_3": "",
                "Ee_3": "",
                "Esr_3": "",
                "Il_3": "",
                "Ip_3": "",
                "Sr_3": "",
                "e_3": "",
                "R_3": "",
                "filter_3": "",
                "particles_3": "",
                "Угол_внутреннего_трения_3": "",
                "влажность_Wl_3": "",
                "влажность_Wp_3": "",
                "плотность_p_3": "",
                "плотность_pd_3": "",
                "плотность_ps_3": "",
                "природная_влажность_W_3": "",
                "сцепление_C_3": ""
            },
            {
                "глубина_залегания": 4.2,
                "подтип_грунта": "",
                "тип_грунта": "суглинок",
                "E_1": "",
                "Ee_1": "",
                "Esr_1": "",
                "Il_1": "0.07",
                "Ip_1": "17.9",
                "Sr_1": "0.88",
                "e_1": "0.79",
                "R_1": "",
                "filter_1": "",
                "particles_1": "",
                "Угол_внутреннего_трения_1": "",
                "влажность_Wl_1": "",
                "влажность_Wp_1": "",
                "плотность_p_1": "",
                "плотность_pd_1": "",
                "плотность_ps_1": "2.70",
                "природная_влажность_W_1": "",
                "сцепление_C_1": "",
                "E_2": "",
                "Ee_2": "",
                "Esr_2": "13.3",
                "Il_2": "",
                "Ip_2": "",
                "Sr_2": "",
                "e_2": "",
                "R_2": "",
                "filter_2": "",
                "particles_2": "",
                "Угол_внутреннего_трения_2": "16",
                "влажность_Wl_2": "",
                "влажность_Wp_2": "",
                "плотность_p_2": "1.89",
                "плотность_pd_2": "",
                "плотность_ps_2": "",
                "природная_влажность_W_2": "",
                "сцепление_C_2": "33",
                "E_3": "",
                "Ee_3": "",
                "Esr_3": "",
                "Il_3": "",
                "Ip_3": "",
                "Sr_3": "",
                "e_3": "",
                "R_3": "",
                "filter_3": "",
                "particles_3": "",
                "Угол_внутреннего_трения_3": "",
                "влажность_Wl_3": "",
                "влажность_Wp_3": "",
                "плотность_p_3": "",
                "плотность_pd_3": "",
                "плотность_ps_3": "",
                "природная_влажность_W_3": "",
                "сцепление_C_3": ""
            }
        ],
        "tableSize": "normal",
        "tabs": "1",
        "нагрузки": "загружения",
        "привязка_к_абсолютным_отметкам": false,
        "класс_бетона": "B15",
        "город": 274,
        "глубина_слоя": 0.3,
        "YII_": 1.82,
        "Su": 15,
        "класс_арматуры": "A500",
        "initDataHTMLReport": true,
        "titlePageHTMLReport": true,
        "contentTitles": true,
        "планируемость_территории": false,
        "dw": 4.5,
        "errors": {},
        "тип_расчета": "solve",
        "наличие_подготовки": true,
        "поперечное_сечение": "квадратная",
        "продольное_сечение": "открытый",
        "lr": 5,
        "br": 5,
        "метод_определения": "расчет",
        "a": 0.4,
        "глубина_котлована": "1",
        "высота_оголовка": "1",
        "длина_сваи": "10",
        "hr": 4,
        "способ_погружения": "1",
        "низ_котлована": "",
        "отметка_низа": "",
        "Yn": 1.1,
        "sizeOptimize": false,
        "twoEdges": false,
        "геотехническая_категория": "1",
        "b": 0.2,
        "hs": "10",
        "hs0": 9,
        "lm":25,
        "am":0.8,
        "stype":"t1",
    };

    optionsSu = [];
    optionsCities = [];
    bolts = [];

  saveData = ()=>{ localStorage.setItem('data2',JSON.stringify(this.state));};
  deleteData = ()=>{ localStorage.removeItem('data2')};
  getData = () =>{ this.setState({...JSON.parse(localStorage.getItem('data2'))})};

  getSu(){
    fetch(URL + '/su')
        .then((response) => { return response.json(); })
        .then((myJson) => {
            myJson.forEach(c=>{
                this.optionsSu.push({
                    value:c.id,
                    Su:c.Su,
                    label:c.title,
                    isDisabled:c.isDisabled,
                    kren:c.iu,
                    type:c.type,
                })
            })
        })

  }
  getCities(){
        fetch(URL + '/cities')
            .then((response)=> { return response.json(); })
            .then((myJson) => {
                myJson.forEach(c => {
                    this.optionsCities.push({
                        value: c.id,
                        label: c.city
                    })
                })
            })
    }
  showErrorFields = (errors) =>{

      Object.keys(errors).forEach( key =>{
          let el = document.querySelector('input#'+key);
          let evt = new Event('blur');
          if(el) el.dispatchEvent(evt);

      });

  };

  validateDataAfterSubmit(){

      let errors = this.state.errors;

      // Валидация полей без таблицы

          Object.keys(this.state).forEach( key =>{
                const value = this.state[key];
                const name = key;

                let isValid = true;

                switch (name) {
                    case 'dw':
                        let deep = 0;
                        this.state.surveysData.forEach(item => deep+= item.глубина_залегания);
                        isValid = true;
                        if(deep < value && value !== 0) isValid = false;
                        if (value < 0) isValid = false;
                        break;
                    case 'Su':
                            if (value < 0) isValid = false;
                        break;
                    case 'глубина_слоя':

                            if(!this.state['планируемость_территории']) {
                                if (value > this.state['глубина_заложения'] && !isEmpty(this.state['глубина_заложения'])) {
                                    isValid = false;
                                }
                                if (isEmpty(value)) isValid = false;
                            }
                        break;
                    case 'YII_':
                        if (value > 2.5 || value < 0) isValid = false;
                        if (isEmpty(value))  isValid = false;
                        break;
                }

                if(!isValid) errors[name] = {};
                else delete  errors[name];
            });


      // Валидация таблицы
      if(this.state.surveysData.length > 0){
          let deep = 0;
          let ip = 0;
          this.state.surveysData.forEach(item => deep += item['глубина_залегания']);
          let dw = (this.state.dw <= deep && this.state.dw !== 0);

          this.state.surveysData.forEach( (item,index) => {
          if(this.state['поперечное_сечение'] === 'пирамидальная'){
              let a_ = (this.state['a'] - this.state['d'])/2;
              ip = a_/this.state['hs'];
          }
              Object.keys(item).forEach( key =>{

                  const gType = item['тип_грунта'];
                  const value = item[key];
                  const name = key + "_" + index;

                  let isClay = false;
                  const isNotRock = gType !== 'скальные';

                  let isValid = true;
                  switch (gType) {
                      case 'суглинок':
                      case 'глина':
                      case 'супесь':
                          isClay = true;
                          break;
                      case 'крупнообломочный грунт':
                          if(item['подтип_грунта'] === 'глинистый_заполнитель') isClay = true;
                          break;
                  }

                  switch (key) {
                      case 'Ip_1':
                      {
                          if(value < 0 || value > 100 || isEmpty(value) && isClay ) isValid = false;
                      }
                          break;
                      case 'Il_1':
                      {
                          if(value < -1 || value > 2 || isEmpty(value) && isClay ) isValid = false;
                      }
                          break;
                      case 'плотность_ps_1':
                      {
                          if(dw){
                              if(value < 1.5 || value > 3 || isEmpty(value) && isNotRock) isValid = false;
                          }
                      }
                          break;
                      case 'плотность_pd_1':
                      {
                          if(this.state.tableSize !== 'normal'){
                              if(value < 1.3 || value > 2.5 && isEmpty(value)) isValid = false;
                          }
                      }
                          break;
                      case 'e_1':
                      {
                          if(isNotRock && dw) {
                              if (value < 0.2 || value > 1 || isEmpty(value)) isValid = false;
                          }
                      }
                          break;
                      case 'Ee_2':
                      {
                          if(this.state['геотехническая_категория'] === '3' && isNotRock){
                              if(value < 1 || value > 250 || isEmpty(value)) isValid = false;
                          }
                      }
                          break;
                      case 'filter':
                      {
                          if(value < 0 || value > 100 || isEmpty(value)) isValid = false;
                      }
                          break;
                      case 'Sr_1':
                      {
                          if(isNotRock && value < 0 || value > 1 || isEmpty(value)) isValid = false;
                      }
                          break;
                      case 'Esr_2':
                      {
                          if(isNotRock){
                              if(value < 1 || value > 50 || isEmpty(value) && isNotRock) isValid = false;
                          }
                      }
                          break;
                      case 'плотность_p_2':
                      {
                          if(isNotRock){
                              if(value < 1.5 || value > 2.5 || isEmpty(value) && isNotRock) isValid = false;
                          }
                      }
                      break;
                      case 'плотность_p_3':
                      {
                          if(isNotRock && ip > 0.025){
                              if(value < 1.5 || value > 2.5 || isEmpty(value) && isNotRock) isValid = false;
                          }
                      }
                          break;
                      case 'E_2':
                      {
                          if(isNotRock){
                              if(value < 1 || value > 100 || isEmpty(value) && isNotRock) isValid = false;
                          }

                      }
                          break;
                      case 'сцепление_C_2':
                      {
                          if((value < 0.1 || value > 50 || isEmpty(value)) && isNotRock) isValid = false;
                      }
                          break;
                      case 'сцепление_C_3':
                      {
                          if((value < 1 || value > 50 || isEmpty(value)) && isNotRock && ip > 0.025) isValid = false;
                      }
                          break;
                      case 'Угол_внутреннего_трения_2':
                      {
                          if( isNotRock ){
                              if (value < 0 || value > 45 || isEmpty(value))  isValid = false;
                          }
                      }
                          break;
                      case 'Угол_внутреннего_трения_3':
                      {
                          if( isNotRock && ip > 0.025){
                              if (value < 0 || value > 45 || isEmpty(value))  isValid = false;
                          }
                      }
                          break;
                      case 'R_2':
                          if((value < 5 || value > 20 || isEmpty(value)) && !isNotRock) isValid = false;
                          break;
                      case 'particles':
                          if(this.state.tableSize !== 'normal'){
                              if(value < 0 || value > 100 || isEmpty(value) && isNotRock) isValid = false;
                          }

                          break;
                      case 'природная_влажность_W_1':
                      {
                          if(this.state.tableSize !== 'normal'){
                              if(value < 0.01 || value > 100 || isEmpty(value) && isNotRock) isValid = false;
                          }
                      }
                          break;
                      case 'влажность_Wl_1':
                      {
                          if(isNotRock && this.state.tableSize !== 'normal'){
                              if(value < 0 || value > 50 || isEmpty(value)) isValid = false;
                          }
                      }
                          break;
                      case 'влажность_Wp_1':
                      {
                          if(isNotRock && this.state.tableSize !== 'normal'){
                              if(value < 0 || value > 50 || isEmpty(value)) isValid = false;
                          }
                      }
                          break;
                      case 'filter_1':
                      {
                          if(this.state.tableSize !== 'normal') {
                              if (value < 0 || value > 100 || isEmpty(value)) isValid = false;
                          }
                      }
                          break;
                      case 'глубина_залегания':
                          if (value <= 0 || isEmpty(value)) isValid = false;
                          break;
                  }
                  if(!isValid) errors[name] = {};
                  else delete  errors[name];
              });
          });
      }
      this.showErrorFields(errors);


      this.setState({
          errors:errors
      });

  }

  tabsHandler = (e) => {
        this.setState({
            tabs:e.target.value
        });
  };


  checkGroundUnderStilts(){
      let hs0 = 0;
      let d = 0;
      let sumD = 0;
      if (this.state['привязка_к_абсолютным_отметкам']) {
          hs0 = Math.abs(Math.abs(this.state['отметка_низа']) - Math.abs(this.state['низ_котлована']));
      } else {
          hs0 = this.state['длина_сваи'] - this.state['высота_оголовка'];
      }
      let i = 0;
      this.state.surveysData.forEach(item => sumD+= item['глубина_залегания']);
      if(hs0 > sumD){
          i = this.state.surveysData.length - 1;
      }else{
          while(d < hs0) {
              d+= this.state.surveysData[i]['глубина_залегания'];
              if(d > hs0) break;
              i++;
          }
      }
      let currentLayer = this.state.surveysData[i];

      let allowGrounds = ['песок','глина','суглинок','супесь'];
      if(!allowGrounds.includes(currentLayer['тип_грунта'])) return false;

      if(currentLayer['тип_грунта'] === 'песок'){
         if(currentLayer['подтип_грунта'] === 'пылеватый' && currentLayer['e_1'] > 0.8) return true;
         if(currentLayer['подтип_грунта'] === 'мелкий' && currentLayer['e_1'] > 0.75) return true;
         if(currentLayer['e_1'] > 0.75) return true;
         return false;
      }
      if(currentLayer['тип_грунта'] !== 'глина' || currentLayer['тип_грунта'] !== 'суглинок' || currentLayer['тип_грунта'] !== 'супесь'){
          if(currentLayer['Il_1'] > 0.6) return true;
          return false;
      }

  }
  formSubmit = (e) => {
    e.preventDefault();

    this.validateDataAfterSubmit();
    let loadsIsValid = false;

    this.state.loadsData.forEach(item => {
        if (item.type === 'постоянная' || item.type === 'длительная') loadsIsValid = true;
    });

    let sumD = 0;
    this.state.surveysData.forEach(item => sumD+= item['глубина_залегания']);

    if(sumD < 10 && this.state['способ_погружения'] === '6'){
        Notification.error({
            title: 'Ошибка',
            duration: 40000,
            description: <div className={'notification-container'}>Сваи с камуфлетным уширением должны быть устроены на глубину 10 м или более.</div>,
        });
        return false;
    }

    if(!isEmpty(this.state.errors)) {

        Notification.error({
            title: 'Ошибка',
            duration: 40000,
            description: <Errors app={this}/>,
        });
        return false;
    }

    if(this.checkGroundUnderStilts()){
        Notification.error({
            title: 'Ошибка',
            duration: 40000,
            description: <div className={'notification-container'}> awdawd</div>,
        });
        return false;
    }


    if(this.state.surveysData.length === 0){
        Notification.error({
            title: 'Ошибка',
            duration: 40000,
            description: <div className={'notification-container'}> Не задано ни одного грунтового слоя</div>,
        });
        return false;
    }

    if(!loadsIsValid){
        Notification.error({
            title: 'Ошибка',
            duration: 40000,
            description: <div className={'notification-container'}> Должна быть задана хотя бы одна длительная или постоянная нагрузка</div>,
        });
        return false;
    }
    e.target.submit();
  };



  render() {

    return (
        <div className="stilt container-fluid p-0 root-container">
            <div className="container-fluid p-0">
                <div className="control-panel ">
                    <div className="d-flex">
                        <div className="btn-group btn-group--ml5" onChange={this.tabsHandler} role="group" aria-label="Basic example">
                            <input type="radio" name='tabs' id='control1' value='1' checked={this.state.tabs === '1'}  />
                            <label htmlFor="control1" className="btn btn-control">Сбор нагрузок на фундамент</label>
                            <input type="radio" name='tabs' id='control3' value='3' checked={this.state.tabs === '3'}/>
                            <label htmlFor="control3" className="btn btn-control">Параметры фундамента</label>
                            <input type="radio" name='tabs' id='control2' value='2' checked={this.state.tabs === '2'}/>
                            <label htmlFor="control2" className="btn btn-control">Инженерно-геологические изыскания</label>
                            <input type="radio" name='tabs' id='control4' value='4' checked={this.state.tabs === '4'}/>
                            <label htmlFor="control4" className="btn btn-control">Параметры отчета</label>
                        </div>
                        <div className="btn-group btn-group--ml5" role="group" aria-label="Basic example">
                            <a href='#' className='btn btn-primary my-5px' onClick={this.saveData}>Сохранить данные!</a>
                            <a href='#' className='btn btn-primary my-5px' onClick={this.deleteData}>
                                <img src={deleteIcon} style={{width:'15px',height:'100%'}} alt=""/>
                            </a>
                        </div>
                        <div role="group" className='btn-group btn-group--ml5 my-5px' aria-label="Basic example">
                            <form method="POST" onSubmit={this.formSubmit} target="_blank" action={URL + '/' + this.state['тип_расчета'] + 'Stilt'}>
                                <button type='submit'  className='mx-1 btn btn-success' > Получить отчет </button>
                                <input type='text' className='hidden' name='data' value={JSON.stringify(this.state)}/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
          <div className='container-fluid'>
               <StiltLoads app={this}/>
               <div style={{display: (this.state.tabs === '2')?'block':'none'}} className={'mt-2'}>
                    <StiltSurveys data={this.state} app={this} />
                    <SurveysTable data={this.state} isStilt app={this}/>
                   <div className="d-flex justify-content-between mt-3">
                       <div className="position-relative z-index-top mr-3">
                           <div className={'mb-5 pointer'} >
                               <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabs:"3"})} >
                                   <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt="К Параметрам"/>
                                   <span className={'mr-2 fz-14'}><b>К Параметрам</b></span>
                               </div>
                           </div>
                       </div>
                       <div className="position-relative z-index-top ml-3">
                           <div className={'mb-5 pointer'} >
                               <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabs:"4"})}>
                                   <span className={'mr-2 fz-14'}><b>К Отчету</b></span>
                                   <img width='25px' height='25px' src={rightArrow} alt="К отчету"/>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
              <StiltFoundationProps app={this} />
              <StiltReport app={this}/>
          </div>
    </div>
    );
  }
  componentDidMount() {

      this.getData();
      this.getSu(URL);
      this.getCities(URL);
  }
}

export default StiltApp;
