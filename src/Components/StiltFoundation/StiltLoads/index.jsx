import React, { Component } from 'react';
import { ToggleButton,ToggleButtonGroup,Form,Row } from 'react-bootstrap'
import scheme from '../../../img/stiltMomentsScheme.svg';
import LoadsTable from '../StiltLoadsTable';
import $ from 'jquery';
import { Notification } from 'rsuite/lib/index';
import './index.css';
import rightArrow from '../../../img/right-arrow.svg'
import leftLoadsArrow from "../../../img/leftLoadsArrow.svg";
import rightLoadsArrow from "../../../img/rightLoadsArrow.svg";

class StiltLoads extends Component {
  state = {
    loads:'загружения',
    schemeOpen:true
  };
  
  schemeHandler = () =>this.setState({schemeOpen:!this.state.schemeOpen});
  radioHandler = (e) => this.setState({loads:e});

  app = this.props.app;

  upload = (file) => {
      var fd = new FormData();
      fd.append('loadsFiles', file);

      $.ajax({
          url: 'http://foundation-ru.1gb.ru/loads',
          data: fd,
          processData: false,
          contentType: false,
          type: 'POST',
          success: (data) => {
             this.app.setState({loadsData:data});
          },
          error: () => {
              Notification.error({
                  title: 'Ошибка',
                  duration: 40000,
                  description: <div>
                      <p>Проверте корректность загружаемых данных</p>
                  </div>,
              });
          }
      });

    };

  cleanInputFile = (e) => {
      e.target.value = null;
  };
  uploadLoads = (e) =>{
      this.upload(e.target.files[0]);
  };
  nextTab = () => {
      this.app.setState({
          tabs:'3',
      });
  };
  render() {

    let arrow = (this.state.schemeOpen)? rightLoadsArrow : leftLoadsArrow;
    return (
        <div style={{display: (this.app.state.tabs === '1')?'block':'none'}} className="pb-5 container-loads container-fluid" >
          <Form.Group className='mt-5 d-flex' onChange={this.props.paramsHandler}>
            <ToggleButtonGroup type="radio" name="нагрузки" onChange={this.radioHandler} value={this.state.loads}>
                <ToggleButton size="sm" variant="custom" value={'загружения'}>Ввод загружений</ToggleButton>
                <ToggleButton size="sm" variant="custom" disabled value={'сочетания'}>Ввод наихудших сочетаний</ToggleButton>
              </ToggleButtonGroup>
              <input type='file' name='loads' id={'loadsFile'} className={'hide'} onClick={this.cleanInputFile} onChange={this.uploadLoads}/>
              <label htmlFor="loadsFile"><span>Импорт загружений</span></label>
              <label className={'download-loads'}><a href="http://foundation-ru.1gb.ru/template.xlsx">Скачать шаблон</a></label>
          </Form.Group>
          <Row className={(this.state.loads === 'сочетания')?'mt-4 p-0 disabled':'mt-4 p-0'}>
            <div className='loads-table-wrapper' style={(this.state.schemeOpen === true)?{width:"auto"}:{width:'95%'}}>
              <LoadsTable app={this.app}/>
            </div>
              <div onClick={this.schemeHandler} className='btn-spoiler mr-1 ml-3 schemeButton' ><img src={arrow} alt=""/> </div>
            <object id="loads-scheme" data={scheme} type="image/svg+xml" width={(this.state.schemeOpen === true)?'400':'0'} height='352'></object>
          </Row>
            <div className="text-right mt-3">
                <div className="position-relative z-index-top">
                    <div className={'mb-5 pointer'} >
                        <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:'3'})}>
                            <span className={'mr-2 fz-14'}><b>К Параметрам</b></span>
                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

export default StiltLoads;
