import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-select/dist/css/bootstrap-select.min.css';
import './index.css';
import CustomNumericInput from "../../CustomNumericInput/CustomNumericInput";
import ConstructProperties from "../../ConstructProperties/index.jsx";
import {ToggleButton, ToggleButtonGroup} from "react-bootstrap";
import {Checkbox, Popover, Whisper} from "rsuite";

import anchor1 from "../../../img/anchor1.png";
import anchor2 from "../../../img/anchor2.png";
import anchor5 from "../../../img/anchor3.png";
import anchor4 from "../../../img/anchor4.png";
import anchor3 from "../../../img/anchor5.png";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';


import highTraversaSvgL from '.././../../img/highTraversaSvgL.svg';
import highTraversaSvgB from '.././../../img/highTraversaSvgB.svg';
import lowTraversa from '.././../../img/lowTraversa.svg';
import twoBranches from '.././../../img/twoBranches.svg';
import {isEmpty} from "../../Global/Global";
import leftArrow from "../../../img/left-arrow.svg";
import rightArrow from "../../../img/right-arrow.svg";

class PillarFoundationCheck extends Component {

    state ={
        su:null,
        selectedOption:{
            Su: 10,
            isDisabled: 0,
            kren: "",
            label: "то же. с устройством железобетонных поясов или монолитных перекрытий. а также здания монолитной конструкции",
            value: 3
        },
        tabIndex:0,
        tabIndex2:0,
    }

    wrapper = this.props.wrapper;
    app = this.props.app;

    options = this.app.optionsSu;
    svgTwoBranchesHandler = this.wrapper.svgTwoBranchesHandler
    svgOneBranchHandler = this.wrapper.svgOneBranchHandler;
    paramsHandler = this.wrapper.paramsHandler;
    toggleButtonsGroupHandler = this.wrapper.toggleButtonsGroupHandler;

    customSelectHandler = (e) => {
        this.app.setState({ 'Su':e.Su });
        this.setState({selectedOption:e});
        if(!isEmpty(e.kren)){
            this.app.setState({
                'крен':true,
                iu:e.kren,
            })
        }else{
            this.app.setState({
                'крен':false,
                iu:'',
            })
        }
    };
    renderAnchorBoltsOptions = () =>{
      let options = [];
      let bolt = this.app.bolts.find(b => b.t === this.app.state['конструкция_болта']);
      let diams = String(Object(bolt).d).split(',');

      diams.forEach((item,i) =>{
          options.push(
              <option key={i} value={item}>{item}</option>
          )
      })
      return options;

  }
    customNumericInputHandler = (value, name , validator) =>{

        value = parseFloat(value);

      if(name === 'nb' || name === 'nl'){
          if(value > 5) value = 5;
          if(isEmpty(value)) value = 0;
      }
      if(name.includes('xl') || name.includes('hl') || name.includes('hb') || name.includes('xb')){

          let nameArray = name.split('-');
          let index = nameArray[1];
          let param = nameArray[0];
          let stateArray = this.app.state[param];
          stateArray[index] = value;


          let errors = this.app.state.errors;
          if(!validator.isValid) {
              errors[name] = {
                  textError: validator.textError,
                  tab: '3'
              };

          }else{
              delete  errors[name];
          }

          this.app.setState({
              [param]:stateArray,
              errors:errors
          });

          return false;
      }

      let errors = this.app.state.errors;
      if(!validator.isValid) {
          errors[name] = {
            textError: validator.textError,
            tab: '3'
          };

      }else{
          delete  errors[name];
      }

      this.app.setState({
        [name]: value,
        errors:errors
      });
  };
    customCheckboxHandler = (name,checked) =>{
        this.app.setState({[name]:checked})

        if(name === 'anchor'){
            if(checked){
                this.app.setState({
                    'тип_колонны':'металлическая',
                    anchorBoltHTMLReport:true,
                })
            }else{
                this.app.setState({
                    'тип_колонны':'монолитная',
                    anchorBoltHTMLReport:false,
                })
            }
        }

        let ch = [];
        let tabIndex = null;
        if(this.app.state['anchor']) ch.push(1);
        if(this.app.state['reinforcement']) ch.push(2);
        if(this.app.state['size']) ch.push(0);

        switch (name) {
            case 'anchor': tabIndex = 1;
                break;
            case 'reinforcement': tabIndex = 2;
                break;
            case 'size': tabIndex = 0;
                break;

        }

        if(!checked && this.state.tabIndex === tabIndex) {
            let idArr = Array(ch.filter(item => item !== tabIndex))[0];
            let id = idArr.length > 0 ? idArr[0]:123;
            this.setState({tabIndex:id});
        }
        if(checked && this.state.tabIndex === 123){
            this.setState({tabIndex:tabIndex});
        }

    }
    renderStepsFields = (side) => {
      let fields = [];

      for(let i = 1; i < this.app.state['n' + side] + 1 && i <= 5;i++){
          fields.push(
              <div className="mt-3" key={i}>
                  <div className="row">
                      <div className="col-md-6">
                          <CustomNumericInput
                              name={'h'+ side + '-'+i}
                              className="form-control input-text"
                              value={this.app.state['h'+ side ][i]}
                              allowNegative={false}
                              measure={'[м]'}
                              label={"Высота " + i +"-й ступени"}
                              min={0.1}
                              presicion={2}
                              max={7}
                              step={0.1}
                              onValidate={this.validator}
                              enabledHandlerOnInput={true}
                              isValid={!this.app.state.errors.hasOwnProperty('h' + side +'-'+i)}
                              onChange={this.customNumericInputHandler}
                          />
                      </div>
                      <div className="col-md-6">
                          <CustomNumericInput
                              name={'x'+ side + '-'+i}
                              className="form-control input-text"
                              value={this.app.state['x'+ side ][i]}
                              allowNegative={false}
                              measure={'[м]'}
                              label={"Длина " + i + "-й ступени"}
                              min={0.1}
                              presicion={2}
                              max={7}
                              step={0.1}
                              onValidate={this.validator}
                              enabledHandlerOnInput={true}
                              isValid={!this.app.state.errors.hasOwnProperty('x' + side +'-'+i)}
                              onChange={this.customNumericInputHandler}
                          />
                      </div>
                  </div>
              </div>
          )
      }
      return fields;

  }
    popover = (html) =>{
        return <Popover visible style={{
            color:"#fff"
        }}>{html}</Popover>
    }
    validator = (props) => {
        const value = props.value;
        const name = props.name;
        let isValid = true,
            textError = 'Заполните поле';

        switch (name) {
            case 'hn':
                if(this.app.state['наличие_подготовки']){
                    if(isEmpty(value)) {
                        isValid = false;
                        textError = 'Заполните поле';
                    }

                    if(value < 0.1 || value > 1){
                        isValid = false;
                        textError = 'hn должно быть не более 1 м и не менее 0.1 м';
                    }
                }
                break;
            case 'Su':
                if (value < 0) {
                    isValid = false;
                    textError = 'Осадка не может быть отрицательной';
                }
                if(isEmpty(value)){
                    isValid = false;
                    textError = "Заполните поле";
                }
                break;
            case 'la':
            case 'ba':
                if(this.app.state['тип_колонны'] === 'металлическая'){
                    if(this.app.state['тип_металлической_колонны'] === 'одноветвевая'){
                        if (value < 0.05 || isEmpty(value) || value > 0.8) {
                            isValid = false;
                            textError = 'Расстояние от оси колонны до оси болта не менее 0.05 м и не более 0.8 м';
                        }
                    }
                }
                break;
            case 'kh_temp':
                if(value < 0){
                    isValid = false;
                    textError = 'Температура не может быть отрицательной, введите корректные данные';
                }
                if(value > 45){
                    isValid = false;
                    textError = 'Температура должна быть не более 45';
                }
                if(isEmpty(value)) isValid = false;
                break;
            case 'bp':
                if(value < 0.2 || value > 1){
                    isValid = false;
                    textError = 'Введены некорректные размеры анкерной плиты, уточните размеры';
                }
                if(this.app.state['тип_колонны'] === 'металлическая'){
                    if(this.app.state['тип_металлической_колонны'] === 'одноветвевая'){
                        if(this.app.state['база_колонны'] === 'низкая'){
                            if(value <= this.app.state.c_*2){
                                isValid = false;
                                textError = 'Анкерный болт находится за границей плиты,увеличьте анкерную плиту'
                            }
                        }
                    }
                }
                if(isEmpty(value)) isValid = false;
                break;
            case 'lp':
                if(value < 0.2 || value > 1){
                    isValid = false;
                    textError = 'Введены некорректные размеры анкерной плиты, уточните размеры';
                }
                if(this.app.state['тип_колонны'] === 'металлическая'){
                    if(this.app.state['тип_металлической_колонны'] === 'одноветвевая'){
                        if(this.app.state['база_колонны'] === 'низкая'){
                            if(value <= this.app.state.c*2){
                                isValid = false;
                                textError = 'Анкерный болт находится за границей плиты,увеличьте анкерную плиту'
                            }
                        }
                    }
                }
                if(isEmpty(value)) isValid = false;
                break;
            case 'lc':
                if(this.app.state['тип_колонны'] !== 'металлическая'){
                    if(this.app.state['форма_подошвы'] !== 'круглая' && this.app.state['тип_колонны'] === 'монолитная' ){
                        if(value < 0.3 || value > 1.2){
                            isValid = false;
                            textError = 'Длина подколонника должна быть в пределах от 0.3 м до 1.2 м';
                        }
                    }
                }

                break;
            case 'bc':
                if(this.app.state['тип_колонны'] !== 'металлическая'){
                    if(this.app.state['форма_подошвы'] !== 'круглая' && this.app.state['тип_колонны'] === 'монолитная' ){
                        if(value < 0.3 || value > 1.2){
                            isValid = false;
                            textError = 'Длина подколонника должна быть в пределах от 0.3 м до 1.2 м';
                        }
                    }
                }

                break;
            case 'Yc_l':
                if(value > 120){
                    isValid = false;
                    textError = 'L не более 120 м';
                }
                if(value < 1){
                    isValid = false;
                    textError = 'L не менее 1 м';
                }
                if(isEmpty(value)){
                    isValid = false;
                    textError = 'Заполните поле';
                }
                break;
            case 'Yc_h':
                if(value > 60){
                    isValid = false;
                    textError = 'H не более 60 м';
                }
                if(value < 1){
                    isValid = false;
                    textError = 'H не менее 1 м';
                }
                if(isEmpty(value)){
                    isValid = false;
                    textError = 'Заполните поле';
                }
                break;
            case 'h':
                if(value < 0.2 || value > 3 || isEmpty(value)){
                    isValid = false;
                    textError = 'Расстояние в осях между ветвями колонны должно быть не менее 0.2 м и не более 3 м';
                }
                break;
            case 'k':
                if(value < 0.15 || value > 1.1 || isEmpty(value)){
                    isValid = false;
                    textError = 'Расстояние от оси болта до противоположной грани болта должна быть в пределах от 0.15 м до 1.1 м';
                }
                break;
            case 'La':
                if(value < 0.2 || value > 1 || isEmpty(value)){
                    isValid = false;
                    textError = 'Расстояние от оси болта до противоположной грани пластины должно быть в пределах от 0.2 м до 1 м';
                }
                break;
            case 'c':
                if(value < 0.1 || value > 0.6 || isEmpty(value)){
                    isValid = false;
                    textError = 'Расстояние от оси болта до оси колонны должно быть в пределах от 0.1 м до 0.6 м';
                }
                if(this.app.state['тип_колонны'] === 'металлическая'){
                    if(this.app.state['тип_металлической_колонны'] === 'одноветвевая'){
                        if(this.app.state['база_колонны'] === 'низкая'){
                            if(this.app.state.lp <= value*2){
                                isValid = false;
                                textError = 'Анкерный болт находится за границей плиты,увеличьте анкерную плиту'
                            }
                        }
                    }
                }
                break;
            case 'c_':
                if(value < 0.02 || value > 0.6 || isEmpty(value)){
                    isValid = false;
                    textError = 'Расстояние от оси болта до оси колонны должно быть в пределах от 0.02 м до 0.6 м';
                }
                if(this.app.state['тип_колонны'] === 'металлическая'){
                    if(this.app.state['тип_металлической_колонны'] === 'одноветвевая'){
                        if(this.app.state['база_колонны'] === 'низкая'){
                            if(this.app.state.bp <= value*2){
                                isValid = false;
                                textError = 'Анкерный болт находится за границей плиты,увеличьте анкерную плиту'
                            }
                        }
                    }
                }
                break;
            case 'LBc':
                if(this.app.state['форма_подошвы'] === 'круглая') {
                    if (this.app.state['тип_подколонника'] === 'круглый') {
                        if (value < 0.3 || value > 1.2 || isEmpty(value)) {
                            isValid = false;
                            textError = 'Диаметр подколонника  должна быть в пределах от 0.3 м до 1.2 м';
                        }
                    }
                }
                break;
            case 'l':
                if(this.app.state['форма_подошвы'] === 'прямоугольная') {
                        if (value < 0 || value > 12 || isEmpty(value)) {
                            isValid = false;
                            textError = 'Длина подколонника  должна быть в пределах от 0 м до 12 м';
                        }
                }
                break;
            case 'b':
                if(this.app.state['форма_подошвы'] === 'прямоугольная') {
                        if (value < 0 || value > 12 || isEmpty(value)) {
                            isValid = false;
                            textError = 'Ширина подколонника  должна быть в пределах от 0 м до 12 м';
                        }
                }
                break;
            case 'D':
                if(this.app.state['форма_подошвы'] === 'прямоугольная') {
                    if (value < 0 || value > 12 || isEmpty(value)) {
                        isValid = false;
                        textError = 'Ширина подколонника  должна быть в пределах от 0 м до 12 м';
                    }
                }
                break;
            case 'dc':
                if(this.app.state['тип_колонны'] === 'сборная'){
                    if(value > 2){
                        isValid = false;
                        textError = 'Глубина заделки должна быть меньше из условия соотношения сечения колонны к глубине ее заделки';
                    }
                    if(Math.max(this.app.state.lc,this.app.state.bc) > value){
                        isValid = false;
                        textError = 'Условие заделки колонны в стакан не обеспеченно убедитесь в правильности введенных данных';
                    }
                    if(!isEmpty(this.app.state['высота_фундамента'])){
                        if(value > this.app.state['высота_фундамента'] - 0.2){
                            isValid = false;
                            textError = 'Высота фундамента меньше глубины заделки колонны в стакан';
                        }
                    }

                }
                break;
            case 'ad':
                if(value < 4 || value > 7){
                    isValid = false;
                    textError = 'Величина защитного слоя должна быть не меньше 4см  и не больше 7см';
                }
                break;
            case 'глубина_заложения':
            if(value < 0){
                isValid = false;
                textError = 'Не может быть отрицательной';
            }
            if(value < 0.5 || value > 4.5){
                isValid = false;
                textError = 'Глубина заложения должна быть не мене 0.5 м и не более 4.5 м';
            }
            if(!this.app.state['планируемость_территории']){
                if(value < this.app.state['глубина_слоя'] && !isEmpty(value)){
                    isValid = false;
                    textError = 'Подошва фундамента залегает на насыпном грунте,увеличьте глубину заложения';
                }
            }
            if (isEmpty(value)) isValid = true;
            break;
        case 'высота_фундамента':
            if(value < 0){
                isValid = false;
                textError = 'Не может быть отрицательной';
            }
            if(value < 0.6 || value > 5){
                isValid = false;
                textError = 'Высота фундамента должна быть не мене 0.6 м и не более 5 м';
            }
            if (isEmpty(value)) isValid = true;
            break;
        case 'nl':
        case 'nb':
            if(this.app.state['тип_расчета'] === 'check'){
                if(this.app.state.nl + this.app.state.nb === 0){
                    isValid = false;
                    textError = 'Подбор арматуры подошвы не будет проведен ввиду отсутствия заданного количества ступеней';
                }else{
                    let errors = this.app.state.errors;
                    delete errors.nl;
                    delete errors.nb;
                    this.app.setState({errors:errors});
                }

            }
            break;
        }

        if(name.includes('xl') || name.includes('xb')){
            if(value < 0.1 || value >7 || isEmpty(value) || isNaN(value)){
                isValid = false;
                textError = 'Длина ступени должна быть не меньше 0.1м и не больше 7м';
            }
        }
        if(name.includes('hl') || name.includes('hb')){
            if(value < 0.2 || value > 0.45 || isEmpty(value) || isNaN(value)){
                isValid = false;
                textError = 'Высота ступени должна быть не меньше 0.2 и не больше 0.45';
            }
        }

        if(value < 0){
            isValid = false;
            textError = 'Значение не может быть отрицательным';
        }
        return {
            isValid: isValid,
            textError: (isValid) ? '' : textError
        }
    }

    render() {

    let boltOptions = this.renderAnchorBoltsOptions();
    return (
        <div className='container-fluid mt-3 '>
            <div className="row mt-3">
                <div className="col-md-12">
                    <Tabs forceRenderTabPanel selectedIndex={this.state.tabIndex} onSelect={(tabIndex,last,e) => {
                        if(e.target.classList.contains('rs-checkbox-wrapper')) return false;
                        this.setState({ tabIndex })
                    }}>
                        <TabList>
                            <Tab disabled={!this.app.state.size}>
                                <div className="d-flex">
                                    <Checkbox name={'size'} checked={this.app.state.size} onChange={(v,checked) => this.customCheckboxHandler('size',checked)} />
                                    <p className={'mb-0 lh-35'}>Габариты фундамента</p>
                                </div>
                            </Tab>
                            <Tab disabled={!this.app.state.anchor}>
                                <div className="d-flex">
                                    <Checkbox name={'anchor'} checked={this.app.state.anchor} onChange={(v,checked) => this.customCheckboxHandler('anchor',checked)} />
                                    <p className={'mb-0 lh-35'}>Диаметр анкера</p>
                                </div>
                            </Tab>
                            <Tab disabled={!this.app.state.reinforcement}>
                                <div className="d-flex">
                                    <Checkbox name={'reinforcement'} checked={this.app.state.reinforcement} onChange={(v,checked) => this.customCheckboxHandler('reinforcement',checked)} />
                                    <p className={'mb-0 lh-35'}>Армирование</p>
                                </div>
                            </Tab>
                        </TabList>

                        <TabPanel>
                            <Tabs forceRenderTabPanel selectedIndex={this.state.tabIndex2} onSelect={tabIndex2 => this.setState({ tabIndex2 })}>
                                <TabList>
                                    <Tab>Параметры фундамента</Tab>
                                    <Tab>Параметры здания</Tab>
                                </TabList>
                                <TabPanel>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="mt-3">
                                                <p className="mb-1 mt-3">Форма подошвы</p>
                                                <ToggleButtonGroup type="radio" name="форма_подошвы" value={this.app.state['форма_подошвы']} onChange={this.toggleButtonsGroupHandler}>
                                                    <ToggleButton size="sm" variant="custom" value={'прямоугольная'}>Прямоугольная</ToggleButton>
                                                    <ToggleButton size="sm" variant="custom" value={'круглая'}>Круглая</ToggleButton>
                                                    <ToggleButton size="sm" variant="custom" value={'квадратная'}>Квадратная</ToggleButton>
                                                </ToggleButtonGroup>
                                            </div>
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'высота_фундамента'}
                                                    className="form-control input-text"
                                                    value={this.app.state['высота_фундамента']}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    labelLeft
                                                    label={"Высота h<sub>ф</sub>:"}
                                                    min={0}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('высота_фундамента')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'глубина_заложения'}
                                                    className="form-control input-text"
                                                    value={this.app.state['глубина_заложения']}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    labelLeft
                                                    label={"Глубина заложения d:"}
                                                    min={0}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('глубина_заложения')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            {this.app.state['форма_подошвы'] === 'прямоугольная' &&
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'l'}
                                                    className="form-control input-text"
                                                    value={this.app.state.l}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    labelLeft
                                                    label={"Длина l:"}
                                                    min={0}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('l')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>

                                            }
                                            {this.app.state['форма_подошвы'] === 'квадратная' &&
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'l'}
                                                    className="form-control input-text"
                                                    value={this.app.state.l}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    labelLeft
                                                    label={"Сторона фундамента а:"}
                                                    min={0}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('l')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            }

                                            {this.app.state['форма_подошвы'] === 'прямоугольная' &&

                                            <div className="mt-3">
                                                    <CustomNumericInput
                                                        name={'b'}
                                                        className="form-control input-text"
                                                        value={this.app.state.b}
                                                        allowNegative={false}
                                                        measure={'[м]'}
                                                        labelLeft
                                                        label={"Ширина b:"}
                                                        min={0}
                                                        step={0.1}
                                                        onValidate={this.validator}
                                                        enabledHandlerOnInput={true}
                                                        isValid={!this.app.state.errors.hasOwnProperty('b')}
                                                        onChange={this.customNumericInputHandler}
                                                    />
                                            </div>

                                            }
                                            {(this.app.state['форма_подошвы'] === 'круглая') &&
                                            <div className="mt-3">
                                                    <CustomNumericInput
                                                        name={'D'}
                                                        className="form-control input-text"
                                                        value={this.app.state.D}
                                                        allowNegative={false}
                                                        measure={'[м]'}
                                                        labelLeft
                                                        label={"Диаметр:"}
                                                        min={0}
                                                        step={0.1}
                                                        onValidate={this.validator}
                                                        enabledHandlerOnInput={true}
                                                        isValid={!this.app.state.errors.hasOwnProperty('D')}
                                                        onChange={this.customNumericInputHandler}
                                                    />
                                            </div>
                                            }

                                            <p className={'mt-3'}><b><ins>Конструктивные особенности фундамента</ins></b></p>
                                            <div className="d-flex  justify-content-between">
                                                <p className="lh-30 mb-0 mr-2">Фундамент под:</p>
                                                <ToggleButtonGroup type="radio" onChange={this.toggleButtonsGroupHandler} name="фундамент_под" value={this.app.state['фундамент_под']}>
                                                    <ToggleButton size="sm" variant="custom" value={'наружную_стену'}>Наружную стену</ToggleButton>
                                                    <ToggleButton size="sm" variant="custom" value={'внутреннюю_стену'}>Внутреннюю стену</ToggleButton>
                                                </ToggleButtonGroup>
                                            </div>
                                            <div className="mt-3">
                                                <Checkbox name={'наличие_подготовки'} checked={this.app.state['наличие_подготовки']} onChange={(v,checked) => this.customCheckboxHandler('наличие_подготовки',checked)} >Наличие подготовки под фундамент</Checkbox>
                                                {(this.app.state['наличие_подготовки']) &&
                                                <CustomNumericInput
                                                    name={'hn'}
                                                    className="form-control input-text"
                                                    value={this.app.state['hn']}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    precision={2}
                                                    min={0}
                                                    labelLeft={true}
                                                    label={"Толщина подготовки h<sub>n</sub>:"}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('hn')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="d-flex justify-content-between">
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'} >
                                                <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:"1"})} >
                                                    <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                                    <span className={'mr-2 fz-14'}><b>К Нагрузкам</b></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'} >
                                                <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex2:this.state.tabIndex2 + 1})}>
                                                    <span className={'mr-2 fz-14'}><b>К параметрам здания</b></span>
                                                    <img width='25px' height='25px' src={rightArrow} alt=""/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <ConstructProperties context={this}/>
                                    <div className="d-flex justify-content-between">
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'} >
                                                <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex2:this.state.tabIndex2 - 1})} >
                                                    <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                                    <span className={'mr-2 fz-14'}><b>К параметрам фундамента</b></span>
                                                </div>
                                            </div>
                                        </div>
                                        {(this.app.state.anchor) &&
                                            <div className="position-relative z-index-top">
                                                <div className={'mb-5 pointer'} >
                                                    <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex + (this.app.state.anchor)?1:2})}>
                                                        <span className={'mr-2 fz-14'}><b>К проверке анкера</b></span>
                                                        <img width='25px' height='25px' src={rightArrow} alt=""/>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                        {(this.app.state.reinforcement && !this.app.state.anchor) &&
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'} >
                                                <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex + (this.app.state.anchor)?1:2})}>
                                                    <span className={'mr-2 fz-14'}><b>К проверке армирования</b></span>
                                                    <img width='25px' height='25px' src={rightArrow} alt=""/>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                        {(!this.app.state.anchor && !this.app.state.reinforcement) &&
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'} >
                                                <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:"2"})} >
                                                    <span className={'mr-2 fz-14'}><b>К Геологии</b></span>
                                                    <img width='25px' height='25px' src={rightArrow} alt=""/>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                </TabPanel>
                            </Tabs>
                        </TabPanel>
                        <TabPanel>
                            <div className="row pb-5">
                                <div className="col-md-7">
                                    <div className="mt-3">
                                        <div className="d-flex justify-content-between">
                                            <p className="lh-30 mb-0 mr-5">Конструкция колонны:</p>
                                            <ToggleButtonGroup type="radio" name="тип_металлической_колонны" onChange={this.toggleButtonsGroupHandler} value={this.app.state['тип_металлической_колонны']}>
                                                <ToggleButton size="sm" variant="custom" value='двухветвевая'>Двухветвевая</ToggleButton>
                                                <ToggleButton size="sm" variant="custom" value='одноветвевая'>Одноветвевая</ToggleButton>
                                            </ToggleButtonGroup>
                                        </div>
                                        {(this.app.state['тип_металлической_колонны'] === 'одноветвевая') &&
                                        <div className="d-flex justify-content-between mt-3">
                                            <p className="lh-30 mb-0 mr-5">База колонны:</p>
                                            <ToggleButtonGroup type="radio" name="база_колонны" onChange={this.toggleButtonsGroupHandler} value={this.app.state['база_колонны']}>
                                                <ToggleButton size="sm" variant="custom" value='высокая'>С высокой траверсой</ToggleButton>
                                                <ToggleButton size="sm" variant="custom" value='низкая'>С низкой траверсой</ToggleButton>
                                            </ToggleButtonGroup>
                                        </div>
                                        }
                                        <div className="d-flex justify-content-between mt-3">
                                            <p className="lh-30 mb-0 mr-5">Нагрузка на колонну:</p>
                                            <ToggleButtonGroup type="radio" name="нагрузка_на_колонну" onChange={this.toggleButtonsGroupHandler} value={this.app.state['нагрузка_на_колонну']}>
                                                <ToggleButton size="sm" variant="custom" value='статическая'>Статическая</ToggleButton>
                                                <ToggleButton size="sm" variant="custom" value='динамическая'>Динамическая</ToggleButton>
                                            </ToggleButtonGroup>
                                        </div>

                                        {(this.app.state['нагрузка_на_колонну'] === 'динамическая') &&
                                        <div className='d-flex justify-content-between mt-3'>
                                            <p className="lh-30 mb-0 mr-5">Число циклов нагружений:</p>
                                            <select name='alpha' className='fz-14 form-control w-25' onChange={this.paramsHandler} value={this.app.state.alpha}>
                                                <option value='3.15'>50 тыс.</option>
                                                <option value='2.25'>200 тыс.</option>
                                                <option value='1.57'>800 тыс.</option>
                                                <option value='1.25'>2 млн.</option>
                                                <option value='1'>5 млн. и более</option>
                                            </select>
                                        </div>
                                        }
                                        <div className='mt-3'>
                                            <p className="lh-30 mb-0 mr-5">Конструкция болта: <b>{this.app.state['конструкция_болта']} </b></p>
                                            <div className="d-flex justify-content-between" onChange={this.paramsHandler} >
                                                <input type="radio" name='конструкция_болта' id='bolt1' value='конический' checked={this.app.state['конструкция_болта'] === 'конический'} />
                                                <label htmlFor="bolt1" >
                                                    <img src={anchor1} alt="" className="anchor-img"/>
                                                </label>
                                                <input type="radio" name='конструкция_болта' id='bolt2' value='прямой' checked={this.app.state['конструкция_болта'] === 'прямой'}/>
                                                <label htmlFor="bolt2" >
                                                    <img src={anchor2} alt="" className="anchor-img"/>
                                                </label>
                                                <input type="radio" name='конструкция_болта' id='bolt3' value='с отгибом' checked={this.app.state['конструкция_болта'] === 'с отгибом'}/>
                                                <label htmlFor="bolt3">
                                                    <img src={anchor3} alt="с анкерной плитой съемный" className="anchor-img"/>
                                                </label>
                                                <input type="radio" name='конструкция_болта' id='bolt4' value='с анкерной плитой съемный' checked={this.app.state['конструкция_болта'] === 'с анкерной плитой съемный'}/>
                                                <label htmlFor="bolt4">
                                                    <img src={anchor4} alt="" className="anchor-img"/>
                                                </label>
                                                <input type="radio" name='конструкция_болта' id='bolt5' value='с анкерной плитой глухой' checked={this.app.state['конструкция_болта'] === 'с анкерной плитой глухой'}/>
                                                <label htmlFor="bolt5" >
                                                    <img src={anchor5} alt="" className="anchor-img"/>
                                                </label>
                                            </div>
                                        </div>
                                        <div className="mt-3 d-flex justify-content-between">
                                            <p className="lh-30 mb-0 mr-5">Марка стали болта:</p>
                                            <select name='марка_стали' className='fz-14 form-control w-25' onChange={this.paramsHandler} value={this.app.state['марка_стали']} >
                                                <option value='Ст3пс2'>Ст3пс2</option>
                                                <option value='Ст3пс4'>Ст3пс4</option>
                                                <option value='Ст3сп4'>Ст3сп4</option>
                                                <option value='Ст3сп2'>Ст3сп2</option>
                                                <option value='09Г2С-4'>09Г2С-4</option>
                                                <option value='09Г2С-6'>09Г2С-6</option>
                                            </select>
                                        </div>
                                        <div className="mt-3 d-flex justify-content-between mt-3">
                                            <p className="lh-30 mb-0">Диаметр анкерного болта:</p>
                                            <select name='db' className='fz-14 form-control w-25' onChange={this.paramsHandler} value={this.app.state['db']} >
                                                {boltOptions}
                                            </select>
                                        </div>
                                        <div className="mt-3">
                                            <CustomNumericInput
                                                name={'H0'}
                                                className="form-control input-text"
                                                value={this.app.state['H0']}
                                                allowNegative={false}
                                                measure={'[см]'}
                                                precision={0}
                                                maxLength={1}
                                                labelLeft
                                                label={"Глубина заделки H<sub>0</sub>:"}
                                                step={2}
                                                min={0}
                                                onValidate={this.validator}
                                                enabledHandlerOnInput={true}
                                                isValid={!this.app.state.errors.hasOwnProperty('H0')}
                                                onChange={this.customNumericInputHandler}
                                            />
                                        </div>
                                        {(this.app.state['тип_металлической_колонны'] === 'одноветвевая' && this.app.state['база_колонны'] === 'низкая') &&
                                        <div className="d-flex justify-content-between mt-3">
                                            <p className="lh-30 mb-0">Класс бетона:</p>
                                            <select name='класс_бетона' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['класс_бетона']}>
                                                <option value='B15'>B15</option>
                                                <option value='B20'>B20</option>
                                                <option value='B25'>B25</option>
                                                <option value='B30'>B30</option>
                                                <option value='B35'>B35</option>
                                                <option value='B40'>B40</option>
                                                <option value='B45'>B45</option>
                                                <option value='B50'>B50</option>
                                                <option value='B55'>B55</option>
                                                <option value='B60'>B60</option>
                                                <option value='B70'>B70</option>
                                                <option value='B80'>B80</option>
                                                <option value='B90'>B90</option>
                                                <option value='B100'>B100</option>
                                            </select>
                                        </div>
                                        }
                                        {(this.app.state['тип_металлической_колонны'] === 'двухветвевая') &&
                                        <div>
                                            <div className="mt-3 d-flex justify-content-between">
                                                <p className="lh-30 mb-0 mr-5">Количество болтов на одной ветви:</p>
                                                <select name='n'
                                                        onMouseOver={this.svgTwoBranchesHandler}
                                                        onMouseOut={this.svgTwoBranchesHandler}
                                                        className='fz-14 form-control w-25'
                                                        onChange={this.paramsHandler}
                                                        value={this.app.state.n}
                                                >
                                                    <option value={2}>2</option>
                                                    <option value={4}>4</option>
                                                    <option value={6}>6</option>
                                                    <option value={8}>8</option>
                                                </select>
                                            </div>
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'k'}
                                                    className="form-control input-text"
                                                    value={this.app.state.k}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    precision={2}
                                                    labelLeft
                                                    label={"Растояние в осях между болтами k:"}
                                                    min={0}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    onMouseOver={this.svgTwoBranchesHandler}
                                                    onMouseOut={this.svgTwoBranchesHandler}
                                                    isValid={!this.app.state.errors.hasOwnProperty('k')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'h'}
                                                    className="form-control input-text"
                                                    value={this.app.state['h']}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    precision={2}
                                                    labelLeft
                                                    label={"Расстояние в осях между ветвей h:"}
                                                    min={0}
                                                    step={0.1}
                                                    onMouseOver={this.svgTwoBranchesHandler}
                                                    onMouseOut={this.svgTwoBranchesHandler}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('h')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            {(this.app.state.n != 2) &&
                                            <div className="mt-2">
                                                <CustomNumericInput
                                                    name={'c_two'}
                                                    className="form-control input-text"
                                                    value={this.app.state.c_two}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    precision={2}
                                                    labelLeft
                                                    label={"Расстояние по осям между крайними болтами ветвей c:"}
                                                    min={0}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    onMouseOver={this.svgTwoBranchesHandler}
                                                    onMouseOut={this.svgTwoBranchesHandler}
                                                    isValid={!this.app.state.errors.hasOwnProperty('c_two')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            }
                                        </div>
                                        }
                                        {(this.app.state['тип_металлической_колонны'] === 'одноветвевая') &&
                                        <div>
                                            <div className="mt-3 d-flex justify-content-between">
                                                <p className="lh-30 mb-0 mr-5">Количество растянутых болтов:</p>
                                                <select name='n'
                                                        className='fz-14 form-control w-25'
                                                        onChange={this.paramsHandler}
                                                        value={this.app.state.n}
                                                        onMouseOver={this.svgOneBranchHandler}
                                                        onMouseOut={this.svgOneBranchHandler}
                                                >
                                                    <option value={1}>1</option>
                                                    <option value={2}>2</option>
                                                    <option value={3}>3</option>
                                                    <option value={4}>4</option>
                                                </select>
                                            </div>
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'bp'}
                                                    className="form-control input-text m-0"
                                                    value={this.app.state['bp']}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    precision={2}
                                                    labelLeft
                                                    label={"Ширина анкерной плитки b : "}
                                                    min={0}
                                                    step={0.1}
                                                    onMouseOver={this.svgOneBranchHandler}
                                                    onMouseOut={this.svgOneBranchHandler}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('bp')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'lp'}
                                                    className="form-control input-text"
                                                    value={this.app.state['lp']}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    labelLeft
                                                    label={"Длинна анкерной плитки l : "}
                                                    min={0}
                                                    step={0.1}
                                                    precision={2}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    onMouseOver={this.svgOneBranchHandler}
                                                    onMouseOut={this.svgOneBranchHandler}
                                                    isValid={!this.app.state.errors.hasOwnProperty('lp')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>


                                            {(this.app.state['база_колонны'] === 'высокая') &&

                                            <div>

                                                <div className="mt-3">
                                                    <CustomNumericInput
                                                        name={'la'}
                                                        className="form-control input-text"
                                                        value={this.app.state.la}
                                                        allowNegative={false}
                                                        measure={'[м]'}
                                                        labelLeft
                                                        label={"Расстояние от оси колонны до оси болта la:"}
                                                        min={0}
                                                        precision={3}
                                                        step={0.1}
                                                        onMouseOver={this.svgOneBranchHandler}
                                                        onMouseOut={this.svgOneBranchHandler}
                                                        onValidate={this.validator}
                                                        enabledHandlerOnInput={true}
                                                        isValid={!this.app.state.errors.hasOwnProperty('la')}
                                                        onChange={this.customNumericInputHandler}
                                                    />
                                                </div>
                                                {(this.app.state.n > 1) &&
                                                    <div className="mt-3">
                                                        <CustomNumericInput
                                                            name={'ba'}
                                                            className="form-control input-text"
                                                            value={this.app.state.ba}
                                                            allowNegative={false}
                                                            measure={'[м]'}
                                                            labelLeft
                                                            label={"Расстояние от оси колонны до оси болта ba:"}
                                                            min={0}
                                                            precision={3}
                                                            step={0.1}
                                                            onMouseOver={this.svgOneBranchHandler}
                                                            onMouseOut={this.svgOneBranchHandler}
                                                            onValidate={this.validator}
                                                            enabledHandlerOnInput={true}
                                                            isValid={!this.app.state.errors.hasOwnProperty('ba')}
                                                            onChange={this.customNumericInputHandler}
                                                        />
                                                    </div>
                                                }
                                            </div>
                                            }

                                            {(this.app.state['база_колонны'] === 'низкая') &&
                                            <div>
                                                <div className="mt-3">
                                                    <CustomNumericInput
                                                        name={'c'}
                                                        className="form-control input-text"
                                                        value={this.app.state.c}
                                                        allowNegative={false}
                                                        measure={'[м]'}
                                                        precision={3}
                                                        labelLeft
                                                        label={"Расстояние от оси болта до оси колонны с:"}
                                                        min={0}
                                                        step={0.1}
                                                        onMouseOver={this.svgOneBranchHandler}
                                                        onMouseOut={this.svgOneBranchHandler}
                                                        onValidate={this.validator}
                                                        enabledHandlerOnInput={true}
                                                        isValid={!this.app.state.errors.hasOwnProperty('c')}
                                                        onChange={this.customNumericInputHandler}
                                                    />
                                                </div>
                                                {(this.app.state.n > 1) &&
                                                <div className="mt-3">
                                                    <CustomNumericInput
                                                        name={'c_'}
                                                        className="form-control input-text"
                                                        value={this.app.state.c_}
                                                        allowNegative={false}
                                                        measure={'[м]'}
                                                        precision={3}
                                                        labelLeft
                                                        label={"Расстояние от оси болта до оси колонны с':"}
                                                        onMouseOver={this.svgOneBranchHandler}
                                                        onMouseOut={this.svgOneBranchHandler}
                                                        min={0}
                                                        step={0.1}
                                                        onValidate={this.validator}
                                                        enabledHandlerOnInput={true}
                                                        isValid={!this.app.state.errors.hasOwnProperty('c_')}
                                                        onChange={this.customNumericInputHandler}
                                                    />
                                                </div>
                                                }

                                            </div>
                                            }
                                        </div>
                                        }
                                    </div>
                                </div>
                                <div className="col-md-5 position-relative">
                                    <div className="imgS mt-5" >
                                        <object id="svg-object"  data={twoBranches} type="image/svg+xml" />
                                        <object id="lowTraversaSvg" data={lowTraversa} type="image/svg+xml" />
                                        <object id="highTraversaSvgL" data={highTraversaSvgL} type="image/svg+xml" />
                                        <object id="highTraversaSvgB" data={highTraversaSvgB} type="image/svg+xml" />
                                    </div>
                                </div>
                            </div>
                            <div className="d-flex justify-content-between">
                                {(this.app.state.size) &&
                                <div className="position-relative z-index-top">
                                    <div className={'mb-5 pointer'}>
                                        <div className="btn btn-light p-1 px-5 "
                                             onClick={() => this.setState({tabIndex: this.state.tabIndex - 1})}>
                                            <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                            <span className={'mr-2 fz-14'}><b>К проверке габаритов</b></span>

                                        </div>
                                    </div>
                                </div>
                                }
                                {(!this.app.state.size) &&
                                <div className="position-relative z-index-top">
                                    <div className={'mb-5 pointer'}>
                                        <div className="btn btn-light p-1 px-5 "
                                             onClick={() => this.app.setState({ tabs:"1"})}>
                                            <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                            <span className={'mr-2 fz-14'}><b>К нагрузкам</b></span>

                                        </div>
                                    </div>
                                </div>
                                }
                                {(this.app.state.reinforcement) &&
                                <div className="position-relative z-index-top">
                                    <div className={'mb-5 pointer'} >
                                        <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex + 1})}>
                                            <span className={'mr-2 fz-14'}><b>К проверке армирования</b></span>
                                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                                        </div>
                                    </div>
                                </div>
                                }
                                {(!this.app.state.reinforcement) &&
                                <div className="position-relative z-index-top">
                                    <div className={'mb-5 pointer'} >
                                        <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:"2"})} >
                                            <span className={'mr-2 fz-14'}><b>К Геологии</b></span>
                                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                                        </div>
                                    </div>
                                </div>
                                }

                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="row pb-5">
                                <div className="col-md-6">
                                    <div className={'position-relative'}>
                                        <p className={this.app.state.size?'disabled-size-data':'hide'}>Данные взяты из вкладки габариты фундамента</p>
                                        <div className={this.app.state.size?'disabled':''}>
                                            <div className="mt-3">
                                                <p className="mb-1 mt-3">Форма подошвы</p>
                                                <ToggleButtonGroup type="radio" name="форма_подошвы" value={this.app.state['форма_подошвы']} onChange={this.toggleButtonsGroupHandler}>
                                                    <ToggleButton size="sm" variant="custom" value={'прямоугольная'}>Прямоугольная</ToggleButton>
                                                    <ToggleButton size="sm" variant="custom" value={'круглая'}>Круглая</ToggleButton>
                                                    <ToggleButton size="sm" variant="custom" value={'квадратная'}>Квадратная</ToggleButton>
                                                </ToggleButtonGroup>
                                            </div>
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'hf'}
                                                    className="form-control input-text"
                                                    value={this.app.state.hf}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    labelLeft
                                                    label={"Высота h<sub>ф</sub>:"}
                                                    min={0}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('hf')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'d'}
                                                    className="form-control input-text"
                                                    value={this.app.state.d}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    labelLeft
                                                    label={"Глубина заложения d:"}
                                                    min={0}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('d')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            {this.app.state['форма_подошвы'] === 'прямоугольная' &&
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'l'}
                                                    className="form-control input-text"
                                                    value={this.app.state.l}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    labelLeft
                                                    label={"Длина l:"}
                                                    min={0}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('l')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            }
                                            {this.app.state['форма_подошвы'] === 'квадратная' &&
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'l'}
                                                    className="form-control input-text"
                                                    value={this.app.state.l}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    labelLeft
                                                    label={"Сторона фундамента а: :"}
                                                    min={0}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('l')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            }

                                            {this.app.state['форма_подошвы'] === 'прямоугольная' &&
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'b'}
                                                    className="form-control input-text"
                                                    value={this.app.state.b}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    labelLeft
                                                    label={"Ширина b:"}
                                                    min={0}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('b')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            }
                                            {(this.app.state['форма_подошвы'] === 'круглая') &&
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'diam'}
                                                    className="form-control input-text"
                                                    value={this.app.state.diam}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    labelLeft
                                                    label={"Диаметр круглого фундамента:"}
                                                    min={0}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('diam')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            }
                                        </div>
                                    </div>
                                    <div className="mt-3 d-flex justify-content-between">
                                        <p className={'mb-0 lh-30'}>Диаметр арматуры:</p>
                                        <select name='диаметр_арматуры' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['диаметр_арматуры']}>
                                            <option value='0.012'>12 мм</option>
                                            <option value='0.014'>14 мм</option>
                                            <option value='0.016'>16 мм</option>
                                            <option value='0.018'>18 мм</option>
                                            <option value='0.02'>20 мм</option>
                                            <option value='0.022'>22 мм</option>
                                            <option value='0.025'>25 мм</option>
                                            <option value='0.028'>28 мм</option>
                                            <option value='0.032'>32 мм</option>
                                            <option value='0.036'>36 мм</option>
                                            <option value='0.04'>40 мм</option>
                                            <option value='0.045'>45 мм</option>
                                            <option value='0.05'>50 мм</option>
                                            <option value='0.055'>55 мм</option>
                                            <option value='0.06'>60 мм</option>
                                            <option value='0.07'>70 мм</option>
                                            <option value='0.08'>80 мм</option>
                                        </select>
                                    </div>
                                    <div className="mt-3 d-flex justify-content-between">
                                        <p className={'mb-0 lh-30'}>Диаметр арматуры в подколонную часть:</p>
                                        <select name='ds_undercolumn' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['ds_undercolumn']}>
                                            <option value='0.012'>12 мм</option>
                                            <option value='0.014'>14 мм</option>
                                            <option value='0.016'>16 мм</option>
                                            <option value='0.018'>18 мм</option>
                                            <option value='0.02'>20 мм</option>
                                            <option value='0.022'>22 мм</option>
                                            <option value='0.025'>25 мм</option>
                                            <option value='0.028'>28 мм</option>
                                            <option value='0.032'>32 мм</option>
                                            <option value='0.036'>36 мм</option>
                                            <option value='0.04'>40 мм</option>
                                            <option value='0.045'>45 мм</option>
                                            <option value='0.05'>50 мм</option>
                                            <option value='0.055'>55 мм</option>
                                            <option value='0.06'>60 мм</option>
                                            <option value='0.07'>70 мм</option>
                                            <option value='0.08'>80 мм</option>
                                        </select>
                                    </div>
                                    <div className="mt-3">
                                        <CustomNumericInput
                                            name={'ad'}
                                            className="form-control input-text"
                                            value={this.app.state.ad}
                                            allowNegative={false}
                                            measure={'[см]'}
                                            precision={2}
                                            labelLeft
                                            label={"Величина защитного слоя :"}
                                            min={4}
                                            max={7}
                                            step={1}
                                            onValidate={this.validator}
                                            enabledHandlerOnInput={true}
                                            isValid={!this.app.state.errors.hasOwnProperty('ad')}
                                            onChange={this.customNumericInputHandler}
                                        />
                                    </div>
                                    <div className="mt-3">
                                        <CustomNumericInput
                                            name={'bc'}
                                            className="form-control input-text"
                                            value={this.app.state.bc}
                                            allowNegative={false}
                                            measure={'[м]'}
                                            precision={2}
                                            labelLeft
                                            label={"Ширина подколонника bc"}
                                            min={0}
                                            max={10}
                                            step={0.1}
                                            onValidate={this.validator}
                                            enabledHandlerOnInput={true}
                                            isValid={!this.app.state.errors.hasOwnProperty('bc')}
                                            onChange={this.customNumericInputHandler}
                                        />
                                    </div>
                                    <div className="mt-3">
                                        <CustomNumericInput
                                            name={'lc'}
                                            className="form-control input-text"
                                            value={this.app.state.lc}
                                            allowNegative={false}
                                            measure={'[м]'}
                                            precision={2}
                                            labelLeft
                                            label={"Длина подколонника lc"}
                                            min={0}
                                            max={10}
                                            step={0.1}
                                            onValidate={this.validator}
                                            enabledHandlerOnInput={true}
                                            isValid={!this.app.state.errors.hasOwnProperty('lc')}
                                            onChange={this.customNumericInputHandler}
                                        />
                                    </div>
                                    <div className="d-flex justify-content-between mt-3">
                                        <p className="lh-30 mb-0">Класс бетона:</p>
                                        <select name='класс_бетона' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['класс_бетона']}>
                                            <option value='B15'>B15</option>
                                            <option value='B20'>B20</option>
                                            <option value='B25'>B25</option>
                                            <option value='B30'>B30</option>
                                            <option value='B35'>B35</option>
                                            <option value='B40'>B40</option>
                                            <option value='B45'>B45</option>
                                            <option value='B50'>B50</option>
                                            <option value='B55'>B55</option>
                                            <option value='B60'>B60</option>
                                            <option value='B70'>B70</option>
                                            <option value='B80'>B80</option>
                                            <option value='B90'>B90</option>
                                            <option value='B100'>B100</option>
                                        </select>
                                    </div>
                                    <div className="d-flex mt-3  justify-content-between ">
                                        <p className="lh-30 mb-0">Класс арматуры:</p>
                                        <select name='класс_арматуры' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['класс_арматуры']}>
                                            <option value='A240'>А240</option>
                                            <option value='A400'>А400</option>
                                            <option value='A500'>А500</option>
                                            <option value='A600'>А600</option>
                                            <option value='A800'>А800</option>
                                            <option value='A1000'>А1000</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="mt-3">
                                        <CustomNumericInput
                                            name={'nl'}
                                            className="form-control input-text"
                                            value={this.app.state.nl}
                                            allowNegative={false}
                                            measure={' '}
                                            labelLeft
                                            label={"Количество ступеней по стороне l:"}
                                            min={0}
                                            max={5}
                                            step={1}
                                            onValidate={this.validator}
                                            enabledHandlerOnInput={true}
                                            isValid={!this.app.state.errors.hasOwnProperty('nb')}
                                            onChange={this.customNumericInputHandler}
                                        />
                                        <p className={'steps-notification mt-2'}>Вылет ступени принимать не более трехкратной величины суммы всех ступеней</p>
                                        {this.renderStepsFields('l')}
                                    </div>
                                    <hr/>
                                    <div className="mt-3">
                                            <CustomNumericInput
                                                name={'nb'}
                                                className="form-control input-text"
                                                value={this.app.state.nb}
                                                allowNegative={false}
                                                measure={' '}
                                                labelLeft
                                                label={"Количество ступеней по стороне b:"}
                                                min={0}
                                                max={5}
                                                step={1}
                                                onValidate={this.validator}
                                                enabledHandlerOnInput={true}
                                                isValid={!this.app.state.errors.hasOwnProperty('nb')}
                                                onChange={this.customNumericInputHandler}
                                            />
                                        {this.renderStepsFields('b')}
                                    </div>
                                </div>
                            </div>
                            <div className="d-flex justify-content-between">
                                {(this.app.state.anchor) &&

                                <div className="position-relative z-index-top">
                                    <div className={'mb-5 pointer'}>
                                        <div className="btn btn-light p-1 px-5 "
                                             onClick={() => this.setState({tabIndex: this.state.tabIndex - 1})}>
                                            <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                            <span className={'mr-2 fz-14'}><b>К проверке анкера</b></span>
                                        </div>
                                    </div>
                                </div>
                                }
                                {(!this.app.state.anchor && this.app.state.size) &&

                                <div className="position-relative z-index-top">
                                    <div className={'mb-5 pointer'}>
                                        <div className="btn btn-light p-1 px-5 "
                                             onClick={() => this.setState({tabIndex: this.state.tabIndex - 2})}>
                                            <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                            <span className={'mr-2 fz-14'}><b>К проверке габаритов</b></span>
                                        </div>
                                    </div>
                                </div>
                                }

                                {(!this.app.state.anchor && !this.app.state.size) &&
                                <div className="position-relative z-index-top">
                                    <div className={'mb-5 pointer'}>
                                        <div className="btn btn-light p-1 px-5 "
                                             onClick={() => this.app.setState({ tabs:"1"})}>
                                            <img width='25px' height='25px'  className={'mr-2'} src={leftArrow} alt=""/>
                                            <span className={'mr-2 fz-14'}><b>К Нагрузкам</b></span>

                                        </div>
                                    </div>
                                </div>
                                }
                                <div className="position-relative z-index-top">
                                    <div className={'mb-5 pointer'} >
                                        <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:"2"})} >
                                            <span className={'mr-2 fz-14'}><b>К Геологии</b></span>
                                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </TabPanel>
                    </Tabs>
                </div>
            </div>
        </div>
    );
  }

}

export default PillarFoundationCheck;
