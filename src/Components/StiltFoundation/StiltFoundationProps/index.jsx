import React, { Component } from 'react';
import { ToggleButton,ToggleButtonGroup } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-select/dist/css/bootstrap-select.min.css';
import StiltFoundationSolve from '../StiltFoundationSolve'
import StiltFoundationCheck from '../StiltFoundationCheck'

import './index.css';

class StiltFoundationProps extends Component {

  app = this.props.app;
  toggleButtonsGroupHandler = (value,e) =>{
        let name = e.target.name;
        this.app.setState({
            [name]:value
        });

    };

  paramsHandler = e =>{
      let name = e.target.name;
      let value = e.target.value;
      this.app.setState({[name]:value})
      console.log(name,value);
  };
  render() {
    return (
        <div style={{display: (this.app.state.tabs === '3')?'block':'none'}} className='container'>
            <div className={'row'}>
                <div className="col-md-6 mt-3">
                    <ToggleButtonGroup type="radio" className={'w-50'} name="тип_расчета" onChange={this.toggleButtonsGroupHandler} value={this.app.state['тип_расчета']}>
                        <ToggleButton size="sm" variant="custom" value={"solve"}>Вычислить</ToggleButton>
                        <ToggleButton size="sm" variant="custom" disabled value={"check"}>Проверить</ToggleButton>
                    </ToggleButtonGroup>
                </div>

              {(this.app.state['тип_расчета'] === 'solve') &&
                  <StiltFoundationSolve
                      app={this.app}
                      wrapper={this}
                  />
              }
              {(this.app.state['тип_расчета'] === 'check') &&
                  <StiltFoundationCheck
                      app={this.app}
                      wrapper={this}
                  />
              }

            </div>
        </div>
    );
  }
}

export default StiltFoundationProps;
