import React, { Component } from 'react';
import {ToggleButton, ToggleButtonGroup} from 'react-bootstrap'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Popover, Whisper,Checkbox} from 'rsuite/lib/index';
import ReactHtmlParser from "react-html-parser";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-select/dist/css/bootstrap-select.min.css';
import 'react-tabs/style/react-tabs.css';
import stiltScheme from "../../../img/stiltScheme.svg";
import './index.css';
import CustomNumericInput from "../../CustomNumericInput/CustomNumericInput";
import {isEmpty } from "../../Global/Global";
import rightArrow from "../../../img/right-arrow.svg";
import leftArrow from "../../../img/left-arrow.svg";
import Select from "react-select";
import crossSectionSquareImg from "../../../img/crossSectionSquareImg.svg";
import crossSectionRectangleImg from "../../../img/crossSectionRectangleImg.svg";
import crossSectionCircleInCircleImg from "../../../img/crossSectionCircleInCircleImg.svg";
import crossSectionCircleInRectImg from "../../../img/crossSectionCircleInRectImg.svg";
import crossSectionCircleImg from "../../../img/crossSectionCircleImg.svg";

// const URL = 'http://127.0.0.1:8000';
const URL = 'http://foundation-ru.1gb.ru';


class StiltFoundationSolve extends Component {

  state ={
    su:null,
    selectedOption:{
      Su: 10,
      isDisabled: 0,
      kren: "",
      label: "то же. с устройством железобетонных поясов или монолитных перекрытий. а также здания монолитной конструкции",
      value: 3
    },
    selectedOption2:{
      label: "1 Погружение сплошных и полых с закрытым нижним концом свай механическими (подвесными), паровоздушными и дизельными молотами",
      value: '1'
    },
    tabIndex:0,
  };

  wrapper = this.props.wrapper;
  app = this.props.app;
  options = this.app.optionsSu;
  svgTwoBranchesHandler = this.wrapper.svgTwoBranchesHandler
  svgOneBranchHandler = this.wrapper.svgOneBranchHandler;
  paramsHandler = this.wrapper.paramsHandler;
  toggleButtonsGroupHandler = this.wrapper.toggleButtonsGroupHandler;
  methodOptions2 = [];
  methodsOptions = [
    {
      value:'1',
      label:'1 Погружение сплошных и полых с закрытым нижним концом свай механическими (подвесными), паровоздушными и дизельными молотами'
    },
    {
      value:'2',
      label:'2 Погружение забивкой и вдавливанием в предварительно пробуренные лидерные скважины с заглублением концов свай не менее 1 м ниже забоя скважины при ее диаметре'
    },
    {
      value:'3',
      label:'3 Погружение с подмывом в песчаные грунты при условии добивки свай на последнем этапе погружения без применения подмыва на 1 м и более'
    },
    {
      value:'4',
      label:'4 Вибропогружение свай-оболочек, вибропогружение и вибровдавливание свай в грунты'
    },
    {
      value:'5',
      label:'5 Погружение молотами полых железобетонных свай с открытым нижним концом'
    },
    {
      value:'6',
      label:'6 Погружение любым способом полых свай круглого сечения с закрытым нижним концом на глубину 10 м и более с последующим устройством в нижнем конце свай камуфлетного уширения в песчаных грунтах средней плотности и в глинистых грунтах с показателем текучести ≤ 0,5 при диаметре уширения, равном'
    },
    {
      value:'7',
      label:'7 Погружение вдавливанием свай'
    },
  ];

  validateField = (props) => {

    const value = props.value;
    const name = props.name;
    let isValid = true,
        textError = 'Заполните поле';

    switch (name) {
      case 'Su':
        if (value < 0) {
          isValid = false;
          textError = 'Осадка не может быть отрицательной';
        }
        if(isEmpty(value)){
          isValid = false;
          textError = "Заполните поле";
        }
        break;
    }

    if(value < 0){
      isValid = false;
      textError = 'Значение не может быть отрицательным';
    }
    return {
      isValid: isValid,
      textError: (isValid) ? '' : textError
    }
  };
  getSt2(){
    fetch(URL + '/st2')
        .then((response) => { return response.json(); })
        .then((myJson) => {
            myJson.forEach(c=>{
                this.methodOptions2.push({
                    value:c.clay,
                    label:c.title,
                })
            })
        })

  }
  customNumericInputHandler = (value, name , validator) =>{

      let errors = this.app.state.errors;
      if(!validator.isValid) {
          errors[name] = {
            textError: validator.textError,
            tab: '3'
          };
      }else{
          delete  errors[name];
      }

      this.app.setState({
        [name]: value,
        errors:errors
      });
  };

  popover = (html) =>{
    return <Popover visible style={{
      color:"#fff"
    }}>{html}</Popover>
  };
  customSelectMethodHandler = e => {
    this.app.setState({'способ_погружения':e.value});
    this.setState({selectedOption2:e});

    switch (e.value) {
      case '5':
        this.app.setState({
           'поперечное_сечение':'квадратно_круглая',
           'продольное_сечение':'открытый',
        });
        break;
      case '6':
        this.app.setState({
          'поперечное_сечение':'полые',
          'продольное_сечение':'закрытый',
        });
        break;

    }
  };

  customSelectHandler = (e) => {
    this.app.setState({ 'Su':e.Su });
    this.setState({selectedOption:e});
  };
  customCheckboxHandler = (name,checked) =>{
     this.app.setState({
        [name]:checked,
    })

  };

  getSectionImg = () =>{
    let crossSectionImg = crossSectionSquareImg;
    switch (this.app.state['поперечное_сечение']) {
      case 'круглая': crossSectionImg = crossSectionCircleImg;
        break;
      case 'квадратная': crossSectionImg = crossSectionSquareImg;
        break;
      case 'прямоугольная': crossSectionImg = crossSectionRectangleImg;
        break;
      case 'квадратно_круглая':
        switch (this.app.state['продольное_сечение']) {
          case 'закрытый':
            crossSectionImg = crossSectionSquareImg;
            break;
          case 'открытый':
            crossSectionImg = crossSectionCircleInRectImg;
            break;
        }

        break;
      case 'полые':
        switch (this.app.state['продольное_сечение']) {
          case 'закрытый':
            crossSectionImg = crossSectionCircleImg;
            break;
          case 'открытый':
            crossSectionImg = crossSectionCircleInCircleImg;
            break;
        }
        break;
    }
    return crossSectionImg;
  };
 render() {
    let crossSectionImg = this.getSectionImg();
    if(this.app.state['stype'] === 't2'){
        console.log('awdawd');
        this.getSt2();
    }
    return (
        <div className='container-fluid'>
            <div className="row mt-3">
              <div className="col-md-12">
                <Tabs forceRenderTabPanel selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.setState({ tabIndex })}>
                  <TabList>
                    <Tab>Конструирование ростверка</Tab>
                    <Tab>Параметры сваи</Tab>
                    <Tab>Параметры здания</Tab>
                  </TabList>
                  <TabPanel>
                    <div className={'row'}>
                      <div className="col-md-6 pb-5">
                        <p className="mb-1 mt-3"><ins>Задайте размер свайного ростверка</ins></p>
                        <div className="mt-3">
                          <CustomNumericInput
                              name={'lr'}
                              className="form-control input-text m-0"
                              value={this.app.state['lr']}
                              allowNegative={false}
                              measure={'[м]'}
                              precision={2}
                              labelLeft
                              label={"Длина по x:"}
                              min={0}
                              step={0.1}
                              onValidate={this.validateField}
                              enabledHandlerOnInput={true}
                              isValid={!this.app.state.errors.hasOwnProperty('lr')}
                              onChange={this.customNumericInputHandler}
                          />
                        </div>
                        <div className="mt-3">
                          <CustomNumericInput
                              name={'br'}
                              className="form-control input-text m-0"
                              value={this.app.state['br']}
                              allowNegative={false}
                              measure={'[м]'}
                              precision={2}
                              labelLeft
                              label={"Высота по y:"}
                              min={0}
                              step={0.1}
                              onValidate={this.validateField}
                              enabledHandlerOnInput={true}
                              isValid={!this.app.state.errors.hasOwnProperty('br')}
                              onChange={this.customNumericInputHandler}
                          />
                        </div>
                        <div className="mt-3">
                            <CustomNumericInput
                                name={'hr'}
                                className="form-control input-text"
                                value={this.app.state['hr']}
                                allowNegative={false}
                                measure={'[м]'}
                                label={"Высота ростверка h:"}
                                min={0}
                                max={999}
                                step={0.1}
                                precision={3}
                                labelLeft
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('hr')}
                                onChange={this.customNumericInputHandler}
                            />
                        </div>
                        <div className="mt-3">
                          <Checkbox name={'sizeOptimize'}
                                    checked={this.app.state['sizeOptimize']}
                                    onChange={(v,checked) =>{
                                      this.app.setState({
                                        'sizeOptimize':checked,
                                      });
                                    }}
                          >Изменять размеры ростверка в зависимости от требуемого количества свай(с запасом в 2 сваи)</Checkbox>
                        </div>
                      </div>
                      <div className="col-md-6 text-center py-5">
                        <img width='60%' src={stiltScheme} alt=""/>
                      </div>
                    </div>
                    <div className="d-flex justify-content-between">
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:"1"})} >
                            <img width='25px' height='25px' className={'mr-2'} src={leftArrow} alt=""/>
                            <span className={'mr-2 fz-14'}><b>К Нагрузкам</b></span>
                          </div>
                        </div>
                      </div>
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex + 1})}>
                            <span className={'mr-2 fz-14'}><b>К особенностям здания</b></span>
                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </TabPanel>
                  <TabPanel>
                    <div className={'row'}>
                      <div className="col-md-7 pb-5">
                        <div className="d-flex justify-content-between">
                          <p className="lh-30 mb-0 mr-5">Метод определения несущей способности сваи:</p>
                          <ToggleButtonGroup type="radio" name="метод_определения" onChange={this.toggleButtonsGroupHandler} value={this.app.state['метод_определения']}>
                            <ToggleButton size="sm" variant="custom" value='расчет'>Расчетом</ToggleButton>
                            <ToggleButton size="sm" variant="custom" value='испытания'>Испытаниями</ToggleButton>
                          </ToggleButtonGroup>
                        </div>
                        <div className='mt-3'>
                          <p className="lh-30 mb-0 mr-5">Выбор свайного фундамента по способу изготовления:</p>
                          <select name='stype' className='fz-14 form-control w-75' onChange={this.paramsHandler} value={this.app.state['stype']}>
                            <option value='t1'>Висячие забивные, вдавливаемые всех видов и железобетонные сваи-оболочки, погружаемые без выемки грунта (забивные сваи трения)</option>
                            <option value='t2'>Висячие набивные, буровые и сваи-оболочки, погружаемые с выемкой грунта и заполняемые бетоном (сваи трения)</option>
                            <option value='t3'>Винтовые сваи</option>
                            <option value='t4'>Сваи-стойки</option>
                          </select>
                        </div>
                        { this.app.state['stype'] === 't1' && 
                        <>
                        <div className='mt-3'>
                          <p className="lh-30 mb-0 mr-5">Способ погружения сваи:</p>
                          <Select
                              onChange={this.customSelectMethodHandler}
                              options={this.methodsOptions}
                              value={this.state.selectedOption2}
                              className='w-75'
                          />
                        </div>
                        <div className="row">
                          <div className="col-md-6">
                            <div className='mt-3'>
                              <p className="lh-30 mb-0 mr-5">Вид сваи по сечению</p>
                              <select name='поперечное_сечение' disabled={this.app.state['способ_погружения'] == '6'} className='fz-14 form-control w-100' onChange={this.paramsHandler} value={this.app.state['поперечное_сечение']}>
                                {this.app.state['способ_погружения'] == '5' &&
                                    <>
                                      <option value='квадратно_круглая'>Квадратное с круглой полостью</option>
                                      <option value='полые'>Полые круглого сечения</option>
                                    </>
                                }
                                {this.app.state['способ_погружения'] != '5' &&
                                  <>
                                    <option value='' disabled >Поперечное сечение</option>
                                    <option value='квадратная'>Квадратное</option>
                                    <option value='круглая'>Круглое</option>
                                    <option value='прямоугольная'>Прямоугольное</option>
                                    <option value='квадратно_круглая'>Квадратное с круглой полостью</option>
                                    <option value='полые'>Полые круглого сечения</option>
                                    <option value='' disabled >Продольное сечение</option>
                                    <option value='пирамидальная'>Пирамидальная</option>
                                    <option value='трапецевидная'>Трапецевидная</option>
                                    <option value='ромбовидная'>Ромбовидная</option>
                                  </>
                                }
                              </select>
                            </div>

                            {(this.app.state['поперечное_сечение'] === 'квадратно_круглая' || this.app.state['поперечное_сечение'] === 'полые') &&
                            <div className='mt-3'>
                              <p className="lh-30 mb-0 mr-5">Форма продольного сечения сваи:</p>
                              <select name='продольное_сечение' disabled={this.app.state['способ_погружения'] == '5' || this.app.state['способ_погружения'] == '6'} className='fz-14 form-control w-100' onChange={this.paramsHandler} value={this.app.state['продольное_сечение']}>
                                <option value='открытый'>Открытый нижний конец</option>
                                <option value='закрытый'>Закрытый нижний конец</option>
                               </select>
                            </div>
                            }
                          </div>
                          <div className="col-md-6">
                            <img src={crossSectionImg} className={'mt-2'} alt=""/>
                          </div>
                        </div>
                       {this.app.state['поперечное_сечение'] === 'квадратная' &&
                          <div className="mt-3">
                              <CustomNumericInput
                                  name={'a'}
                                  className="form-control input-text m-0"
                                  value={this.app.state['a']}
                                  allowNegative={false}
                                  measure={'[м]'}
                                  precision={2}
                                  labelLeft
                                  label={"Сторона сечения сваи a:"}
                                  min={0}
                                  step={0.1}
                                  onValidate={this.validateField}
                                  enabledHandlerOnInput={true}
                                  isValid={!this.app.state.errors.hasOwnProperty('a')}
                                  onChange={this.customNumericInputHandler}
                              />
                          </div>
                          }
                          {this.app.state['поперечное_сечение'] === 'круглая' &&
                          <div className="mt-3">
                              <CustomNumericInput
                                  name={'a'}
                                  className="form-control input-text m-0"
                                  value={this.app.state['a']}
                                  allowNegative={false}
                                  measure={'[м]'}
                                  precision={2}
                                  labelLeft
                                  label={"Диаметр сечения сваи D:"}
                                  min={0}
                                  step={0.1}
                                  onValidate={this.validateField}
                                  enabledHandlerOnInput={true}
                                  isValid={!this.app.state.errors.hasOwnProperty('a')}
                                  onChange={this.customNumericInputHandler}
                              />
                          </div>
                          }
                          {this.app.state['поперечное_сечение'] === 'прямоугольная' &&
                          <>
                              <div className="mt-3">
                                  <CustomNumericInput
                                      name={'a'}
                                      className="form-control input-text m-0"
                                      value={this.app.state['a']}
                                      allowNegative={false}
                                      measure={'[м]'}
                                      precision={2}
                                      labelLeft
                                      label={"Длина сечения сваи a:"}
                                      min={0}
                                      step={0.1}
                                      onValidate={this.validateField}
                                      enabledHandlerOnInput={true}
                                      isValid={!this.app.state.errors.hasOwnProperty('a')}
                                      onChange={this.customNumericInputHandler}
                                  />
                              </div>
                              <div className="mt-3">
                                  <CustomNumericInput
                                      name={'b'}
                                      className="form-control input-text m-0"
                                      value={this.app.state['b']}
                                      allowNegative={false}
                                      measure={'[м]'}
                                      precision={2}
                                      labelLeft
                                      label={"Ширина сечения сваи b:"}
                                      min={0}
                                      step={0.1}
                                      onValidate={this.validateField}
                                      enabledHandlerOnInput={true}
                                      isValid={!this.app.state.errors.hasOwnProperty('b')}
                                      onChange={this.customNumericInputHandler}
                                  />
                              </div>
                          </>
                          }
                          {this.app.state['поперечное_сечение'] === 'квадратно_круглая' &&
                          <>
                              <div>
                                  <div className="mt-3">
                                      <CustomNumericInput
                                          name={'a'}
                                          className="form-control input-text m-0"
                                          value={this.app.state['a']}
                                          allowNegative={false}
                                          measure={'[м]'}
                                          precision={2}
                                          labelLeft
                                          label={"Размер сечения a:"}
                                          min={0}
                                          step={0.1}
                                          onValidate={this.validateField}
                                          enabledHandlerOnInput={true}
                                          isValid={!this.app.state.errors.hasOwnProperty('a')}
                                          onChange={this.customNumericInputHandler}
                                      />
                                  </div>
                                {this.app.state['продольное_сечение'] !== 'закрытый' &&
                                  <div className="mt-3">
                                    <CustomNumericInput
                                        name={'d'}
                                        className="form-control input-text m-0"
                                        value={this.app.state['d']}
                                        allowNegative={false}
                                        measure={'[м]'}
                                        precision={2}
                                        labelLeft
                                        label={"Диаметр полости d:"}
                                        min={0}
                                        step={0.1}
                                        onValidate={this.validateField}
                                        enabledHandlerOnInput={true}
                                        isValid={!this.app.state.errors.hasOwnProperty('d')}
                                        onChange={this.customNumericInputHandler}
                                    />
                                  </div>
                                }
                              </div>
                          </>
                          }
                          {this.app.state['поперечное_сечение'] === 'полые' &&
                          <>
                            <div className="mt-3">
                                <CustomNumericInput
                                    name={'a'}
                                    className="form-control input-text m-0"
                                    value={this.app.state['a']}
                                    allowNegative={false}
                                    measure={'[м]'}
                                    precision={2}
                                    labelLeft
                                    label={"Диаметр сечения внешнего кольца D"}
                                    min={0}
                                    step={0.1}
                                    onValidate={this.validateField}
                                    enabledHandlerOnInput={true}
                                    isValid={!this.app.state.errors.hasOwnProperty('a')}
                                    onChange={this.customNumericInputHandler}
                                />
                            </div>
                            {this.app.state['продольное_сечение'] !== 'закрытый' &&
                            <div className="mt-3">
                              <CustomNumericInput
                                  name={'d'}
                                  className="form-control input-text m-0"
                                  value={this.app.state['d']}
                                  allowNegative={false}
                                  measure={'[м]'}
                                  precision={2}
                                  labelLeft
                                  label={"Диаметр сечения внутреннего кольца d"}
                                  min={0}
                                  step={0.1}
                                  onValidate={this.validateField}
                                  enabledHandlerOnInput={true}
                                  isValid={!this.app.state.errors.hasOwnProperty('d')}
                                  onChange={this.customNumericInputHandler}
                              />
                            </div>
                            }
                          </>
                          }
                          {this.app.state['поперечное_сечение'] === 'пирамидальная' &&
                          <>
                              <div className="mt-3">
                                  <CustomNumericInput
                                      name={'a'}
                                      className="form-control input-text m-0"
                                      value={this.app.state['a']}
                                      allowNegative={false}
                                      measure={'[м]'}
                                      precision={2}
                                      labelLeft
                                      label={"Верхнее сечение сваи a:"}
                                      min={0}
                                      step={0.1}
                                      onValidate={this.validateField}
                                      enabledHandlerOnInput={true}
                                      isValid={!this.app.state.errors.hasOwnProperty('a')}
                                      onChange={this.customNumericInputHandler}
                                  />
                              </div>
                              <div className="mt-3">
                                  <CustomNumericInput
                                      name={'d'}
                                      className="form-control input-text m-0"
                                      value={this.app.state['d']}
                                      allowNegative={false}
                                      measure={'[м]'}
                                      precision={2}
                                      labelLeft
                                      label={"Нижнее сечение сваи b"}
                                      min={0}
                                      step={0.1}
                                      onValidate={this.validateField}
                                      enabledHandlerOnInput={true}
                                      isValid={!this.app.state.errors.hasOwnProperty('d')}
                                      onChange={this.customNumericInputHandler}
                                  />
                              </div>
                          </>
                          }
                          {this.app.state['поперечное_сечение'] === 'трапецевидная' &&
                          <>
                              <div className="mt-3">
                                  <CustomNumericInput
                                      name={'a'}
                                      className="form-control input-text m-0"
                                      value={this.app.state['a']}
                                      allowNegative={false}
                                      measure={'[м]'}
                                      precision={2}
                                      labelLeft
                                      label={"Верхнее сечение сваи a:"}
                                      min={0}
                                      step={0.1}
                                      onValidate={this.validateField}
                                      enabledHandlerOnInput={true}
                                      isValid={!this.app.state.errors.hasOwnProperty('a')}
                                      onChange={this.customNumericInputHandler}
                                  />
                              </div>
                              <div className="mt-3">
                                  <CustomNumericInput
                                      name={'d'}
                                      className="form-control input-text m-0"
                                      value={this.app.state['d']}
                                      allowNegative={false}
                                      measure={'[м]'}
                                      precision={2}
                                      labelLeft
                                      label={"Нижнее сечение сваи b"}
                                      min={0}
                                      step={0.1}
                                      onValidate={this.validateField}
                                      enabledHandlerOnInput={true}
                                      isValid={!this.app.state.errors.hasOwnProperty('d')}
                                      onChange={this.customNumericInputHandler}
                                  />
                              </div>
                              <div className="mt-3">
                                  <CustomNumericInput
                                      name={'hw'}
                                      className="form-control input-text m-0"
                                      value={this.app.state['hw']}
                                      allowNegative={false}
                                      measure={'[м]'}
                                      precision={2}
                                      labelLeft
                                      label={"Расстояние до расширения h"}
                                      min={0}
                                      step={0.1}
                                      onValidate={this.validateField}
                                      enabledHandlerOnInput={true}
                                      isValid={!this.app.state.errors.hasOwnProperty('hw')}
                                      onChange={this.customNumericInputHandler}
                                  />
                              </div>
                          </>

                          }
                        {this.app.state['поперечное_сечение'] === 'ромбовидная' &&
                        <>
                          <div className="mt-3">
                            <CustomNumericInput
                                name={'a'}
                                className="form-control input-text m-0"
                                value={this.app.state['a']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                labelLeft
                                label={"Верхнее сечение сваи a:"}
                                min={0}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('a')}
                                onChange={this.customNumericInputHandler}
                            />
                          </div>
                          <div className="mt-3">
                            <CustomNumericInput
                                name={'d'}
                                className="form-control input-text m-0"
                                value={this.app.state['d']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                labelLeft
                                label={"Нижнее сечение сваи b"}
                                min={0}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('d')}
                                onChange={this.customNumericInputHandler}
                            />
                          </div>
                          <div className="mt-3">
                            <CustomNumericInput
                                name={'hw'}
                                className="form-control input-text m-0"
                                value={this.app.state['hw']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                labelLeft
                                label={"Расстояние до расширения h"}
                                min={0}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('hw')}
                                onChange={this.customNumericInputHandler}
                            />
                          </div>
                          <div className="mt-3">
                            <CustomNumericInput
                                name={'sw'}
                                className="form-control input-text m-0"
                                value={this.app.state['sw']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                labelLeft
                                label={"Сечение широкой части"}
                                min={0}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('sw')}
                                onChange={this.customNumericInputHandler}
                            />
                          </div>
                          <div className="mt-3">
                            <Checkbox name={'twoEdges'}
                                      checked={this.app.state['twoEdges']}
                                      onChange={(v,checked) =>{
                                        this.app.setState({
                                          'twoEdges':checked,
                                        });
                                      }}
                            >Две наклонные грани сваи</Checkbox>
                          </div>
                        </>

                        }
                        {(this.app.state['способ_погружения'] === '6') &&
                        <div className="mt-3">
                          <CustomNumericInput
                              name={'dk'}
                              className="form-control input-text m-0"
                              value={this.app.state['dk']}
                              allowNegative={false}
                              measure={'[м]'}
                              precision={2}
                              labelLeft
                              label={"Диаметр наибольшего камуфлетного уширения"}
                              min={0}
                              step={0.1}
                              onValidate={this.validateField}
                              enabledHandlerOnInput={true}
                              isValid={!this.app.state.errors.hasOwnProperty('dk')}
                              onChange={this.customNumericInputHandler}
                          />
                        </div>
                        }
                          <p className="lh-30 mb-0 mr-5"><ins>При невыполнении условия по несущей способности задайте пределы изменения параметров</ins></p>
                          <div className="mt-3">
                            <CustomNumericInput
                                name={'lm'}
                                className="form-control input-text m-0"
                                value={this.app.state['lm']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                labelLeft
                                label={"Длина сваи до:"}
                                min={0}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('lm')}
                                onChange={this.customNumericInputHandler}
                            />
                          </div>
                          <div className="mt-3">
                            <CustomNumericInput
                                name={'am'}
                                className="form-control input-text m-0"
                                value={this.app.state['am']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                labelLeft
                                label={"Сечение сваи до:"}
                                min={0}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('am')}
                                onChange={this.customNumericInputHandler}
                            />
                          </div>
                          </>
                      }
                      { this.app.state['stype'] === 't2' && 
                        <>
                        <div className='mt-3'>
                          <p className="lh-30 mb-0 mr-5">Способ погружения сваи:</p>
                          <Select
                              onChange={this.customSelectMethodHandler}
                              options={this.methodOptions2}
                              value={this.state.selectedOption2}
                              className='w-75'
                          />
                        </div>
                          <div className="mt-3">
                            <Checkbox name={'dkt'}
                                      checked={this.app.state['dkt']}
                                      onChange={(v,checked) =>{
                                        this.app.setState({
                                          'dkt':checked,
                                        });
                                      }}
                            >Сваи устраиваемые с уширением</Checkbox>
                          </div>
                          <div className="mt-3">
                            <CustomNumericInput
                                name={'d'}
                                className="form-control input-text m-0"
                                value={this.app.state['d']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                labelLeft
                                label={"Диаметр скважины d:"}
                                min={0}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('d')}
                                onChange={this.customNumericInputHandler}
                            />
                          </div>
                        <div className="mt-3">
                          <CustomNumericInput
                              name={'dk'}
                              className="form-control input-text m-0"
                              value={this.app.state['dk']}
                              allowNegative={false}
                              measure={'[м]'}
                              precision={2}
                              labelLeft
                              label={"Диаметр уширение скважины D:"}
                              min={0}
                              step={0.1}
                              onValidate={this.validateField}
                              enabledHandlerOnInput={true}
                              isValid={!this.app.state.errors.hasOwnProperty('dk')}
                              onChange={this.customNumericInputHandler}
                          />
                        </div>
                          </>
                      }
                        </div>
                    </div>
                    <div className="d-flex justify-content-between">
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex - 1})} >
                            <img width='25px' height='25px'  className={'mr-2'} src={leftArrow} alt=""/>
                            <span className={'mr-2 fz-14'}><b>К характеристикам фундамента</b></span>
                          </div>
                        </div>
                      </div>
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex + 1})}>
                            <span className={'mr-2 fz-14'}><b>К параметрам подколонника</b></span>
                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </TabPanel>
                  <TabPanel>
                    <div className={'row'}>
                      <div className="col-md-7 pb-5">
                        <div className="d-flex justify-content-between mt-2">
                          <p className="lh-30 mb-0 mr-5">Геотех. категория</p>
                          <Whisper placement="top" delay={500} trigger="hover" speaker={this.popover(
                              <div className={'text-left'}>
                                <p className={'mb-1'}>Категорию сложности инженерно-геологических условий строительства <br/> следует определять  в соответствии с СП 47.13330. Таблица А.1.
                                </p>
                                <p className={'mb-1'}>I - (простая)</p>
                                <p className={'mb-1'}>II - (средняя)</p>
                                <p className={'mb-1'}>III - (сложная)</p>
                              </div>
                          )} >
                            <ToggleButtonGroup type="radio" onChange={this.toggleButtonsGroupHandler} name="геотехническая_категория" value={this.app.state['геотехническая_категория']}>
                              <ToggleButton size="sm" variant="custom" value={"1"}>I</ToggleButton>
                              <ToggleButton size="sm" variant="custom" value={"2"}>II</ToggleButton>
                              <ToggleButton size="sm" variant="custom" value={"3"}>III</ToggleButton>
                            </ToggleButtonGroup>
                          </Whisper>
                        </div>
                        <div className="d-flex justify-content-between mt-3">
                          <p className="lh-30 mb-0 mr-5">Уровень ответственности</p>
                          <ToggleButtonGroup type="radio" name="Yn" onChange={this.toggleButtonsGroupHandler} value={this.app.state['Yn']}>
                            <ToggleButton size="sm" variant="custom" value={1.1}>Повышенный</ToggleButton>
                            <ToggleButton size="sm" variant="custom" value={1}>Нормальный</ToggleButton>
                            <ToggleButton size="sm" variant="custom" value={0.9}>Пониженный</ToggleButton>
                          </ToggleButtonGroup>
                        </div>
                        <div className="mt-3 d-flex justify-content-between">
                          <p className="lh-30 mb-0">Конструкция здания:</p>
                          <Select
                              onChange={this.customSelectHandler}
                              options={this.options}
                              value={this.state.selectedOption}
                              className='w-50'
                          />
                        </div>
                        <div className="mt-3">
                          <CustomNumericInput
                              name={'Su'}
                              className="form-control input-text"
                              value={this.app.state.Su}
                              allowNegative={false}
                              measure={'[см]'}
                              labelLeft={true}
                              label={"Предельное значение осадки S<sub>u</sub>:"}
                              step={0.1}
                              min={0}
                              onValidate={this.validateField}
                              enabledHandlerOnInput={true}
                              isValid={!this.app.state.errors.hasOwnProperty('Su')}
                              onChange={this.customNumericInputHandler}
                          />
                        </div>
                        <p className={'mt-3'}><b><ins>Характеристики арматуры и бетона</ins></b></p>
                        <div className="d-flex  justify-content-between ">
                          <p className="lh-30 mb-0">Класс арматуры:</p>
                          <select name='класс_арматуры' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['класс_арматуры']}>
                            <option value='A240'>А240</option>
                            <option value='A400'>А400</option>
                            <option value='A500'>А500</option>
                            <option value='A600'>А600</option>
                            <option value='A800'>А800</option>
                            <option value='A1000'>А1000</option>
                          </select>
                        </div>
                        <div className="d-flex  justify-content-between mt-2">
                          <p className="lh-30 mb-0">Класс бетона свай:</p>
                          <select name='класс_бетона' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['класс_бетона']}>
                            <option value='B15'>B15</option>
                            <option value='B20'>B20</option>
                            <option value='B25'>B25</option>
                            <option value='B30'>B30</option>
                            <option value='B35'>B35</option>
                            <option value='B40'>B40</option>
                            <option value='B45'>B45</option>
                            <option value='B50'>B50</option>
                            <option value='B55'>B55</option>
                            <option value='B60'>B60</option>
                            <option value='B70'>B70</option>
                            <option value='B80'>B80</option>
                            <option value='B90'>B90</option>
                            <option value='B100'>B100</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="d-flex justify-content-between">
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex - 1})} >
                            <img width='25px' height='25px' className={'mr-2'} src={leftArrow} alt=""/>
                            <span className={'mr-2 fz-14'}><b>К особенностям здания</b></span>
                          </div>
                        </div>
                      </div>
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:"2"})}>
                            <span className={'mr-2 fz-14'}><b>К Геологии</b></span>
                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                          </div>
                        </div>
                      </div>
                    </div>

                  </TabPanel>
                </Tabs>
              </div>
            </div>
        </div>
    );
  }
}

export default StiltFoundationSolve;
