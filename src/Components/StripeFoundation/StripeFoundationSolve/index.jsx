import React, { Component } from 'react';
import { ToggleButton,ToggleButtonGroup} from 'react-bootstrap'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Popover,Checkbox} from 'rsuite/lib/index';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-select/dist/css/bootstrap-select.min.css';
import 'react-tabs/style/react-tabs.css';
import './index.css';
import CustomNumericInput from "../../CustomNumericInput/CustomNumericInput";
import StripeBasementProps from "../StripeBasementProps/index.jsx";
import ConstructProperties from "../../ConstructProperties/index.jsx";
import { isEmpty } from "../../Global/Global";
import rightArrow from "../../../img/right-arrow.svg";
import leftArrow from "../../../img/left-arrow.svg";

 
class StripeFoundationSolve extends Component {

  state ={
    su:null,
    selectedOption:{
      Su: 12,
      isDisabled: 0,
      label: "крупных панелей",
      value: 2
    },
    tabIndex:0,
  }

  wrapper = this.props.wrapper;
  app = this.props.app;
  options = this.app.optionsSu;
  paramsHandler = this.wrapper.paramsHandler;
  toggleButtonsGroupHandler = this.wrapper.toggleButtonsGroupHandler;

  customSelectHandler = (e) => {
    this.app.setState({ 'Su':e.Su });
    this.setState({selectedOption:e});

  };
  validateField = (props) => {

    const value = props.value;
    const name = props.name;
    let isValid = true,
        textError = 'Заполните поле';

    switch (name) {
      case 'hn':
        if(this.app.state['наличие_подготовки']){
          if(isEmpty(value)) {
            isValid = false;
            textError = 'Заполните поле';
          }

          if(value < 0.1 || value > 1){
            isValid = false;
            textError = 'hn должно быть не более 1 м и не менее 0.1 м';
          }
        }
        break;
      case 'Su':
        if (value < 0) {
          isValid = false;
          textError = 'Осадка не может быть отрицательной';
        }
        if(isEmpty(value)){
          isValid = false;
          textError = "Заполните поле";
        }
        break;
      case 'kh_temp':
        if(value < 0){
          isValid = false;
          textError = 'Температура не может быть отрицательной, введите корректные данные';
        }
        if(value > 45){
          isValid = false;
          textError = 'Температура должна быть не более 45';
        }
        if(isEmpty(value)) isValid = false;
        break;

      case 'Yc_l':
          if(value > 120){
            isValid = false;
            textError = 'L не более 120 м';
          }
          if(value < 1){
            isValid = false;
            textError = 'L не менее 1 м';
          }
          if(isEmpty(value)){
            isValid = false;
            textError = 'Заполните поле';
          }
          break;
      case 'Yc_h':
        if(value > 60){
          isValid = false;
          textError = 'H не более 60 м';
        }
        if(value < 1){
          isValid = false;
          textError = 'H не менее 1 м';
        }
        if(isEmpty(value)){
          isValid = false;
          textError = 'Заполните поле';
        }
        break;
    }

    if(value < 0){
      isValid = false;
      textError = 'Значение не может быть отрицательным';
    }
    return {
      isValid: isValid,
      textError: (isValid) ? '' : textError
    }
  }
  customNumericInputHandler = (value, name , validator) =>{

      let errors = this.app.state.errors;
      if(!validator.isValid) {
          errors[name] = {
            textError: validator.textError,
            tab: '3'
          };
      }else{
          delete  errors[name];
      }

      this.app.setState({
        [name]: value,
        errors:errors
      });
  };
  popover = (html) =>{
    return <Popover visible style={{
      color:"#fff"
    }}>{html}</Popover>
  }
  customCheckboxHandler = (name,checked) =>{
     this.app.setState({
        [name]:checked,
    })
  }

 render() {
    return (
        <div className='container-fluid'>
            <div className="row mt-3">
              <div className="col-md-12">
                <Tabs forceRenderTabPanel selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.setState({ tabIndex })}>
                  <TabList>
                    <Tab>Характеристики фундамента</Tab>
                    <Tab>Параметры подвала</Tab>
                    <Tab>Конструктивные особенности здания</Tab>

                  </TabList>
                  <TabPanel>
                    <div className={'row'}>
                      <div className="col-md-6 pb-5">
                        <p className={'mt-3'}><b><ins>Вариант исполнения фундамента</ins></b></p>
                        <ToggleButtonGroup type="radio" name="тип_фундамента" value={this.app.state['тип_фундамента']} onChange={this.toggleButtonsGroupHandler}>
                          <ToggleButton size="sm" variant="custom" value={'монолитный'}>Монолитный фундамент</ToggleButton>
                          <ToggleButton size="sm" variant="custom" value={'блочный'}>Из ФБС блоков</ToggleButton>
                          <ToggleButton size="sm" variant="custom" value={'комбинированный'}>Комбинированный</ToggleButton>
                        </ToggleButtonGroup>

                        {this.app.state['тип_фундамента'] === 'монолитный' &&
                            <div>
                              <p className={'mt-3'}><b><ins>Вариант опирания перекрытия на стену</ins></b></p>
                              <ToggleButtonGroup type="radio" name="op" value={this.app.state['op']} onChange={this.toggleButtonsGroupHandler}>
                                <ToggleButton size="sm" variant="custom" value={false}>Монолитное</ToggleButton>
                                <ToggleButton size="sm" variant="custom" value={true}>Шарнирное</ToggleButton>
                              </ToggleButtonGroup>
                            </div>
                        }
                        <p className={'mt-3'}><b><ins>Конструктивные особенности фундамента</ins></b></p>
                        <div className="d-flex  justify-content-between">
                          <p className="lh-30 mb-0 mr-2">Фундамент под:</p>
                          <ToggleButtonGroup type="radio" onChange={this.toggleButtonsGroupHandler} name="фундамент_под" value={this.app.state['фундамент_под']}>
                            <ToggleButton size="sm" variant="custom" value={'наружную_стену'}>Наружную стену</ToggleButton>
                            <ToggleButton size="sm" variant="custom" value={'внутреннюю_стену'}>Внутреннюю стену</ToggleButton>
                          </ToggleButtonGroup>
                        </div>
                        <div className="mt-3">
                          <Checkbox name={'наличие_подготовки'} checked={this.app.state['наличие_подготовки']} onChange={(v,checked) => this.customCheckboxHandler('наличие_подготовки',checked)} >Наличие подготовки под фундамент</Checkbox>
                          {(this.app.state['наличие_подготовки']) &&
                          <CustomNumericInput
                              name={'hn'}
                              className="form-control input-text"
                              value={this.app.state['hn']}
                              allowNegative={false}
                              measure={'[м]'}
                              precision={2}
                              min={0}
                              labelLeft={true}
                              label={"Толщина подготовки h<sub>n</sub>:"}
                              step={0.1}
                              onValidate={this.validateField}
                              enabledHandlerOnInput={true}
                              isValid={!this.app.state.errors.hasOwnProperty('hn')}
                              onChange={this.customNumericInputHandler}
                          />
                          }
                        </div>
                        {this.app.state['тип_фундамента'] !== 'блочный' &&

                        <div>
                          <p className={'mt-3'}><b><ins>Характеристики арматуры и бетона</ins></b></p>
                            <div className="d-flex  justify-content-between ">
                              <p className="lh-30 mb-0">Класс арматуры:</p>
                              <select name='класс_арматуры' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['класс_арматуры']}>
                                <option value='A240'>А240</option>
                                <option value='A400'>А400</option>
                                <option value='A500'>А500</option>
                                <option value='A600'>А600</option>
                                <option value='A800'>А800</option>
                                <option value='A1000'>А1000</option>
                              </select>
                            </div>
                            <div className="d-flex  justify-content-between mt-2">
                              <p className="lh-30 mb-0">Класс бетона:</p>
                              <select name='класс_бетона' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['класс_бетона']}>
                                <option value='B15'>B15</option>
                                <option value='B20'>B20</option>
                                <option value='B25'>B25</option>
                                <option value='B30'>B30</option>
                                <option value='B35'>B35</option>
                                <option value='B40'>B40</option>
                                <option value='B45'>B45</option>
                                <option value='B50'>B50</option>
                                <option value='B55'>B55</option>
                                <option value='B60'>B60</option>
                                <option value='B70'>B70</option>
                                <option value='B80'>B80</option>
                                <option value='B90'>B90</option>
                                <option value='B100'>B100</option>
                              </select>
                            </div>
                          </div>



                        }


                      </div>
                    </div>
                    <div className="d-flex justify-content-between">
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:"1"})} >
                            <img width='25px' height='25px' className={'mr-2'} src={leftArrow} alt=""/>
                            <span className={'mr-2 fz-14'}><b>К Нагрузкам</b></span>
                          </div>
                        </div>
                      </div>
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex + 1})}>
                            <span className={'mr-2 fz-14'}><b>К параметрам подвала</b></span>
                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </TabPanel>
                  <TabPanel>
                    <StripeBasementProps context={this} />
                    <div className="d-flex justify-content-between">
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex - 1})} >
                            <img width='25px' height='25px' className={'mr-2'} src={leftArrow} alt=""/>
                            <span className={'mr-2 fz-14'}><b>К характеристикам фундамента</b></span>
                          </div>
                        </div>
                      </div>
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex + 1})} >
                            <span className={'mr-2 fz-14'}><b>К особенностям здания</b></span>
                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </TabPanel>
                  <TabPanel>
                    <ConstructProperties basement={this.app.state['наличие_подвала']} isStripe context={this}/>
                    <div className="d-flex justify-content-between">
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex - 1})} >
                            <img width='25px' height='25px'  className={'mr-2'} src={leftArrow} alt=""/>
                            <span className={'mr-2 fz-14'}><b>К параметрам подвала</b></span>
                          </div>
                        </div>
                      </div>
                      <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                          <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:'2'})}>
                            <span className={'mr-2 fz-14'}><b>К геологии</b></span>
                            <img width='25px' height='25px' src={rightArrow} alt=""/>
                          </div>
                        </div>
                      </div>
                    </div>
                  </TabPanel>

                </Tabs>
              </div>
            </div>


        </div>
    );
  }
}

export default StripeFoundationSolve;
