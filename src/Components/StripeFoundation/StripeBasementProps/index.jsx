import React, { Component } from 'react';
import { ToggleButton,ToggleButtonGroup} from 'react-bootstrap'
import { Popover,Checkbox} from 'rsuite/lib/index';
import { ReactSVG } from 'react-svg'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-select/dist/css/bootstrap-select.min.css';
import 'react-tabs/style/react-tabs.css';
import  basementScheme from "../../../img/basementScheme.svg";
import  basementScheme2 from "../../../img/basementScheme2.svg";
import  $ from 'jquery';
import './index.css';
import CustomNumericInput from "../../CustomNumericInput/CustomNumericInput";
import {isEmpty,precision,round} from "../../Global/Global";
import surveysImg from "../../../img/stripSurveysImg.svg";
 
class StripeBasementProps extends Component {

  state ={
    su:null,
    selectedOption:{
      Su: 12,
      isDisabled: 0,
      kren: "",
      label: "крупных панелей",
      value: 2
    },
    tabIndex:0,
  }


  context = this.props.context;
  wrapper = this.context.props.wrapper;
  app = this.context.props.app;
  options = this.context.app.optionsSu;
  paramsHandler = this.context.wrapper.paramsHandler;
  toggleButtonsGroupHandler = this.context.wrapper.toggleButtonsGroupHandler;
  FBS = this.app.state['тип_фундамента'] === 'блочный';

  customSelectHandler = (e) => {
    this.app.setState({ 'Su':e.Su });
    this.setState({selectedOption:e});
    if(!isEmpty(e.kren)){
      this.app.setState({
          'крен':true,
          iu:e.kren,
      })
    }else{
      this.app.setState({
        'крен':false,
        iu:'',
      })
    }

  };
  validateField = (props) => {

    const value = props.value;
    const name = props.name;
    let isValid = true,
        textError = 'Заполните поле';

    switch (name) {
        case 'hcf':
            if(value > 0.4 || value < 0.05 || isEmpty(value)){
                textError = 'Толщина пола должна быть в пределах  0.05-0.4 м';
                isValid = false;
            }
            break;
        case 'hbase':
            if(value > 6 || value < 0.05 || isEmpty(value)){
                textError = 'Высота подвала h должна быть в пределах 0.3-6 м';
                isValid = false;
            }
            break;
        case 'hs':
            if((value > 1 || value < 0.3 || isEmpty(value)) && !(this.app.state['тип_фундамента'] === 'блочный')){
                textError = 'Высота плитной части hs должна быть в пределах 0,3-1 м';
                isValid = false;
            }
            break;
        case 'q':
            if(value > 200 || value < 0 || isEmpty(value)){
                textError = 'Нагрузка q должна быть в пределах от 0-200 кПа';
                isValid = false;
            }
            break;
        case 'bt':
            if(value > 45 || value < 0 || isEmpty(value)){
                textError = 'Температура должна быть не более 45 C<sub>o</sub>';
                isValid = false;
            }
            break;
        case 'b_':
            if(value > 1 || value < 0.2 || isEmpty(value)){
                textError = 'Толщина стенки b’ должна быть в пределах от 0,2-1 м';
                isValid = false;
            }
            break;
        case 'глубина_заложения':
            if(value > 7 || value < 0.6 || isEmpty(value)){
                textError = 'Глубина заложения d должна быть в пределах от 0,6-7 м';
                isValid = false;
            }
            if(value > this.app.state['высота_фундамента']){
                textError = 'Верх фундамента должен быть не ниже уровня земли';
                isValid = false;
            }
            break;
        case 'высота_фундамента':
            if(value > 8 || value < 0.6 || isEmpty(value)){
                textError = 'Высота фундамента hf должна быть в пределах от 0,6-8 м';
                isValid = false;
            }
            if(value < this.app.state['глубина_заложения']){
                textError = 'Верх фундамента должен быть не ниже уровня земли';
                isValid = false;
            }
            break;
    }
    if(this.app.state['привязка_к_абсолютным_отметкам']){
        switch (name) {
            case 'отметка_поверхности':
            case 'отметка_верха':
            case 'отметка_подошвы':
            case 'отметка_подвала':
                if(isEmpty(value)) {
                    isValid = false;
                    textError = 'Не задана ' + name.split('_').join(' ');
                }
                break;

        }
    }
    return {
      isValid: isValid,
      textError: (isValid) ? '' : textError
    }
  }

  customNumericInputHandler = (value, name , validator) =>{

      let errors = this.app.state.errors;
      if(!validator.isValid) {
          errors[name] = {
            textError: validator.textError,
            tab: '3'
          };
      }else{
          delete  errors[name];
      }

      this.app.setState({
        [name]: value,
        errors:errors
      });


      if(name === 'hs' || name === 'hbase' || name === 'hcf'){
          if(this.app.state['тип_фундамента'] === 'блочный'){
              this.app.setState({
                  'высота_фундамента':round(0.5 + parseFloat(this.app.state.hbase) + parseFloat(this.app.state.hcf),3),
              })
          }else{
              this.app.setState({
                  'высота_фундамента':round(parseFloat(this.app.state.hs) + parseFloat(this.app.state.hbase) + parseFloat(this.app.state.hcf),3),
              })
          }

      }
  };
  popover = (html) =>{
    return <Popover visible style={{
      color:"#fff"
    }}>{html}</Popover>
  }

  customCheckboxHandler = (name,checked) =>{
     this.app.setState({
        [name]:checked,
    })
  };
    lineHandler = (lines,state,color = 'black') => {
        if(state === 'show'){
            for(let i = 0; i < lines.length;i++){
                if(lines[i].hasAttribute('stroke')){
                    lines[i].setAttribute('stroke',color);
                    lines[i].setAttribute('stroke-width','1px');
                }else{
                    lines[i].setAttribute('fill',color);
                }
            }
        }
        if(state === 'hide'){
            for(let i = 0; i < lines.length;i++){
                if(lines[i].hasAttribute('stroke')){
                    lines[i].setAttribute('stroke','none');
                    lines[i].setAttribute('stroke-width','3px');
                }else{
                    lines[i].setAttribute('fill','none');
                }
            }
        }
    };

    toggleLinesFor = (e) => {
        let id = e.currentTarget.id;
        let isHide = e.type === 'mouseout';
        let isShow = e.type === 'mouseover';
        let lines;
        let svg = document.getElementById('basementScheme');

        lines = svg.getElementsByClassName(id);
        this.lineHandler(lines,isShow?'show':'hide','red');

    }

  handleTracksSvgLines = (svg) =>{
      if(!svg) svg = document.getElementById('basementScheme');
      let svgDoc = svg;
      let isAbs = this.app.state['привязка_к_абсолютным_отметкам'];
      let showLines = svgDoc.getElementsByClassName(isAbs?'abs':'notAbs');
      let hideLines = svgDoc.getElementsByClassName(isAbs?'notAbs':'abs');
      this.lineHandler(showLines,'show');
      this.lineHandler(hideLines,'hide');
  }


  tracksHandler = (e) => {
    let name = e.target.name;
    let value = e.target.value.replace(/[,]/g, ".");
    if(precision(value) > 3) return false;
    if(!$.isNumeric(value) && (value !== '-' && !isEmpty(value))) return false;

      let validator = this.validateField({
          value:value,
          name:name
      });

      let errors = this.app.state.errors;
      if(!validator.isValid) {

          e.target.classList.add('error-table');
          errors[name] = {
              textError: validator.textError,
              tab: '3'
          };

      }else{
          e.target.classList.remove('error-table');
          delete  errors[name];

      }
      this.app.setState({
          [name]: value,
          errors:errors
      });

    if(name === 'отметка_верха' || name === 'отметка_подвала'){
        let val1 = Math.abs(this.app.state['отметка_верха']) - Math.abs(this.app.state['отметка_подвала']);
        this.app.setState({
          hbase:Math.abs(round(val1,3))
        })
    }
    if(name === 'отметка_подвала' || name === 'отметка_подошвы'){
      this.app.setState({
        hs:Math.abs(round(Math.abs(this.app.state['отметка_подвала']) - Math.abs(this.app.state['отметка_подошвы']),3))
      })
    }
    if(name === 'отметка_поверхности' || name === 'отметка_подошвы'){
      this.app.setState({
        'глубина_заложения':Math.abs(round(Math.abs(this.app.state['отметка_поверхности']) - Math.abs(this.app.state['отметка_подошвы']),3))
      })
    }
    if(name === 'отметка_верха' || name === 'отметка_подошвы'){
        let val = Math.abs(round(Math.abs(this.app.state['отметка_верха']) - Math.abs(this.app.state['отметка_подошвы']),3)) + parseFloat(this.app.state.hcf);
        this.app.setState({
        'высота_фундамента':round(val,3)
        })
    }

  };

 render() {
    return (
        <div>
            <div className={'row'}>
                <div className="col-md-7 pb-5">
                  <div className="mt-3">
                    <Checkbox name={'привязка_к_абсолютным_отметкам'} checked={this.app.state['привязка_к_абсолютным_отметкам']} onChange={(v,checked) => this.customCheckboxHandler('привязка_к_абсолютным_отметкам',checked)} >Задать размеры отметками</Checkbox>
                  </div>
                  <p className="mb-1 mt-3">Наличие подвала:</p>
                  <ToggleButtonGroup type="radio" name="наличие_подвала" value={this.app.state['наличие_подвала']} onChange={this.toggleButtonsGroupHandler}>
                    <ToggleButton size="sm" variant="custom" value={true}>Подвальное</ToggleButton>
                    <ToggleButton size="sm" variant="custom" value={false}>Бесподвальное</ToggleButton>
                  </ToggleButtonGroup>
                  {this.app.state['наличие_подвала'] &&
                  <div>
                      <div className="mt-3">
                        <Checkbox name={'отапливаемость_подвала'} checked={this.app.state['отапливаемость_подвала']} onChange={(v,checked) => this.customCheckboxHandler('отапливаемость_подвала',checked)} >Отаплеваемый подвал</Checkbox>
                      </div>
                    <div className="mt-3">
                      <CustomNumericInput
                          name={'hcf'}
                          className="form-control input-text"
                          value={this.app.state['hcf']}
                          allowNegative={false}
                          measure={'[м]'}
                          precision={3}
                          min={0}
                          labelLeft={true}
                          label={"Толщина конструкции пола подвала h<sub>cf</sub>:"}
                          step={0.1}
                          onValidate={this.validateField}
                          enabledHandlerOnInput={true}
                          isValid={!this.app.state.errors.hasOwnProperty('hcf')}
                          onChange={this.customNumericInputHandler}
                          onMouseOver={this.toggleLinesFor}
                          onMouseOut={this.toggleLinesFor}
                      />
                    </div>
                    <div>
                      <div className="mt-3">
                        <CustomNumericInput
                            name={'hbase'}
                            className="form-control input-text"
                            value={this.app.state.hbase}
                            disabled={this.app.state['привязка_к_абсолютным_отметкам']}
                            allowNegative={false}
                            measure={'[м]'}
                            precision={3}
                            min={0}
                            labelLeft={true}
                            label={"Высота подвала h:"}
                            step={0.1}
                            onValidate={this.validateField}
                            enabledHandlerOnInput={true}
                            isValid={!this.app.state.errors.hasOwnProperty('hbase')}
                            onChange={this.customNumericInputHandler}
                            onMouseOver={this.toggleLinesFor}
                            onMouseOut={this.toggleLinesFor}
                        />
                      </div>
                        {!(this.app.state['тип_фундамента'] === 'блочный') &&
                        <div className="mt-3">
                            <CustomNumericInput
                                name={'hs'}
                                className="form-control input-text"
                                value={this.app.state['hs']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={3}
                                disabled={this.app.state['привязка_к_абсолютным_отметкам']}
                                min={0}
                                labelLeft={true}
                                label={"Высота плитной части hs:"}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('hs')}
                                onChange={this.customNumericInputHandler}
                                onMouseOver={this.toggleLinesFor}
                                onMouseOut={this.toggleLinesFor}
                            />
                        </div>
                        }
                    </div>
                    {(this.app.state['отапливаемость_подвала']) &&
                    <div className="mt-3">
                      <CustomNumericInput
                          name={'bt'}
                          className="form-control input-text"
                          value={this.app.state['bt']}
                          allowNegative={false}
                          measure={'[<sup>o</sup>C]'}
                          precision={2}
                          min={0}
                          labelLeft={true}
                          label={"Средняя температура в подвале:"}
                          step={0.1}
                          onValidate={this.validateField}
                          enabledHandlerOnInput={true}
                          isValid={!this.app.state.errors.hasOwnProperty('bt')}
                          onChange={this.customNumericInputHandler}
                      />
                    </div>
                    }
                  </div>
                  }
                    {this.app.state['фундамент_под'] === 'наружную_стену' &&
                    <div className="mt-3">
                        <CustomNumericInput
                            name={'q'}
                            className="form-control input-text"
                            value={this.app.state['q']}
                            allowNegative={false}
                            measure={'[кПа]'}
                            precision={2}
                            min={0}
                            labelLeft={true}
                            label={"Нагрузка на поверхности примыкающей к стене q:"}
                            step={0.1}
                            onValidate={this.validateField}
                            enabledHandlerOnInput={true}
                            isValid={!this.app.state.errors.hasOwnProperty('q')}
                            onChange={this.customNumericInputHandler}
                            onMouseOver={this.toggleLinesFor}
                            onMouseOut={this.toggleLinesFor}
                        />
                    </div>
                    }
                    { (!this.app.state['наличие_подвала'] && this.app.state['тип_расчета'] === 'check') &&
                        <div className="mt-3">
                            <CustomNumericInput
                                name={'hs'}
                                className="form-control input-text"
                                value={this.app.state['hs']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={3}
                                disabled={this.app.state['привязка_к_абсолютным_отметкам']}
                                min={0}
                                labelLeft={true}
                                label={"Высота плитной части hs:"}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('hs')}
                                onChange={this.customNumericInputHandler}
                                onMouseOver={this.toggleLinesFor}
                                onMouseOut={this.toggleLinesFor}
                            />
                        </div>
                    }

                    {(this.app.state['тип_фундамента'] === 'монолитный') &&
                        <div className="mt-3">
                            <CustomNumericInput
                                name={'b_'}
                                className="form-control input-text"
                                value={this.app.state['b_']}
                                allowNegative={false}
                                measure={'[м]'}
                                precision={2}
                                min={0}
                                labelLeft={true}
                                label={"Толщина стенки b':"}
                                step={0.1}
                                onValidate={this.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('b_')}
                                onChange={this.customNumericInputHandler}
                                onMouseOver={this.toggleLinesFor}
                                onMouseOut={this.toggleLinesFor}
                            />
                        </div>
                    }
                    {this.app.state['тип_фундамента'] !== 'монолитный' &&

                        <div className="d-flex mt-3  justify-content-between ">
                            <p className="lh-30 mb-0">Ширина ФБС блока b':</p>
                            <select
                                id='b_'
                                name='b_'
                                onChange={this.paramsHandler}
                                className='fz-14 form-control w-25'
                                value={this.app.state['b_']}
                                onMouseOver={this.toggleLinesFor}
                                onMouseOut={this.toggleLinesFor}
                            >
                                <option value={0.3} >0.3 м</option>
                                <option value={0.4} >0.4 м</option>
                                <option value={0.5} >0.5 м</option>
                                <option value={0.6} >0.6 м</option>
                            </select>
                        </div>

                    }

                    <div className="mt-3">
                      <CustomNumericInput
                          name={'глубина_заложения'}
                          className="form-control input-text"
                          value={this.app.state['глубина_заложения']}
                          allowNegative={false}
                          disabled={this.app.state['привязка_к_абсолютным_отметкам']}
                          measure={'[м]'}
                          precision={2}
                          min={0}
                          labelLeft={true}
                          label={"Глубина заложения d:"}
                          step={0.1}
                          onValidate={this.validateField}
                          enabledHandlerOnInput={true}
                          isValid={!this.app.state.errors.hasOwnProperty('глубина_заложения')}
                          onChange={this.customNumericInputHandler}
                          onMouseOver={this.toggleLinesFor}
                          onMouseOut={this.toggleLinesFor}
                      />
                    </div>
                  {!this.app.state['наличие_подвала'] &&
                    <div className="mt-3">
                      <CustomNumericInput
                          name={'высота_фундамента'}
                          className="form-control input-text"
                          value={this.app.state['высота_фундамента']}
                          allowNegative={false}
                          measure={'[м]'}
                          precision={2}
                          disabled={this.app.state['привязка_к_абсолютным_отметкам']}
                          min={0}
                          labelLeft={true}
                          label={"Высота фундамента h<sub>f</sub>:"}
                          step={0.1}
                          onValidate={this.validateField}
                          enabledHandlerOnInput={true}
                          isValid={!this.app.state.errors.hasOwnProperty('высота_фундамента')}
                          onChange={this.customNumericInputHandler}
                          onMouseOver={this.toggleLinesFor}
                          onMouseOut={this.toggleLinesFor}
                      />
                    </div>
                  }
                </div>
                {(this.app.state['привязка_к_абсолютным_отметкам'])&&
                <div className="col-md-5 basement-scheme" >
                    <div className='position-relative'>
                        <div className={'basement-tracks ' + ((this.app.state['наличие_подвала'])?' track-2':' track-2-2')}>
                            <input name='отметка_верха' id='отметка_верха' className="input-text form-control mr-1" onChange={this.tracksHandler} onBlur={this.tracksHandler} type="text" value={this.app.state['отметка_верха']}/>
                            [м]
                        </div>
                        <div className='basement-tracks track-4'>
                            <input name='отметка_поверхности' id='отметка_поверхности' className="input-text form-control mr-1" onChange={this.tracksHandler} onBlur={this.tracksHandler} type="text" value={this.app.state['отметка_поверхности']}/>
                            [м]
                        </div>
                        <div className='basement-tracks track-5'>
                            <input name='отметка_подошвы' id='отметка_подошвы' className="input-text form-control mr-1" onChange={this.tracksHandler} onBlur={this.tracksHandler} type="text" value={this.app.state['отметка_подошвы']}/>
                            [м]
                        </div>
                        {this.app.state['наличие_подвала'] &&
                        <div className='basement-tracks track-7'>
                            <input name='отметка_подвала' id='отметка_подвала' className="input-text form-control mr-1" onChange={this.tracksHandler} onBlur={this.tracksHandler} type="text" value={this.app.state['отметка_подвала']}/>
                            [м]
                        </div>
                        }
                        {this.app.state['наличие_подвала'] &&
                        <ReactSVG
                            src={basementScheme2}
                            beforeInjection={svg => {
                                svg.setAttribute('class', 'h-auto w-100 mt-5');
                                svg.setAttribute('id', 'basementScheme');
                                this.handleTracksSvgLines(svg)
                            }}
                            evalScripts="always"
                            renumerateIRIElements={false}
                        />
                        }
                        {!this.app.state['наличие_подвала'] &&
                        <ReactSVG
                            src={basementScheme}
                            beforeInjection={svg => {
                                svg.setAttribute('class', 'h-auto w-100 mt-5');
                                svg.setAttribute('id', 'basementScheme');
                                this.handleTracksSvgLines(svg)
                            }}
                            evalScripts="always"
                            renumerateIRIElements={false}
                        />
                        }
                    </div>

                    {this.app.state['тип_фундамента'] === 'блочный' &&
                    <p>Высота плтной части h<sub>s</sub> = 0.5[м],при расчете может измениться на h<sub>s</sub> = 0.3[м]</p>
                    }
                </div>
                }
                {(!this.app.state['привязка_к_абсолютным_отметкам']) &&
                <div className="col-md-5 position-relative basement-scheme">
                    {this.app.state['наличие_подвала'] &&
                    <ReactSVG
                        src={basementScheme2}
                        beforeInjection={svg => {
                            svg.setAttribute('class', 'h-auto w-100 mt-5');
                            svg.setAttribute('id', 'basementScheme');
                            this.handleTracksSvgLines(svg)
                        }}
                        evalScripts="always"
                        renumerateIRIElements={false}
                    />
                    }
                    {!this.app.state['наличие_подвала'] &&
                    <ReactSVG
                        src={basementScheme}
                        beforeInjection={svg => {
                            svg.setAttribute('class', 'h-auto w-100 mt-5');
                            svg.setAttribute('id', 'basementScheme');
                            this.handleTracksSvgLines(svg)
                        }}
                        evalScripts="always"
                        renumerateIRIElements={false}
                    />
                    }
                    {this.app.state['тип_фундамента'] === 'блочный' &&
                     <p>Высота плтной части h<sub>s</sub> = 0.5[м],при расчете может измениться на h<sub>s</sub> = 0.3[м]</p>
                    }
                </div>
                }
        </div>

        </div>
    );
  }
}

export default StripeBasementProps;
