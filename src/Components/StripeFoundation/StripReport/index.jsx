import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-select/dist/css/bootstrap-select.min.css';
import './index.css';
import leftArrow from "../../../img/left-arrow.svg";

const  URL = 'http://foundation-ru.1gb.ru';
// const  URL = 'http://192.168.0.106:8000';


class StripeReport extends Component {

    app = this.props.app;

    paramsHandler = e => {

        this.app.setState({
                [e.target.name]:e.target.checked
        });

    };

    checkAll = () =>{

        this.app.setState({

            initDataHTMLReport: true,
            solveDHTMLReport: true,
            solveAHTMLReport: true,
            solveRHTMLReport: true,
            solveEHtmlReport: true,
            solvePHtmlReport: true,
            EPRCheckHTMLReport: true,
            solveSHTMLReport: true,
            solveStepsHTMLReport: true,
            solvePlateReinforcementHTMLReport: true,
            solveCheckMult: true,
            plateCrackingHTMLReport: true,
            shortSolvedParams: true,
            solveStepHTMLReport:true,
            krenHTMLReport:this.app.state['крен'],
            titlePageHTMLReport:true,
            wallCrackingHTMLReport:true,
            solveWallReinforcementHTMLReport:true,
            solve_pressureHTMLReport:true,
            contentTitles:true,

        })

    };
    uncheckAll = () =>{

        this.app.setState({

            initDataHTMLReport: false,
            solveDHTMLReport: false,
            solveAHTMLReport: false,
            solveRHTMLReport: false,
            solveEHtmlReport: false,
            solvePHtmlReport: false,
            EPRCheckHTMLReport: false,
            solveSHTMLReport: false,
            solvePlateReinforcementHTMLReport: false,
            solveCheckMult: false,
            solveStepHTMLReport:false,
            plateCrackingHTMLReport: false,
            shortSolvedParams: false,
            krenHTMLReport:false,
            titlePageHTMLReport:false,
            wallCrackingHTMLReport:false,
            solveWallReinforcementHTMLReport:false,
            solve_pressureHTMLReport:false,
            contentTitles:false,

        })

    };

    render() {
        return (
            <div style={{display: (this.app.state.tabs === '4')?'block':'none'}} className="container-fluid" >
                <h3>Параметры отчета</h3>
                <div className=" my-3">
                    <form method = "POST"  target="_blank" action={ URL +'/report/layers'}>
                        <button type='submit'  className='mx-1 btn btn-success' onClick={this.showPreloader}> Таблица слоев </button>
                        <input type='text' className='hidden' name='data' value={JSON.stringify(this.props.data.surveysData)}/>
                    </form>
                </div>
                <div className="d-flex mb-3">
                    <div className="btn btn-secondary mr-3" onClick={this.checkAll}>Отметить</div>
                    <div className="btn btn-secondary mr-3" onClick={this.uncheckAll}>Очистить</div>
                </div>
                <div onChange={this.paramsHandler} className="form-group">
                    <div className="my-1 d-flex">
                        <input type="checkbox" checked={this.props.data.titlePageHTMLReport} className="report_checkbox mr-2" id="report20" name='titlePageHTMLReport'/>
                        <label htmlFor="report20" >Титульный лист</label>
                    </div>
                    <div className="my-1 d-flex">
                        <input type="checkbox" checked={this.props.data.contentTitles} className="report_checkbox mr-2" id="report30" name='contentTitles'/>
                        <label htmlFor="report30" >Содержание</label>
                    </div>
                    <div className="my-1 d-flex">
                        <input type="checkbox" checked={this.props.data.initDataHTMLReport} className="report_checkbox mr-2" id="report14" name='initDataHTMLReport'/>
                        <label htmlFor="report14" >Вывести исходные данные</label>
                    </div>
                    {(this.app.state.size || this.app.state['тип_расчета'] === 'solve') &&
                        <div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.solveDHTMLReport} className="report_checkbox mr-2" id="report1" name='solveDHTMLReport'/>
                                <label htmlFor="report1">Определение глубины заложения</label>
                            </div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.solveAHTMLReport} id="report2" className="report_checkbox mr-2" name='solveAHTMLReport'/>
                                <label htmlFor="report2" >Определение габаритов фундамента</label>
                            </div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.solveRHTMLReport} className="report_checkbox mr-2" id="report3" name='solveRHTMLReport'/>
                                <label htmlFor="report3" >Расчет сопротивления грунта основания</label>
                            </div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.solveEHtmlReport} id="report4" className="report_checkbox mr-2" name='solveEHtmlReport'/>
                                <label htmlFor="report4" >Расчет эксцентриситетов нагрузки по подошве фундамента</label>
                            </div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.solvePHtmlReport} id="report5" className="report_checkbox mr-2" name='solvePHtmlReport'/>
                                <label htmlFor="report5" >Рассчитываем давление в угловой точке P</label>
                            </div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.EPRCheckHTMLReport} id="report6" className="report_checkbox mr-2" name='EPRCheckHTMLReport'/>
                                <label htmlFor="report6" >Проверки на эксцетриситеты,R,P</label>
                            </div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.solveSHTMLReport} id="report7" className="report_checkbox mr-2" name='solveSHTMLReport'/>
                                <label htmlFor="report7" >Расчет осадки основания</label>
                            </div>
                        </div>
                    }
                    {this.app.state['тип_расчета'] === 'solve' &&
                        <div className="my-1 d-flex">
                            <input type="checkbox" checked={this.props.data.solveStepHTMLReport} id="report7" className="report_checkbox mr-2" name='solveStepHTMLReport'/>
                            <label htmlFor="report7" >Расчет ступени</label>
                        </div>
                    }
                    {(this.app.state['reinforcement'] || this.app.state['тип_расчета'] === 'solve') &&
                        <div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.solvePlateReinforcementHTMLReport} id="report10" className="report_checkbox mr-2" name='solvePlateReinforcementHTMLReport'/>
                                <label htmlFor="report10" >Армирование плитной части</label>
                            </div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.solveCheckMult} id="report13" disabled={!this.app.state.solvePlateReinforcementHTMLReport} className="report_checkbox mr-2" name='solveCheckMult'/>
                                <label htmlFor="report13" >Проверка прочности плитной части</label>
                            </div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.plateCrackingHTMLReport} id="report11" disabled={!this.app.state.solvePlateReinforcementHTMLReport} className="report_checkbox mr-2" name='plateCrackingHTMLReport'/>
                                <label htmlFor="report11" >Рачет по образованию и раскрытию трещин плитной части</label>
                            </div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.solve_pressureHTMLReport} id="report21" className="report_checkbox mr-2" name='solve_pressureHTMLReport'/>
                                <label htmlFor="report21" >Расчет стены подвала</label>
                            </div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.solveWallReinforcementHTMLReport} id="report22" className="report_checkbox mr-2" name='solveWallReinforcementHTMLReport'/>
                                <label htmlFor="report22" >Армирование и проверка прочности стены подвала</label>
                            </div>
                            <div className="my-1 d-flex">
                                <input type="checkbox" checked={this.props.data.wallCrackingHTMLReport} id="report23" disabled={!this.app.state.solveWallReinforcementHTMLReport} className="report_checkbox mr-2" name='wallCrackingHTMLReport'/>
                                <label htmlFor="report23" >Расчет по образованию и раскрытию трещин стены подвала</label>
                            </div>
                        </div>
                    }

                    <div className="my-1 d-flex">
                        <input type="checkbox"  checked={this.props.data.shortSolvedParams} className="report_checkbox mr-2" id="report24" name='shortSolvedParams'/>
                        <label htmlFor='report24'>Вывод конечных результатов расчета</label>
                    </div>
                </div>
                <div className="d-flex justify-content-between">
                    <div className="position-relative z-index-top">
                        <div className={'mb-5 pointer'} >
                            <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:"2"})} >
                                <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                <span className={'mr-2 fz-14'}><b>К Геологии</b></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default StripeReport;
