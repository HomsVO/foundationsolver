import 'rsuite/dist/styles/rsuite-dark.min.css';
import React, { Component } from 'react';
import StripeLoads from '../StripeLoads/index.jsx'
import deleteIcon from '../../../img/trash.svg';
import StripeSurveys from '../StripeSurveys'
import SurveysTable from '../../SurveysTable'
import StripeFoundationProps from '../StripeFoundationProps/index.jsx'
import StripReport from '../StripReport/index.jsx'
import Errors from '../../Errors/index.jsx'
import { Notification } from 'rsuite';
import { isEmpty } from "../../Global/Global";
import rightArrow from "../../../img/right-arrow.svg";
import leftArrow from "../../../img/left-arrow.svg";


import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';


// const URL = 'http://192.168.0.106:8000';
const URL = 'http://foundation-ru.1gb.ru';

class StripeApp extends Component {

    state = {
        "loadsData": [
            {
                "id": 1,
                "title": "Собственный вес",
                "N": "121",
                "Mx": "0",
                "Qy": 0,
                "type": "постоянная"
            },
            {
                "id": 2,
                "title": "Полезные",
                "N": "37",
                "Mx": "0",
                "Qy": "0",
                "type": "длительная"
            },
            {
                "id": 3,
                "title": "Ветер",
                "N": "0",
                "Mx": "25",
                "Qy": "4.9",
                "type": "кратковременная"
            },
            {
                "id": 4,
                "title": "Снег",
                "N": "16",
                "Mx": "14",
                "Qy": "1",
                "type": "кратковременная"
            }
        ],
        "surveysData": [
            {
                "глубина_залегания": 4.2,
                "подтип_грунта": "",
                "тип_грунта": "суглинок",
                "E_1": "",
                "Ee_1": "",
                "Esr_1": "",
                "Il_1": "0.67",
                "Ip_1": "12.4",
                "Sr_1": "0.63",
                "e_1": "0.85",
                "R_1": "",
                "filter_1": "",
                "particles_1": "",
                "Угол_внутреннего_трения_1": "",
                "влажность_Wl_1": "",
                "влажность_Wp_1": "",
                "плотность_p_1": "",
                "плотность_pd_1": "",
                "плотность_ps_1": "2.69",
                "природная_влажность_W_1": "",
                "сцепление_C_1": "",
                "E_2": "14.1",
                "Ee_2": "",
                "Esr_2": "4.7",
                "Il_2": "",
                "Ip_2": "",
                "Sr_2": "",
                "e_2": "",
                "R_2": "",
                "filter_2": "",
                "particles_2": "",
                "Угол_внутреннего_трения_2": "17",
                "влажность_Wl_2": "",
                "влажность_Wp_2": "",
                "плотность_p_2": "1.73",
                "плотность_pd_2": "",
                "плотность_ps_2": "",
                "природная_влажность_W_2": "",
                "сцепление_C_2": "13",
                "E_3": "",
                "Ee_3": "",
                "Esr_3": "",
                "Il_3": "",
                "Ip_3": "",
                "Sr_3": "",
                "e_3": "",
                "R_3": "",
                "filter_3": "",
                "particles_3": "",
                "Угол_внутреннего_трения_3": "",
                "влажность_Wl_3": "",
                "влажность_Wp_3": "",
                "плотность_p_3": "",
                "плотность_pd_3": "",
                "плотность_ps_3": "",
                "природная_влажность_W_3": "",
                "сцепление_C_3": ""
            },
            {
                "глубина_залегания": 2.4,
                "подтип_грунта": "",
                "тип_грунта": "суглинок",
                "E_1": "",
                "Ee_1": "",
                "Esr_1": "",
                "Il_1": "0.60",
                "Ip_1": "14.1",
                "Sr_1": "0.87",
                "e_1": "0.93",
                "R_1": "",
                "filter_1": "",
                "particles_1": "",
                "Угол_внутреннего_трения_1": "",
                "влажность_Wl_1": "",
                "влажность_Wp_1": "",
                "плотность_p_1": "",
                "плотность_pd_1": "",
                "плотность_ps_1": "2.69",
                "природная_влажность_W_1": "",
                "сцепление_C_1": "",
                "E_2": "",
                "Ee_2": "",
                "Esr_2": "5.3",
                "Il_2": "",
                "Ip_2": "",
                "Sr_2": "",
                "e_2": "",
                "R_2": "",
                "filter_2": "",
                "particles_2": "",
                "Угол_внутреннего_трения_2": "17",
                "влажность_Wl_2": "",
                "влажность_Wp_2": "",
                "плотность_p_2": "1.80",
                "плотность_pd_2": "",
                "плотность_ps_2": "",
                "природная_влажность_W_2": "",
                "сцепление_C_2": "15",
                "E_3": "",
                "Ee_3": "",
                "Esr_3": "",
                "Il_3": "",
                "Ip_3": "",
                "Sr_3": "",
                "e_3": "",
                "R_3": "",
                "filter_3": "",
                "particles_3": "",
                "Угол_внутреннего_трения_3": "",
                "влажность_Wl_3": "",
                "влажность_Wp_3": "",
                "плотность_p_3": "",
                "плотность_pd_3": "",
                "плотность_ps_3": "",
                "природная_влажность_W_3": "",
                "сцепление_C_3": ""
            },
            {
                "глубина_залегания": 3.8,
                "подтип_грунта": "",
                "тип_грунта": "суглинок",
                "E_1": "",
                "Ee_1": "",
                "Esr_1": "",
                "Il_1": "0.15",
                "Ip_1": "15.1",
                "Sr_1": "0.88",
                "e_1": "0.75",
                "R_1": "",
                "filter_1": "",
                "particles_1": "",
                "Угол_внутреннего_трения_1": "",
                "влажность_Wl_1": "",
                "влажность_Wp_1": "",
                "плотность_p_1": "",
                "плотность_pd_1": "",
                "плотность_ps_1": "2.69",
                "природная_влажность_W_1": "",
                "сцепление_C_1": "",
                "E_2": "",
                "Ee_2": "",
                "Esr_2": "11",
                "Il_2": "",
                "Ip_2": "",
                "Sr_2": "",
                "e_2": "",
                "R_2": "",
                "filter_2": "",
                "particles_2": "",
                "Угол_внутреннего_трения_2": "21",
                "влажность_Wl_2": "",
                "влажность_Wp_2": "",
                "плотность_p_2": "1.91",
                "плотность_pd_2": "",
                "плотность_ps_2": "",
                "природная_влажность_W_2": "",
                "сцепление_C_2": "23",
                "E_3": "",
                "Ee_3": "",
                "Esr_3": "",
                "Il_3": "",
                "Ip_3": "",
                "Sr_3": "",
                "e_3": "",
                "R_3": "",
                "filter_3": "",
                "particles_3": "",
                "Угол_внутреннего_трения_3": "",
                "влажность_Wl_3": "",
                "влажность_Wp_3": "",
                "плотность_p_3": "",
                "плотность_pd_3": "",
                "плотность_ps_3": "",
                "природная_влажность_W_3": "",
                "сцепление_C_3": ""
            },
            {
                "глубина_залегания": 4.2,
                "подтип_грунта": "",
                "тип_грунта": "суглинок",
                "E_1": "",
                "Ee_1": "",
                "Esr_1": "",
                "Il_1": "0.07",
                "Ip_1": "17.9",
                "Sr_1": "0.88",
                "e_1": "0.79",
                "R_1": "",
                "filter_1": "",
                "particles_1": "",
                "Угол_внутреннего_трения_1": "",
                "влажность_Wl_1": "",
                "влажность_Wp_1": "",
                "плотность_p_1": "",
                "плотность_pd_1": "",
                "плотность_ps_1": "2.70",
                "природная_влажность_W_1": "",
                "сцепление_C_1": "",
                "E_2": "",
                "Ee_2": "",
                "Esr_2": "13.3",
                "Il_2": "",
                "Ip_2": "",
                "Sr_2": "",
                "e_2": "",
                "R_2": "",
                "filter_2": "",
                "particles_2": "",
                "Угол_внутреннего_трения_2": "16",
                "влажность_Wl_2": "",
                "влажность_Wp_2": "",
                "плотность_p_2": "1.89",
                "плотность_pd_2": "",
                "плотность_ps_2": "",
                "природная_влажность_W_2": "",
                "сцепление_C_2": "33",
                "E_3": "",
                "Ee_3": "",
                "Esr_3": "",
                "Il_3": "",
                "Ip_3": "",
                "Sr_3": "",
                "e_3": "",
                "R_3": "",
                "filter_3": "",
                "particles_3": "",
                "Угол_внутреннего_трения_3": "",
                "влажность_Wl_3": "",
                "влажность_Wp_3": "",
                "плотность_p_3": "",
                "плотность_pd_3": "",
                "плотность_ps_3": "",
                "природная_влажность_W_3": "",
                "сцепление_C_3": ""
            }
        ],
        "tableSize": "normal",
        "tabs": "1",
        "нагрузки": "загружения",
        "привязка_к_абсолютным_отметкам": false,
        "глубина_заложения": "3.35",
        "высота_фундамента": 3.4,
        "отметка_поверхности": "-0.350",
        "отметка_верха": "-0.200",
        "отметка_подошвы": "-4.050",
        "класс_бетона": "B20",
        "город": 274,
        "отапливаемость_здания": true,
        "отапливаемость_подвала": true,
        "глубина_слоя": 0.6,
        "конструктивная_схема": "жесткая",
        "устройство_пола": "с_подвалом",
        "kh_temp": 18,
        "фундамент_под": "наружную_стену",
        "YII_": 2.3,
        "геотехническая_категория": "1",
        "Su": 12,
        "крен": false,
        "класс_арматуры": "A500",
        "initDataHTMLReport": true,
        "solveDHTMLReport": true,
        "solveAHTMLReport": true,
        "solveRHTMLReport": true,
        "solveEHtmlReport": true,
        "solvePHtmlReport": true,
        "EPRCheckHTMLReport": true,
        "solveSHTMLReport": true,
        "solveStepHTMLReport": true,
        "solvePlateReinforcementHTMLReport": true,
        "solveCheckMult": true,
        "plateCrackingHTMLReport": true,
        "shortSolvedParams": true,
        "krenHTMLReport": false,
        "solve_pressureHTMLReport": true,
        "solveWallReinforcementHTMLReport": true,
        "wallCrackingHTMLReport": true,
        "titlePageHTMLReport": true,
        "contentTitles": true,
        "планируемость_территории": false,
        "dw": 4.6,
        "Yc_h": 5,
        "Yc_l": 15,
        "errors": {},
        "тип_расчета": "solve",
        "op": false,
        "size": true,
        "reinforcement": true,
        "ad": 5.5,
        "rein_step": 200,
        "ds_plate": "0.014",
        "ds_wall": 0.012,
        "hn": 0.1,
        "наличие_подготовки": true,
        "задать_ширину_ленты": false,
        "наличие_подвала": true,
        "hbase": 2.8,
        "hcf": 0.15,
        "hs": 0.45,
        "bt": 17,
        "q": 1,
        "b": 2,
        "отметка_подвала": "-3.500",
        "отметка_ступени": 0,
        "отметка_земли": 0,
        "тип_фундамента": "монолитный",
        "b_": 0.5,
        "ds_wall_step": 200,
        "iu": ""
    };

    optionsSu = [];
    optionsCities = [];

  saveData = ()=>{ localStorage.setItem('data1',JSON.stringify(this.state));};
  deleteData = ()=>{ localStorage.removeItem('data1')};
  getData = () =>{ this.setState({...JSON.parse(localStorage.getItem('data1'))})};

  getSu(){
    fetch(URL + '/su')
        .then((response) => { return response.json(); })
        .then((myJson) => {
            myJson.forEach(c=>{

                let ids = [7,8,9,10];
                if(ids.includes(c.id)){
                    this.optionsSu.push({
                        value:c.id,
                        Su:c.Su,
                        label:c.title,
                        isDisabled:c.isDisabled,
                        kren:c.iu,
                        type:c.type,
                    })
                }
            })
        })

  }
  getCities(){
        fetch(URL + '/cities')
            .then((response)=> { return response.json(); })
            .then((myJson) => {
                myJson.forEach(c => {
                    this.optionsCities.push({
                        value: c.id,
                        label: c.city
                    })
                })
            })
    }
  solveFoundation = () =>{
    (async () => {
      const rawResponse = await fetch(URL + '/solverStripe', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(this.state)
      });
      const content = await rawResponse.json();
      console.log(content);
    })();
  };
  showErrorFields = (errors) =>{
      Object.keys(errors).forEach( key =>{
          let el = document.querySelector('input#'+key);
          let evt = new Event('blur');
          if(el) el.dispatchEvent(evt);
      });

  };
  validateBaseParams = (errors) =>{
      Object.keys(this.state).forEach( key =>{
          const value = this.state[key];
          const name = key;
          const isBasement = this.state['наличие_подвала'];
          const isBlock = this.state['тип_фундамента'] === 'блочный';
          let isValid = true;
          switch (name) {
              case 'dw':
                  let deep = 0;
                  this.state.surveysData.forEach(item => deep+= item.глубина_залегания);
                  isValid = true;
                  if(deep < value && value !== 0) isValid = false;
                  if (value < 0) isValid = false;
                  break;
              case 'Su':
                  if (value < 0) isValid = false;
                  break;
              case 'глубина_слоя':
                  if(!this.state['планируемость_территории']) {
                      if (value > this.state['глубина_заложения'] && !isEmpty(this.state['глубина_заложения'])) {
                          isValid = false;
                      }
                      if (isEmpty(value)) isValid = false;
                  }
                  break;
              case 'глубина_заложения':

                  if(value > 7 || value < 0.6 || isEmpty(value)) {
                      isValid = false;
                  }
                  if(value > this.state['высота_фундамента']){
                      isValid = false;
                  }
                  break;
              case 'высота_фундамента':
                  if(value > 8 || value < 0.6 || isEmpty(value)) isValid = false;
                  if(value < this.state['глубина_заложения'])isValid = false;
                  break;
              case 'ds_wall_step':
                  if(this.state['тип_расчета'] === 'check'){
                      if(value > 400 || value < (25 + this.state.ds_wall*1000)){
                          isValid = false;
                      }
                  }
                  break;
              case 'hcf':
                  if((value > 0.4 || value < 0.05 || isEmpty(value)) && isBasement) isValid = false;
                  break;
              case 'hbase':
                  if((value > 6 || value < 0.05 || isEmpty(value)) && isBasement) isValid = false;
                  break;
              case 'hs':
                  if((value > 1 || value < 0.3 || isEmpty(value)) && isBasement && !isBlock) isValid = false;
                  break;
              case 'q':
                  if(value > 200 || value < 0 || isEmpty(value) && isBasement) isValid = false;
                  break;
              case 'bt':
                  if((value > 45 || value < 0 || isEmpty(value)) && isBasement) isValid = false;
                  break;
              case 'b_':
                  if(value > 1 || value < 0.2 || isEmpty(value)) isValid = false;
                  break;
              case 'ad':
                  if(value < 4 || value > 7 && this.state['reinforcement']){
                      isValid = false;
                  }
              break;

              case 'hn':
                  if(this.state['наличие_подготовки']){
                      if(isEmpty(value) || value < 0.1 || value > 1) isValid = false;
                  }
                  break;
              case 'YII_':
                  if (value > 2.5 || value < 0) isValid = false;
                  if (isEmpty(value))  isValid = false;
                  break;
              case 'kh_temp':
                  if(value < 0 || value > 35) isValid = false;
                  break;
              case 'Yc_l':
                  if(this.state['конструктивная_схема'] === 'жесткая'){
                      if(value < 0 || value > 120) isValid = false;
                      if(isEmpty(value)) isValid = false;
                  }
                  break;
              case 'Yc_h':
                  if(this.state['конструктивная_схема'] === 'жесткая'){
                      if(value < 0 || value > 60) isValid = false;
                      if(isEmpty(value)) isValid = false;
                  }
                  break;
              default:
                  break;
          }

          if(this.state['привязка_к_абсолютным_отметкам']){
              switch (name) {
                  case 'отметка_поверхности':
                  case 'отметка_верха':
                  case 'отметка_подошвы':
                  case 'отметка_подвала':
                      if(isEmpty(value)) {
                          isValid = false;
                      }
                      break;
              }
          }


          if(!isValid) errors[name] = {};
          else delete  errors[name];
      });

      return errors;
  };
  validateSurveysTable = (errors) =>{
      let deep = 0;
      this.state.surveysData.forEach(item => deep+= item['глубина_залегания']);
      let dw = (this.state.dw <= deep && this.state.dw !== 0);

      this.state.surveysData.forEach( (item,index) => {
          Object.keys(item).forEach( key =>{

              const gType = item['тип_грунта'];
              const value = item[key];
              const name = key + "_" + index;

              let isClay = false;
              const isNotRock = gType !== 'скальные';

              switch (gType) {
                  case 'суглинок':
                  case 'глина':
                  case 'супесь':
                      isClay = true;
                      break;
                  case 'крупнообломочный грунт':
                      if(item['подтип_грунта'] === 'глинистый_заполнитель') isClay = true;
                      break;
              }

              let isValid = true;
              switch (key) {
                  case 'Ip_1':
                  {
                      if(value < 0 || value > 100 || isEmpty(value) && isClay ) isValid = false;
                  }
                      break;
                  case 'Il_1':
                  {
                      if(value < -1 || value > 2 || isEmpty(value) && isClay ) isValid = false;
                  }
                      break;
                  case 'плотность_ps_1':
                  {
                      if(dw){
                          if(value < 1.5 || value > 3 || isEmpty(value) && isNotRock) isValid = false;
                      }
                  }
                      break;
                  case 'плотность_pd_1':
                  {
                      if(this.state.tableSize !== 'normal'){
                          if(value < 1.3 || value > 2.5 && isEmpty(value)) isValid = false;
                      }
                  }
                      break;
                  case 'e_1':
                  {
                      if(isNotRock && dw) {
                          if (value < 0.2 || value > 1 || isEmpty(value)) isValid = false;
                      }
                  }
                      break;
                  case 'Ee_2':
                  {
                      if(this.state['геотехническая_категория'] === '3' && isNotRock){
                          if(value < 1 || value > 250 || isEmpty(value)) isValid = false;
                      }
                  }
                      break;
                  case 'filter':
                  {
                      if(value < 0 || value > 100 || isEmpty(value)) isValid = false;
                  }
                      break;
                  case 'Sr_1':
                  {
                      if(isNotRock && value < 0 || value > 1 || isEmpty(value)) isValid = false;
                  }
                      break;
                  case 'Esr_2':
                  {
                      if(isNotRock){
                          if(value < 1 || value > 50 || isEmpty(value) && isNotRock) isValid = false;
                      }
                  }
                      break;
                  case 'плотность_p_2':
                  {
                      if(isNotRock){
                          if(value < 1.5 || value > 2.5 || isEmpty(value) && isNotRock) isValid = false;
                      }
                  }
                      break;
                  case 'E_2':
                  {
                      if(isNotRock){
                          if(value < 1 || value > 100 || isEmpty(value) && isNotRock) isValid = false;
                      }

                  }
                      break;
                  case 'сцепление_C_2':
                  {
                      if((value < 0.1 || value > 50 || isEmpty(value)) && isNotRock) isValid = false;
                  }
                      break;
                  case 'Угол_внутреннего_трения_2':
                  {
                      if( isNotRock ){
                          if (value < 0 || value > 45 || isEmpty(value))  isValid = false;
                      }
                  }
                      break;
                  case 'R_2':
                      if((value < 5 || value > 20 || isEmpty(value)) && !isNotRock) isValid = false;
                      break;
                  case 'particles':
                      if(this.state.tableSize !== 'normal'){
                          if(value < 0 || value > 100 || isEmpty(value) && isNotRock) isValid = false;
                      }

                      break;
                  case 'природная_влажность_W_1':
                  {
                      if(this.state.tableSize !== 'normal'){
                          if(value < 0.01 || value > 100 || isEmpty(value) && isNotRock) isValid = false;
                      }
                  }
                      break;
                  case 'влажность_Wl_1':
                  {
                      if(isNotRock && this.state.tableSize !== 'normal'){
                          if(value < 0 || value > 50 || isEmpty(value)) isValid = false;
                      }
                  }
                      break;
                  case 'влажность_Wp_1':
                  {
                      if(isNotRock && this.state.tableSize !== 'normal'){
                          if(value < 0 || value > 50 || isEmpty(value)) isValid = false;
                      }
                  }
                      break;
                  case 'filter_1':
                  {
                      if(this.state.tableSize !== 'normal') {
                          if (value < 0 || value > 100 || isEmpty(value)) isValid = false;
                      }
                  }
                      break;
                  case 'глубина_залегания':
                      if (value <= 0 || isEmpty(value)) isValid = false;
                      break;
              }
              if(!isValid) errors[name] = {};
              else delete  errors[name];
          });
      });
      return errors;
  };

  validateDataAfterSubmit(){
      let errors = this.state.errors;
      // Валидация полей без таблицы
      errors = this.validateBaseParams(errors);
      // Валидация таблицы

      errors = this.validateSurveysTable(errors);
      this.showErrorFields(errors);

      this.setState({
          errors:errors
      });

  }

  tabsHandler = (e) => {this.setState({tabs:e.target.value});};

  formSubmit = (e) => {
    e.preventDefault();

    this.validateDataAfterSubmit();

      let loadsIsValid = false;
      this.state.loadsData.forEach(item => {
          if (item.type === 'постоянная' || item.type === 'длительная') loadsIsValid = true;
      });

      if(!isEmpty(this.state.errors)) {

          Notification.error({
              title: 'Ошибка',
              duration: 40000,
              description: <Errors app={this}/>,
          });
          return false;
      }
      if(this.state.surveysData.length === 0){
          Notification.error({
              title: 'Ошибка',
              duration: 40000,
              description: <div className={'notification-container'}> Не задано ни одного грунтового слоя</div>,
          });
          return false;
      }
      if(!loadsIsValid){
          Notification.error({
              title: 'Ошибка',
              duration: 40000,
              description: <div className={'notification-container'}> Должна быть задана хотя бы одна длительная или постоянная нагрузка</div>,
          });
          return false;
      }
      e.target.submit();
  };

  render() {

    return (
        <div className="stripe container-fluid p-0 root-container">
            <div className="container-fluid p-0">
                <div className="control-panel ">
                    <div className="d-flex">
                        <div className="btn-group btn-group--ml5" onChange={this.tabsHandler} role="group" aria-label="Basic example">
                            <input type="radio" name='tabs' id='control1' value='1' checked={this.state.tabs === '1'}  />
                            <label htmlFor="control1" className="btn btn-control">Сбор нагрузок на фундамент</label>
                            <input type="radio" name='tabs' id='control3' value='3' checked={this.state.tabs === '3'}/>
                            <label htmlFor="control3" className="btn btn-control">Параметры фундамента</label>
                            <input type="radio" name='tabs' id='control2' value='2' checked={this.state.tabs === '2'}/>
                            <label htmlFor="control2" className="btn btn-control">Инженерно-геологические изыскания</label>
                            <input type="radio" name='tabs' id='control4' value='4' checked={this.state.tabs === '4'}/>
                            <label htmlFor="control4" className="btn btn-control">Параметры отчета</label>
                        </div>
                        <div className="btn-group btn-group--ml5" role="group" aria-label="Basic example">
                            <a href='#' className='btn btn-primary my-5px' onClick={this.saveData}>Сохранить данные!</a>
                            <a href='#' className='btn btn-primary my-5px' onClick={this.deleteData}>
                                <img src={deleteIcon} style={{width:'15px',height:'100%'}} alt=""/>
                            </a>
                        </div>
                        <div role="group" className='btn-group btn-group--ml5 my-5px' aria-label="Basic example">
                            <form method="POST" onSubmit={this.formSubmit} target="_blank" action={URL + '/' + this.state['тип_расчета'] + 'Stripe'}>
                                <button type='submit'  className='mx-1 btn btn-success' > Получить отчет </button>
                                <input type='text' className='hidden' name='data' value={JSON.stringify(this.state)}/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
          <div className='container-fluid'>
               <StripeLoads app={this}/>
               <div style={{display: (this.state.tabs === '2')?'block':'none'}} className={'mt-2'}>
                    <StripeSurveys data={this.state} app={this} />
                    <SurveysTable data={this.state} app={this}/>
                   <div className="d-flex justify-content-between mt-3">
                       <div className="position-relative z-index-top mr-3">
                           <div className={'mb-5 pointer'} >
                               <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabs:"3"})} >
                                   <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                   <span className={'mr-2 fz-14'}><b>К Параметрам</b></span>
                               </div>
                           </div>
                       </div>
                       <div className="position-relative z-index-top ml-3">
                           <div className={'mb-5 pointer'} >
                               <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabs:"4"})}>
                                   <span className={'mr-2 fz-14'}><b>К Отчету</b></span>
                                   <img width='25px' height='25px' src={rightArrow} alt=""/>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
              <StripeFoundationProps data={this.state} app={this} />
              <StripReport data={this.state} app={this}/>
          </div>
    </div>
    );
  }
  componentDidMount() {

      this.getData();
      this.getSu(URL);
      this.getCities(URL);
  }
}

export default StripeApp;
