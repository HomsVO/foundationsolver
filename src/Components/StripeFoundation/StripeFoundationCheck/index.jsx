import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-select/dist/css/bootstrap-select.min.css';
import './index.css';
import CustomNumericInput from "../../CustomNumericInput/CustomNumericInput";
import ConstructProperties from "../../ConstructProperties/index.jsx";
import StripeBasementProps from "../StripeBasementProps/index.jsx";
import {ToggleButton, ToggleButtonGroup} from "react-bootstrap";
import {Checkbox, Popover} from "rsuite";

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import {isEmpty} from "../../Global/Global";
import leftArrow from "../../../img/left-arrow.svg";
import rightArrow from "../../../img/right-arrow.svg";

class StripeFoundationCheck extends Component {

    state ={
        su:null,
        selectedOption:{
            Su: 10,
            isDisabled: 0,
            kren: "",
            label: "железобетонным",
            value: 2
        },
        tabIndex:0,
        tabIndex2:0,
        tabIndex3:0,
        tabIndex4:0,
    }

    wrapper = this.props.wrapper;
    app = this.props.app;
    options = this.app.optionsSu;
    paramsHandler = this.wrapper.paramsHandler;
    toggleButtonsGroupHandler = this.wrapper.toggleButtonsGroupHandler;

    customSelectHandler = (e) => {
        this.app.setState({ 'Su':e.Su });
        this.setState({selectedOption:e});
    };
    customNumericInputHandler = (value, name , validator) =>{

        value = parseFloat(value);

      let errors = this.app.state.errors;
      if(!validator.isValid) {
          errors[name] = {
            textError: validator.textError,
            tab: '3'
          };

      }else{
          delete  errors[name];
      }

      this.app.setState({
        [name]: value,
        errors:errors
      });
  };
    customCheckboxHandler = (name,checked) =>{
        this.app.setState({[name]:checked})

        let ch = [];
        let tabIndex = null;
        if(this.app.state['reinforcement']) ch.push(1);
        if(this.app.state['size']) ch.push(0);

        switch (name) {
            case 'reinforcement': tabIndex = 1;
                break;
            case 'size': tabIndex = 0;
                break;
        }

        if(!checked && this.state.tabIndex === tabIndex) {
            let idArr = Array(ch.filter(item => item !== tabIndex))[0];
            let id = idArr.length > 0 ? idArr[0]:123;
            this.setState({tabIndex:id});
        }
        if(checked && this.state.tabIndex === 123){
            this.setState({tabIndex:tabIndex});
        }
  }
    popover = (html) =>{
        return <Popover visible style={{
            color:"#fff"
        }}>{html}</Popover>
    }


    validator = (props) => {
        const value = props.value;
        const name = props.name;
        let isValid = true,
            textError = 'Заполните поле';

        switch (name) {
            case 'hn':
                if (this.app.state['наличие_подготовки']) {
                    if (isEmpty(value)) {
                        isValid = false;
                        textError = 'Заполните поле';
                    }

                    if (value < 0.1 || value > 1) {
                        isValid = false;
                        textError = 'hn должно быть не более 1 м и не менее 0.1 м';
                    }
                }
                break;
            case 'Su':
                if (value < 0) {
                    isValid = false;
                    textError = 'Осадка не может быть отрицательной';
                }
                if (isEmpty(value)) {
                    isValid = false;
                    textError = "Заполните поле";
                }
                break;
            case 'kh_temp':
                if (value < 0) {
                    isValid = false;
                    textError = 'Температура не может быть отрицательной, введите корректные данные';
                }
                if (value > 45) {
                    isValid = false;
                    textError = 'Температура должна быть не более 45';
                }
                if (isEmpty(value)) isValid = false;
                break;
            case 'Yc_l':
                if (value > 120) {
                    isValid = false;
                    textError = 'L не более 120 м';
                }
                if (value < 1) {
                    isValid = false;
                    textError = 'L не менее 1 м';
                }
                if (isEmpty(value)) {
                    isValid = false;
                    textError = 'Заполните поле';
                }
                break;
            case 'ds_wall_step':
                    if(value > 400 || value < (25 + this.app.state.ds_wall*1000)){
                        isValid = false;
                        textError = 'Недопустимый шаг арматуры';
                    }
                break;
            case 'ad':
                if(value < 4 || value > 7){
                    isValid = false;
                    textError = 'Величина защитного слоя должна быть не меньше 4см  и не больше 7см';
                }
                break;
            case 'Yc_h':
                if (value > 60) {
                    isValid = false;
                    textError = 'H не более 60 м';
                }
                if (value < 1) {
                    isValid = false;
                    textError = 'H не менее 1 м';
                }
                if (isEmpty(value)) {
                    isValid = false;
                    textError = 'Заполните поле';
                }
                break;
            case 'b':
                if (this.app.state['форма_подошвы'] === 'прямоугольная') {
                    if (value < 0 || value > 12 || isEmpty(value)) {
                        isValid = false;
                        textError = 'Ширина подколонника  должна быть в пределах от 0 м до 12 м';
                    }
                }
                break;
            case 'глубина_заложения':
                if (value < 0) {
                    isValid = false;
                    textError = 'Не может быть отрицательной';
                }
                if (value < 0.5 || value > 4.5) {
                    isValid = false;
                    textError = 'Глубина заложения должна быть не мене 0.5 м и не более 4.5 м';
                }
                if (!this.app.state['планируемость_территории']) {
                    if (value < this.app.state['глубина_слоя'] && !isEmpty(value)) {
                        isValid = false;
                        textError = 'Подошва фундамента залегает на насыпном грунте,увеличьте глубину заложения';
                    }
                }
                if (isEmpty(value)) isValid = true;
                break;
            case 'высота_фундамента':
                if (value < 0) {
                    isValid = false;
                    textError = 'Не может быть отрицательной';
                }
                if (value < 0.6 || value > 5) {
                    isValid = false;
                    textError = 'Высота фундамента должна быть не мене 0.6 м и не более 5 м';
                }
                if (isEmpty(value)) isValid = true;
                break;

                if (value < 0) {
                    isValid = false;
                    textError = 'Значение не может быть отрицательным';
                }
                return {
                    isValid: isValid,
                    textError: (isValid) ? '' : textError
                }
        }

        return {
            isValid: isValid,
            textError: (isValid) ? '' : textError
        }
    }

    render() {

    return (
        <div className='container-fluid mt-3 '>
            <div className="row mt-3">
                <div className="col-md-12">
                    <Tabs forceRenderTabPanel selectedIndex={this.state.tabIndex} onSelect={(tabIndex,last,e) => {
                        if(e.target.classList.contains('rs-checkbox-wrapper')) return false;
                        this.setState({ tabIndex })
                    }}>
                        <TabList>
                            <Tab disabled={!this.app.state.size}>
                                <div className="d-flex">
                                    <Checkbox name={'size'} checked={this.app.state.size} onChange={(v,checked) => this.customCheckboxHandler('size',checked)} />
                                    <p className={'mb-0 lh-35'}>Габариты фундамента</p>
                                </div>
                            </Tab>
                            <Tab disabled={!this.app.state.reinforcement}>
                                <div className="d-flex">
                                    <Checkbox name={'reinforcement'} checked={this.app.state.reinforcement} onChange={(v,checked) => this.customCheckboxHandler('reinforcement',checked)} />
                                    <p className={'mb-0 lh-35'}>Армирование стенки и плиты</p>
                                </div>
                            </Tab>
                        </TabList>
                        <TabPanel>
                            <Tabs forceRenderTabPanel selectedIndex={this.state.tabIndex3} onSelect={tabIndex3 => this.setState({ tabIndex3 })}>
                                <TabList>
                                    <Tab>Параметры фундамента</Tab>
                                    <Tab>Параметры подвала</Tab>
                                    <Tab>Параметры здания</Tab>
                                </TabList>
                                <TabPanel>
                                    <div className="row">
                                        <div className="col-md-6">
                                           <div className="mt-3">
                                                    <CustomNumericInput
                                                        name={'b'}
                                                        className="form-control input-text"
                                                        value={this.app.state.b}
                                                        allowNegative={false}
                                                        measure={'[м]'}
                                                        labelLeft
                                                        label={"Ширина b:"}
                                                        min={0}
                                                        step={0.1}
                                                        onValidate={this.validator}
                                                        enabledHandlerOnInput={true}
                                                        isValid={!this.app.state.errors.hasOwnProperty('b')}
                                                        onChange={this.customNumericInputHandler}
                                                    />
                                            </div>
                                            <p className={'mt-3'}><b><ins>Конструктивные особенности фундамента</ins></b></p>
                                            <div className="d-flex  justify-content-between">
                                                <p className="lh-30 mb-0 mr-2">Фундамент под:</p>
                                                <ToggleButtonGroup type="radio" onChange={this.toggleButtonsGroupHandler} name="фундамент_под" value={this.app.state['фундамент_под']}>
                                                    <ToggleButton size="sm" variant="custom" value={'наружную_стену'}>Наружную стену</ToggleButton>
                                                    <ToggleButton size="sm" variant="custom" value={'внутреннюю_стену'}>Внутреннюю стену</ToggleButton>
                                                </ToggleButtonGroup>
                                            </div>
                                            <div className="mt-3">
                                                <Checkbox name={'наличие_подготовки'} checked={this.app.state['наличие_подготовки']} onChange={(v,checked) => this.customCheckboxHandler('наличие_подготовки',checked)} >Наличие подготовки под фундамент</Checkbox>
                                                {(this.app.state['наличие_подготовки']) &&
                                                <CustomNumericInput
                                                    name={'hn'}
                                                    className="form-control input-text"
                                                    value={this.app.state['hn']}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    precision={2}
                                                    min={0}
                                                    labelLeft={true}
                                                    label={"Толщина подготовки h<sub>n</sub>:"}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('hn')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="d-flex justify-content-between">
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'} >
                                                <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:"1"})} >
                                                    <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                                    <span className={'mr-2 fz-14'}><b>К Нагрузкам</b></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'} >
                                                <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex3:this.state.tabIndex3 + 1})}>
                                                    <span className={'mr-2 fz-14'}><b>К параметрам подвала</b></span>
                                                    <img width='25px' height='25px' src={rightArrow} alt=""/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <StripeBasementProps context={this} />
                                    <div className="d-flex justify-content-between">
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'} >
                                                <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex3:this.state.tabIndex3 - 1})} >
                                                    <img width='25px' height='25px' className={'mr-2'} src={leftArrow} alt=""/>
                                                    <span className={'mr-2 fz-14'}><b>К параметрам фундамента</b></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'} >
                                                <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex3:this.state.tabIndex3 + 1})}>
                                                    <span className={'mr-2 fz-14'}><b>К параметрам здания</b></span>
                                                    <img width='25px' height='25px' src={rightArrow} alt=""/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <ConstructProperties context={this} isStripe basement={this.app.state['наличие_подвала']}/>
                                    <div className="d-flex justify-content-between">
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'} >
                                                <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex3:this.state.tabIndex3 - 1})} >
                                                    <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                                    <span className={'mr-2 fz-14'}><b>К параметрам подвала</b></span>
                                                </div>
                                            </div>
                                        </div>
                                        {(this.app.state.reinforcement) &&
                                            <div className="position-relative z-index-top">
                                                <div className={'mb-5 pointer'} >
                                                    <div className="btn btn-light p-1 px-5 " onClick={() => this.setState({ tabIndex:this.state.tabIndex + 1})}>
                                                        <span className={'mr-2 fz-14'}><b>К проверке армирования</b></span>
                                                        <img width='25px' height='25px' src={rightArrow} alt=""/>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                        {(!this.app.state.reinforcement) &&
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'} >
                                                <div className="btn btn-light p-1 px-5 " onClick={() => this.app.setState({ tabs:"2"})} >
                                                    <span className={'mr-2 fz-14'}><b>К Геологии</b></span>
                                                    <img width='25px' height='25px' src={rightArrow} alt=""/>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                    </div>
                                </TabPanel>
                            </Tabs>
                        </TabPanel>
                        <TabPanel>
                            <Tabs forceRenderTabPanel selectedIndex={this.state.tabIndex4} onSelect={tabIndex4 => this.setState({ tabIndex4 })}>
                                <TabList>
                                    <Tab>Параметры фундамента</Tab>
                                    {!this.app.state.size &&
                                        <Tab>Параметры подвала</Tab>
                                    }
                                    {this.app.state.size &&
                                    <Tab disabled>Параметры подвала (Данные взяты из вкладки габариты фундамента)</Tab>
                                    }
                                </TabList>
                                <TabPanel>
                                    <div className="row pb-5">
                                        <div className="col-md-6">
                                            <div className={'position-relative'}>
                                                <p className={this.app.state.size?'disabled-size-data':'hide'}>Данные взяты из вкладки габариты фундамента</p>
                                                <div className={this.app.state.size?'disabled':''}>
                                                    <div className="mt-3">
                                                        <CustomNumericInput
                                                            name={'b'}
                                                            className="form-control input-text"
                                                            value={this.app.state.b}
                                                            allowNegative={false}
                                                            measure={'[м]'}
                                                            labelLeft
                                                            label={"Ширина b:"}
                                                            min={0}
                                                            step={0.1}
                                                            onValidate={this.validator}
                                                            enabledHandlerOnInput={true}
                                                            isValid={!this.app.state.errors.hasOwnProperty('b')}
                                                            onChange={this.customNumericInputHandler}
                                                        />
                                                    </div>
                                            <p className={'mt-3'}><b><ins>Конструктивные особенности фундамента</ins></b></p>
                                            <div className="d-flex  justify-content-between">
                                                <p className="lh-30 mb-0 mr-2">Фундамент под:</p>
                                                <ToggleButtonGroup type="radio" onChange={this.toggleButtonsGroupHandler} name="фундамент_под" value={this.app.state['фундамент_под']}>
                                                    <ToggleButton size="sm" variant="custom" value={'наружную_стену'}>Наружную стену</ToggleButton>
                                                    <ToggleButton size="sm" variant="custom" value={'внутреннюю_стену'}>Внутреннюю стену</ToggleButton>
                                                </ToggleButtonGroup>
                                            </div>
                                            <div className="mt-3">
                                                <Checkbox name={'наличие_подготовки'} checked={this.app.state['наличие_подготовки']} onChange={(v,checked) => this.customCheckboxHandler('наличие_подготовки',checked)} >Наличие подготовки под фундамент</Checkbox>
                                                {(this.app.state['наличие_подготовки']) &&
                                                <CustomNumericInput
                                                    name={'hn'}
                                                    className="form-control input-text"
                                                    value={this.app.state['hn']}
                                                    allowNegative={false}
                                                    measure={'[м]'}
                                                    precision={2}
                                                    min={0}
                                                    labelLeft={true}
                                                    label={"Толщина подготовки h<sub>n</sub>:"}
                                                    step={0.1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('hn')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                                }
                                            </div>
                                        </div>
                                    </div>
                                            <div>
                                                <p className={'mt-3'}><b><ins>Вариант опирания перекрытия на стену</ins></b></p>
                                                <ToggleButtonGroup type="radio" name="опирание_плиты" value={this.app.state['опирание_плиты']} onChange={this.toggleButtonsGroupHandler}>
                                                    <ToggleButton size="sm" variant="custom" value={'монолитное'}>Монолитное</ToggleButton>
                                                    <ToggleButton size="sm" variant="custom" disabled value={'шарнирное'}>Шарнирное</ToggleButton>
                                                </ToggleButtonGroup>
                                            </div>
                                            <div className="mt-3 d-flex justify-content-between">
                                                <p className={'mb-0 lh-30'}>Диаметр арматуры плитной части:</p>
                                                <select name='ds_plate' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['диаметр_арматуры']}>
                                                    <option value='0.012'>12 мм</option>
                                                    <option value='0.014'>14 мм</option>
                                                    <option value='0.016'>16 мм</option>
                                                    <option value='0.018'>18 мм</option>
                                                    <option value='0.02'>20 мм</option>
                                                    <option value='0.022'>22 мм</option>
                                                    <option value='0.025'>25 мм</option>
                                                    <option value='0.028'>28 мм</option>
                                                    <option value='0.032'>32 мм</option>
                                                    <option value='0.036'>36 мм</option>
                                                    <option value='0.04'>40 мм</option>
                                                    <option value='0.045'>45 мм</option>
                                                    <option value='0.05'>50 мм</option>
                                                    <option value='0.055'>55 мм</option>
                                                    <option value='0.06'>60 мм</option>
                                                    <option value='0.07'>70 мм</option>
                                                    <option value='0.08'>80 мм</option>
                                                </select>
                                            </div>
                                            <div className="mt-3 d-flex justify-content-between">
                                                <p className={'mb-0 lh-30'}>Диаметр арматуры стены фундамента:</p>
                                                <select name='ds_wall' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['диаметр_арматуры']}>
                                                    <option value='0.012'>12 мм</option>
                                                    <option value='0.014'>14 мм</option>
                                                    <option value='0.016'>16 мм</option>
                                                    <option value='0.018'>18 мм</option>
                                                    <option value='0.02'>20 мм</option>
                                                    <option value='0.022'>22 мм</option>
                                                    <option value='0.025'>25 мм</option>
                                                    <option value='0.028'>28 мм</option>
                                                    <option value='0.032'>32 мм</option>
                                                    <option value='0.036'>36 мм</option>
                                                    <option value='0.04'>40 мм</option>
                                                    <option value='0.045'>45 мм</option>
                                                    <option value='0.05'>50 мм</option>
                                                    <option value='0.055'>55 мм</option>
                                                    <option value='0.06'>60 мм</option>
                                                    <option value='0.07'>70 мм</option>
                                                    <option value='0.08'>80 мм</option>
                                                </select>
                                            </div>
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'ds_wall_step'}
                                                    className="form-control input-text"
                                                    value={this.app.state.ds_wall_step}
                                                    allowNegative={false}
                                                    measure={'[мм]'}
                                                    precision={2}
                                                    labelLeft
                                                    label={"Шаг арматуры вдоль стены"}
                                                    min={0}
                                                    max={400}
                                                    step={1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('ds_wall_step')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            <div className="mt-3">
                                                <CustomNumericInput
                                                    name={'ad'}
                                                    className="form-control input-text"
                                                    value={this.app.state.ad}
                                                    allowNegative={false}
                                                    measure={'[см]'}
                                                    precision={2}
                                                    labelLeft
                                                    label={"Величина защитного слоя :"}
                                                    min={4}
                                                    max={7}
                                                    step={1}
                                                    onValidate={this.validator}
                                                    enabledHandlerOnInput={true}
                                                    isValid={!this.app.state.errors.hasOwnProperty('ad')}
                                                    onChange={this.customNumericInputHandler}
                                                />
                                            </div>
                                            <div className="d-flex justify-content-between mt-3">
                                                <p className="lh-30 mb-0">Класс бетона:</p>
                                                <select name='класс_бетона' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['класс_бетона']}>
                                                    <option value='B15'>B15</option>
                                                    <option value='B20'>B20</option>
                                                    <option value='B25'>B25</option>
                                                    <option value='B30'>B30</option>
                                                    <option value='B35'>B35</option>
                                                    <option value='B40'>B40</option>
                                                    <option value='B45'>B45</option>
                                                    <option value='B50'>B50</option>
                                                    <option value='B55'>B55</option>
                                                    <option value='B60'>B60</option>
                                                    <option value='B70'>B70</option>
                                                    <option value='B80'>B80</option>
                                                    <option value='B90'>B90</option>
                                                    <option value='B100'>B100</option>
                                                </select>
                                            </div>
                                            <div className="d-flex mt-3  justify-content-between ">
                                                <p className="lh-30 mb-0">Класс арматуры:</p>
                                                <select name='класс_арматуры' onChange={this.paramsHandler} className='fz-14 form-control w-25' value={this.app.state['класс_арматуры']}>
                                                    <option value='A240'>А240</option>
                                                    <option value='A400'>А400</option>
                                                    <option value='A500'>А500</option>
                                                    <option value='A600'>А600</option>
                                                    <option value='A800'>А800</option>
                                                    <option value='A1000'>А1000</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className={'col-md-6'}>
                                        </div>
                                    </div>
                                    <div className="d-flex justify-content-between">
                                        {(this.app.state.size) &&
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'}>
                                                <div className="btn btn-light p-1 px-5 "
                                                     onClick={() => this.setState({tabIndex: this.state.tabIndex - 1})}>
                                                    <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                                    <span className={'mr-2 fz-14'}><b>К проверке габаритов</b></span>
                                                </div>
                                            </div>
                                        </div>
                                        }
                                        {(!this.app.state.size) &&
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'}>
                                                <div className="btn btn-light p-1 px-5 "
                                                     onClick={() => this.app.setState({ tabs:"1"})}>
                                                    <img width='25px' height='25px'  className={'mr-2'} src={leftArrow} alt=""/>
                                                    <span className={'mr-2 fz-14'}><b>К Нагрузкам</b></span>

                                                </div>
                                            </div>
                                        </div>
                                        }
                                        {(this.app.state.size) &&
                                            <div className="position-relative z-index-top">
                                                <div className={'mb-5 pointer'}>
                                                    <div className="btn btn-light p-1 px-5 "
                                                         onClick={() => this.app.setState({tabs: "2"})}>
                                                        <span className={'mr-2 fz-14'}><b>К Геологии</b></span>
                                                        <img width='25px' height='25px' src={rightArrow} alt=""/>
                                                    </div>
                                                </div>
                                            </div>
                                        }

                                        { (!this.app.state.size) &&
                                            <div className="position-relative z-index-top">
                                                <div className={'mb-5 pointer'}>
                                                    <div className="btn btn-light p-1 px-5 "
                                                         onClick={() => this.setState({tabIndex4: this.state.tabIndex4 + 1})}>
                                                        <span className={'mr-2 fz-14'}><b>К параметрам подвала</b></span>
                                                        <img width='25px' height='25px' src={rightArrow} alt=""/>
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <StripeBasementProps context={this} />
                                    <div className="d-flex justify-content-between">
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'}>
                                                <div className="btn btn-light p-1 px-5 "
                                                     onClick={() => this.setState({tabIndex4: this.state.tabIndex4 - 1})}>
                                                    <img width='25px'  className={'mr-2'} height='25px' src={leftArrow} alt=""/>
                                                    <span className={'mr-2 fz-14'}><b>К параметрам фундамента</b></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="position-relative z-index-top">
                                            <div className={'mb-5 pointer'}>
                                                <div className="btn btn-light p-1 px-5 "
                                                     onClick={() => this.app.setState({tabs: "2"})}>
                                                    <span className={'mr-2 fz-14'}><b>К Геологии</b></span>
                                                    <img width='25px' height='25px' src={rightArrow} alt=""/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPanel>
                            </Tabs>
                        </TabPanel>
                    </Tabs>
                </div>
            </div>
        </div>
    );
  }

}

export default StripeFoundationCheck;
