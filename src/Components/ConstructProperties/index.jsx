import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-select/dist/css/bootstrap-select.min.css';
import './index.css';
import {Whisper} from "rsuite";
import {ToggleButton, ToggleButtonGroup} from "react-bootstrap";
import Select from "react-select";
import CustomNumericInput from "../CustomNumericInput/CustomNumericInput";
import {isEmpty} from "../Global/Global";

class ConstructProperties extends Component {
    state = {
        selectedOption:{
            Su: 12,
            isDisabled: 0,
            kren: "",
            label: "крупных блоков или кирпичной кладки без армирования",
            value: 9
        },
    }
    app = this.props.context.app;
    options = this.app.optionsSu;
    isStripe = this.props.hasOwnProperty('isStripe');

    customSelectHandler = (e) => {
        this.app.setState({ 'Su':e.Su });
        this.setState({selectedOption:e});
        if(!isEmpty(e.kren)){
            this.app.setState({
                'крен':true,
                iu:e.kren,
            })
        }else{
            this.app.setState({
                'крен':false,
                iu:'',
            })
        }
    };
    render() {
        let options = this.options;
        if(!this.isStripe){
            options = this.options.filter(item =>{
                return item.type.split(',').includes(this.app.state['тип_здания']);
            })
        }

        let self = this.props.context;
        return (
            <>
                <div className={'row'}>
                    <div className="col-md-6 pb-5">
                        <div className="d-flex mt-2">
                            <p className="lh-30 mb-0 mr-5">Геотех. категория</p>
                            <Whisper placement="top" delay={500} trigger="hover" speaker={self.popover(
                                <div className={'text-left'}>
                                    <p className={'mb-1'}>Категорию сложности инженерно-геологических условий строительства <br/> следует определять  в соответствии с СП 47.13330. Таблица А.1.
                                    </p>
                                    <p className={'mb-1'}>I - (простая)</p>
                                    <p className={'mb-1'}>II - (средняя)</p>
                                    <p className={'mb-1'}>III - (сложная)</p>
                                </div>
                            )} >
                                <ToggleButtonGroup type="radio" onChange={self.toggleButtonsGroupHandler} name="геотехническая_категория" value={self.app.state['геотехническая_категория']}>
                                    <ToggleButton size="sm" variant="custom" value={"1"}>I</ToggleButton>
                                    <ToggleButton size="sm" variant="custom" value={"2"}>II</ToggleButton>
                                    <ToggleButton size="sm" variant="custom" value={"3"}>III</ToggleButton>
                                </ToggleButtonGroup>
                            </Whisper>
                        </div>
                        {!this.isStripe &&
                            <div className="mt-2">
                                <p className="lh-30 mb-0">Тип здания</p>
                                <select name='тип_здания' className='fz-14 form-control' onChange={self.paramsHandler} value={self.app.state['тип_здания']}>
                                    <option value='тип_1'>С мостовым краном грузоподъемностью 75т и выше</option>
                                    <option value='тип_2'>Для фундаментов колонн открытых крановых эстакад при кранах грузоподъемностью свыше 15т</option>
                                    <option value='тип_3'>Для сооружения башенного типа</option>
                                    <option value='тип_4'>Для фундаментов зданий с мостовыми кранами </option>
                                    <option value='тип_5'>Для фундаментов бескрановых зданий </option>
                                    <option value='тип_6'>Для зданий с подвесным транспортным оборудованием </option>
                                </select>
                            </div>
                        }
                        <div className="mt-3 d-flex justify-content-between">
                            <p className="lh-30 mb-0">Конструкция здания:</p>
                            <Select
                                onChange={this.customSelectHandler}
                                options={options}
                                value={this.state.selectedOption}
                                className='w-50'
                            />
                        </div>
                        <div className="mt-3">
                            <CustomNumericInput
                                name={'Su'}
                                className="form-control input-text"
                                value={this.app.state.Su}
                                allowNegative={false}
                                measure={'[см]'}
                                labelLeft={true}
                                label={"Предельное значение осадки S<sub>u</sub>:"}
                                step={0.1}
                                min={0}
                                onValidate={self.validateField}
                                enabledHandlerOnInput={true}
                                isValid={!this.app.state.errors.hasOwnProperty('Su')}
                                onChange={self.customNumericInputHandler}
                            />
                        </div>
                        {!this.isStripe &&
                            <div className="mt-3 d-flex justify-content-between">
                                <p className="m-0 lh-30">Конструктивная схема:</p>
                                <Whisper placement="top" delay={500} trigger="hover" speaker={self.popover(
                                    <div className={'text-left'}>
                                        <p className={'mb-1'}>
                                            К числу зданий и сооружений с жесткой конструктивной схемой относятся: <br/>
                                            <br/>
                                            - здания панельные, блочные и кирпичные, в которых междуэтажные перекрытия опираются по всему контуру <br/> на поперечные и продольные стены или только на поперечные несущие стены при малом их шаге;
                                            <br/>
                                            <br/>
                                            - сооружения типа башен, силосных корпусов, дымовых труб, домен и др.
                                            </p>
                                    </div>
                                )} >
                                    <ToggleButtonGroup type="radio" name="конструктивная_схема" onChange={self.toggleButtonsGroupHandler} value={self.app.state['конструктивная_схема']}>
                                        <ToggleButton size="sm" variant="custom" value="жесткая">Жесткая</ToggleButton>
                                        <ToggleButton size="sm" variant="custom" value="гибкая">Гибкая</ToggleButton>
                                    </ToggleButtonGroup>
                                </Whisper>
                            </div>
                        }

                        {(this.app.state['конструктивная_схема'] === "жесткая") &&
                        <div className="row mt-3 ">
                            <div className="col-md-4 pr-0 position-relative">
                                <p className="fz-14 m-0 pa-top25">Размеры здания: </p>
                            </div>
                            <div className="col-md-8 pl-0">
                                <div>
                                    <CustomNumericInput
                                        name={'Yc_l'}
                                        className="form-control input-text"
                                        value={this.app.state.Yc_l}
                                        allowNegative={false}
                                        measure={'[м]'}
                                        precision={2}
                                        labelLeft={true}
                                        label={"Длина L:"}
                                        min={0}
                                        step={0.1}
                                        onValidate={self.validateField}
                                        enabledHandlerOnInput={true}
                                        isValid={!this.app.state.errors.hasOwnProperty('Yc_l')}
                                        onChange={self.customNumericInputHandler}
                                    />
                                </div>
                                <div className="mt-2">
                                    <CustomNumericInput
                                        name={'Yc_h'}
                                        className="form-control input-text"
                                        value={this.app.state.Yc_h}
                                        allowNegative={false}
                                        measure={'[м]'}
                                        precision={2}
                                        labelLeft={true}
                                        label={"Высота H: "}
                                        min={0}
                                        step={0.1}
                                        onValidate={self.validateField}
                                        enabledHandlerOnInput={true}
                                        isValid={!this.app.state.errors.hasOwnProperty('Yc_h')}
                                        onChange={self.customNumericInputHandler}
                                    />
                                </div>
                            </div>
                        </div>
                        }
                        {this.props.basement && !this.app.state['отапливаемость_подвала'] &&
                        <div className="mt-3 d-flex justify-content-between">
                            <p className="m-0 lh-30">Отапливаемость здания</p>
                            <ToggleButtonGroup type="radio" name="отапливаемость_здания"
                                               onChange={self.toggleButtonsGroupHandler}
                                               value={this.app.state['отапливаемость_здания']}>
                                <ToggleButton size="sm" variant="custom" value={true}>Отапливаемое</ToggleButton>
                                <ToggleButton size="sm" variant="custom" value={false}>Не отапливаемое</ToggleButton>
                            </ToggleButtonGroup>
                        </div>
                        }
                        {this.props.basement && self.app.state['отапливаемость_подвала'] &&
                            <div className="row mt-2">
                                <div className="col-md-12">
                                    <p className='fz-14 mb-1'>Устройство пола</p>
                                    <select name="устройство_пола" onChange={self.paramsHandler}
                                            value={this.app.state['устройство_пола']} className='form-control p-1'>
                                        <option value="по_цокольному">По утепленному цокольному перекрытию</option>
                                        <option value="с_подвалом">С  подвалом или техническим подпольем</option>
                                    </select>
                                </div>
                            </div>
                        }
                        {!this.props.basement &&
                        <>
                            <div className="mt-3 d-flex justify-content-between">
                                <p className="m-0 lh-30">Отапливаемость здания</p>
                                <ToggleButtonGroup type="radio" name="отапливаемость_здания"
                                                   onChange={self.toggleButtonsGroupHandler}
                                                   value={this.app.state['отапливаемость_здания']}>
                                    <ToggleButton size="sm" variant="custom" value={true}>Отапливаемое</ToggleButton>
                                    <ToggleButton size="sm" variant="custom" value={false}>Не отапливаемое</ToggleButton>
                                </ToggleButtonGroup>
                            </div>
                            {(self.app.state['отапливаемость_здания']) &&
                            <div className="row mt-2">
                                <div className="col-md-6">
                                    <p className='fz-14 mb-1'>Устройство пола</p>
                                    <select name="устройство_пола" onChange={self.paramsHandler}
                                            value={this.app.state['устройство_пола']} className='form-control p-1'>
                                        <option value="по_грунту">По грунту</option>
                                        <option value="на_лагах">На лагах по грунту</option>
                                        <option value="по_цокольному">По утепленному цокольному перекрытию</option>
                                    </select>
                                </div>
                                <div className="col-md-6">
                                    <CustomNumericInput
                                        name={'kh_temp'}
                                        className="form-control input-text"
                                        value={this.app.state.kh_temp}
                                        allowNegative={false}
                                        measure={'[<sup>o</sup>C]'}
                                        label={"Температура в помещении:"}
                                        min={0}
                                        step={0.1}
                                        onValidate={self.validateField}
                                        enabledHandlerOnInput={true}
                                        isValid={!this.app.state.errors.hasOwnProperty('kh_temp')}
                                        onChange={self.customNumericInputHandler}
                                    />
                                </div>
                            </div>
                            }
                        </>
                        }
                    </div>
                </div>
        </>
        );
    }
}

export default ConstructProperties;
